import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Program } from '../../shared/program/program.model';
import { Episode } from '../../shared/episode/episode.model';
import { FavoriteList } from '../../shared/favorite_list/favorite-list.model';
import { GeneralService } from '../../shared/general/general.service';
import { EpisodeService } from '../../shared/episode/episode.service';
import { ProgramService } from '../../shared/program/program.service';
import { EpisodeRepository } from '../../shared/episode/episode.repository';
import { ProgramRepository } from '../../shared/program/program.repository';
import { FavoriteListService } from '../../shared/favorite_list/favorite-list.service';
import { FavoriteListRepository } from '../../shared/favorite_list/favorite-list.repository';

@Component ({
    selector   : 'app-search',
    templateUrl: './search.component.html',
    styleUrls  : [ './search.component.css' ]
})
export class SearchComponent implements OnInit {
    context: string;
    program_results: Program[];
    episode_results: Episode[];
    favorite_list_results: FavoriteList[];
    page: number;
    
    constructor (private route: ActivatedRoute, public generalService: GeneralService, private episodeService: EpisodeService,
                 private programService: ProgramService, private favoriteListService: FavoriteListService) {
        this.context = '';
        this.page    = 0;
    }
    
    getPageData (force_request: boolean) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            () => {
                this.episodeService.getEpisodeSearchResult (this.generalService.router_path, this.context, this.page).subscribe (
                    (result) => {
                        this.episode_results = EpisodeRepository.setEpisodes (result.data);
                    },
                    () => {
                        if (this.episodeService.fetch_get_search_episode_error_retry_count < 6) {
                            this.episodeService.fetch_get_search_episode_error_retry_count += 1;
                            this.getPageData (true);
                        } else {
                            if (!this.generalService.is_platform_server) {
                                this.generalService.showMessage (this.generalService.statics.error_message_500_id, 10);
                            }
                            this.generalService.show_normal_content = false;
                        }
                    }
                );
                
                this.programService.getProgramSearchResult (this.generalService.router_path, this.context, this.page).subscribe (
                    (result) => {
                        this.program_results = ProgramRepository.setPrograms (result.data);
                    },
                    (error) => {
                        if (this.programService.fetch_get_search_error_retry_count < 6) {
                            this.programService.fetch_get_search_error_retry_count += 1;
                            this.getPageData (true);
                        } else {
                            if (!this.generalService.is_platform_server) {
                                this.generalService.showMessage (this.generalService.statics.error_message_500_id, 10);
                            }
                            this.generalService.show_normal_content = false;
                        }
                    },
                    () => {
                        this.favoriteListService.getFavoriteListSearchResult (this.generalService.router_path, this.context, this.page).subscribe (
                            (result) => {
                                this.favorite_list_results = FavoriteListRepository.setFavoriteLists (result.data);
                            }
                        );
                    }
                );
            });
    }
    
    setPageMetaData () {
        this.generalService.setTitle ('جستجو برای واژه ' + this.context);
        this.generalService.setMetaTags ([
            {
                name   : 'description',
                content: 'جستجو برای واژه ' + this.context
            }
        ]);
    }
    
    ngOnInit () {
        this.route.params.subscribe ((params) => {
            this.context = params[ 'context' ];
            this.setPageMetaData ();
            this.getPageData (false);
        });
    }
    
}
