import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { search_routes } from './search.routing';
import { SearchComponent } from './search.component';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(search_routes)
    ],
    declarations: [ SearchComponent ]
})
export class SearchModule {
}
