import { SearchComponent } from './search.component';

export const search_routes = [
    {
        path     : 'search/:context',
        component: SearchComponent
    }
];
