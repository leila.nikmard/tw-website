import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundComponent } from './not-found.component';
import { RouterModule } from '@angular/router';
import { not_found_routes } from './not-found.routing';
import { GeneralService } from '../../shared/general/general.service';
import { ProgramService } from '../../shared/program/program.service';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(not_found_routes)
    ],
    declarations: [ NotFoundComponent ],
    providers: [GeneralService, ProgramService]
})
export class NotFoundModule {
}
