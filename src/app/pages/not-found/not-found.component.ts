import { Component, OnInit } from '@angular/core';
import { Episode } from '../../shared/episode/episode.model';
import { GeneralService } from '../../shared/general/general.service';
import { ProgramService } from '../../shared/program/program.service';
import { EpisodeRepository } from '../../shared/episode/episode.repository';

@Component ({
    selector   : 'app-not-found',
    templateUrl: './not-found.component.html',
    styleUrls  : [ './not-found.component.css' ]
})
export class NotFoundComponent implements OnInit {
    public episodes: Array<Episode> = [];
    public page: number;
    
    constructor (public generalService: GeneralService, private programService: ProgramService) {
        this.page = 0;
    }
    
    ngOnInit () {
        this.getPageData (false);
    }
    
    getPageData (force_request: boolean) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            () => {
                this.programService.getProgramByCategoryGenre (this.generalService.router_path, [ 7 ], this.page).subscribe (
                    (result) => {
                        this.episodes = EpisodeRepository.setEpisodes (result.data);
                    },
                    () => {
                        if (this.programService.fetch_get_not_found_error_retry_count < 6) {
                            this.programService.fetch_get_not_found_error_retry_count += 1;
                            this.getPageData (true);
                        } else {
                            if (!this.generalService.is_platform_server) {
                                this.generalService.showMessage (this.generalService.statics.error_message_500_id, 10);
                            }
                            this.generalService.show_normal_content = false;
                        }
                    },
                    () => {
                        this.setPageMetaData ();
                    }
                );
            }
        );
    }
    
    setPageMetaData () {
        this.generalService.setTitle (this.generalService.statics.pageTitles.OUR_RECOMMENDATION);
        this.generalService.setMetaTags (this.generalService.statics.pageMeta.OUR_RECOMMENDATION);
        this.generalService.setLinkTag (this.generalService.statics.pageLinkService.OUR_RECOMMENDATION);
    }
}
