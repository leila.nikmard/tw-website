import { NotFoundComponent } from './not-found.component';

export const not_found_routes = [
    {
        path     : '404',
        component: NotFoundComponent
    }
];
