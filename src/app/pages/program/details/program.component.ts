import { Component, OnDestroy, OnInit } from '@angular/core';
import { GeneralService } from '../../../shared/general/general.service';
import { ProgramService } from '../../../shared/program/program.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Program } from '../../../shared/program/program.model';
import { Episode } from '../../../shared/episode/episode.model';
import { EpisodeService } from '../../../shared/episode/episode.service';
import { Channel } from '../../../shared/channel/channel.model';
import { ProgramRepository } from '../../../shared/program/program.repository';
import { EpisodeRepository } from '../../../shared/episode/episode.repository';
import { ChannelRepository } from '../../../shared/channel/channel.repository';


@Component ({
    selector   : 'app-program',
    templateUrl: './program.component.html',
    styleUrls  : [ './program.component.scss' ]
})
export class ProgramComponent implements OnInit, OnDestroy {
    public program                 = new Program ();
    public other_episodes: Array<Episode> = [];
    public active_channel                 = new Channel ();
    
    constructor (public generalService: GeneralService, private programService: ProgramService, private episodeService: EpisodeService,
                 private route: ActivatedRoute, private router: Router) {
        this.program = new Program();
    }
    
    ngOnInit () {
        this.route.params.subscribe ((params) => {
            this.program.setId (params[ 'id' ]);
        });
        this.getProgramPageData (false);
    }
    
    getProgramPageData (force_request: boolean = false) {
        this.generalService.requestRouterUrl (force_request).subscribe (() => {
            this.route.params.subscribe ((params) => {
                this.programService.getProgramPage (this.generalService.router_path, this.program.getId()).subscribe (
                    (result) => {
                        console.log(result);
                        this.generalService.request_body.message = result.message.toString();
                        this.generalService.request_body.code = parseInt(result.code);
                        if (this.generalService.request_body.code === 200) {
                            if(this.generalService.request_body.message != 'success') {
                                if(this.generalService.is_platform_browser){
                                    this.generalService.showMessage(this.generalService.request_body.message, 10);
                                }
                            }
                            this.program = ProgramRepository.setProgram (result.data[ 0 ]);
                            this.other_episodes = EpisodeRepository.setEpisodes (result.data[ 0 ].episodes);
                            this.active_channel = ChannelRepository.setChannel (result.data[ 0 ].channel_obj);
                            this.setPageHeaders();
                        } else {
                            this.router.navigateByUrl ('/404');
                        }
                    },
                    (error) => {
                        if (this.generalService.fetch_router_error_retry_count < 6) {
                            this.generalService.fetch_router_error_retry_count += 1;
                            this.getProgramPageData (true);
                        } else {
                            if (this.generalService.is_platform_browser) {
                                this.generalService.showMessage (this.generalService.statics.error_message_500_id, 10);
                            }
                            this.generalService.show_normal_content = false;
                        }
                    });
            });
        });
    }

    setPageHeaders() {
        this.generalService.removeMetaTags();
        this.generalService.setTitle (this.programService.setProgramPageTitle(this.program));
        this.generalService.setMetaTags (this.programService.setProgramPageMetaTags(this.program));
        this.generalService.setLinkTag (this.programService.setProgramPageLinksTags(this.program));
    }

    ngOnDestroy() {
        this.generalService.resetErrorCounts();
    }
    
}
