import { ProgramComponent } from './details/program.component';
import { OtherEpisodesComponent } from './other-episodes/other-episodes.component';

export const program_routes = [
    {
        path     : 'program/:id',
        component: ProgramComponent
    },
    {
        path     : 'program/all-episodes/:program_id',
        component: OtherEpisodesComponent
    }
];
