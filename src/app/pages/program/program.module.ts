import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgramComponent } from './details/program.component';
import { OtherEpisodesComponent } from './other-episodes/other-episodes.component';
import { RouterModule } from '@angular/router';
import { program_routes } from './program.routing';
import { SocialInfoBarModule } from '../../webparts/social-info-bar/social-info-bar.module';
import { ProgramDescriptionModule } from '../../webparts/program-description/program-description.module';
import { CarouselModule } from '../../webparts/carousel/carousel.module';
import { BreadcrumbKnowledgeGraphModule } from '../../webparts/breadcrumb-knowledge-graph/breadcrumb-knowledge-graph.module';
import { ProgramService } from '../../shared/program/program.service';
import { TagBarModule } from '../../webparts/tag-bar/tag-bar.module';

@NgModule({
    declarations: [
        ProgramComponent,
        OtherEpisodesComponent
    ],
    imports     : [
        CommonModule,
        SocialInfoBarModule,
        ProgramDescriptionModule,
        CarouselModule,
        TagBarModule,
        BreadcrumbKnowledgeGraphModule,
        RouterModule.forChild(program_routes)
    ],
    providers   : [ ProgramService ]
})
export class ProgramModule {
}
