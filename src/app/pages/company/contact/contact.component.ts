import { Component, OnInit } from '@angular/core';
import {GeneralService} from '../../../shared/general/general.service';

@Component({
    selector   : 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls  : [ './contact.component.scss' ],
})
export class ContactComponent implements OnInit {

    constructor (public generalService: GeneralService) {}

    ngOnInit () {
        this.generalService.setTitle(this.generalService.statics.pageTitles.CONTACT_US);
        this.generalService.setMetaTags(this.generalService.statics.pageMeta.CONTACT_US);
    }

}
