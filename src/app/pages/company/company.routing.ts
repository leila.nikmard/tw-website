import { DmcaComponent } from './dmca/dmca.component';
import { JobsComponent } from './jobs/jobs.component';
import { TermsComponent } from './terms/terms.component';
import { ContactComponent } from './contact/contact.component';
import { PartnersComponent } from './partners/partners.component';

export const company_routes = [
    {
        path        : 'dmca',
        component: DmcaComponent
    },
    {
        path     : 'jobs',
        component: JobsComponent
    },
    {
        path     : 'terms',
        component: TermsComponent
    },
    {
        path     : 'contact',
        component: ContactComponent
    },
    {
        path     : 'partners',
        component: PartnersComponent
    }
];
