import { Component, OnInit } from '@angular/core';
import {GeneralService} from '../../../shared/general/general.service';

@Component({
    selector   : 'app-dmca',
    templateUrl: './dmca.component.html',
    styleUrls  : [ './dmca.component.scss' ],
})
export class DmcaComponent implements OnInit {

    constructor (public generalService: GeneralService) {}

    ngOnInit () {
        this.generalService.setTitle(this.generalService.statics.pageTitles.DMCA);
        this.generalService.setMetaTags(this.generalService.statics.pageMeta.DMCA);
    }

}
