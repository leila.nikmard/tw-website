import { Component, OnInit } from '@angular/core';
import {GeneralService} from '../../../shared/general/general.service';

@Component({
    selector   : 'app-jobs',
    templateUrl: './jobs.component.html',
    styleUrls  : [ './jobs.component.scss' ],
})
export class JobsComponent implements OnInit {

    constructor (public generalService: GeneralService) {}

    ngOnInit () {
        this.generalService.setTitle(this.generalService.statics.pageTitles.JOBS);
        this.generalService.setMetaTags(this.generalService.statics.pageMeta.JOBS);
    }

}
