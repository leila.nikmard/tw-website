import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { company_routes } from './company.routing';
import { DmcaComponent } from './dmca/dmca.component';
import { JobsComponent } from './jobs/jobs.component';
import { TermsComponent } from './terms/terms.component';
import { PartnersComponent } from './partners/partners.component';
import { GeneralService } from '../../shared/general/general.service';
import { ContactComponent } from './contact/contact.component';

@NgModule({
    declarations: [
        DmcaComponent,
        JobsComponent,
        TermsComponent,
        PartnersComponent,
        ContactComponent,
    ],
    imports     : [
        FormsModule,
        HttpModule,
        CommonModule,
        RouterModule.forChild(company_routes)
    ],
    providers   : [ GeneralService ]
})

export class CompanyModule {
}
