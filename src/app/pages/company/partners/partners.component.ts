import { Component, OnInit } from '@angular/core';
import {GeneralService} from '../../../shared/general/general.service';

@Component({
  selector: 'app-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.scss']
})
export class PartnersComponent implements OnInit {

  constructor(public generalService: GeneralService) { }

  ngOnInit() {
    this.generalService.setTitle(this.generalService.statics.pageTitles.PARTNERS);
    this.generalService.setMetaTags(this.generalService.statics.pageMeta.PARTNERS);
  }

}
