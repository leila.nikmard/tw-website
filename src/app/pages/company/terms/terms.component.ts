import { Component, OnInit } from '@angular/core';
import {GeneralService} from '../../../shared/general/general.service';

@Component({
    selector   : 'app-terms',
    templateUrl: './terms.component.html',
    styleUrls  : [ './terms.component.scss' ],
})
export class TermsComponent implements OnInit {

    constructor (public generalService: GeneralService) {}

    ngOnInit () {
        this.generalService.setTitle(this.generalService.statics.pageTitles.TERMS);
        this.generalService.setMetaTags(this.generalService.statics.pageMeta.TERMS);
    }

}
