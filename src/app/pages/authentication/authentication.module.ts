import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { auth_routes } from './authentication.routing';
import { SignInWithPhoneComponent } from './sign-in-with-phone/sign-in-with-phone.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SignInWithEmailComponent } from './sign-in-with-email/sign-in-with-email.component';
import { RegisterWithEmailComponent } from './register-with-email/register-with-email.component';
import { SetPasswordComponent } from './set-password/set-password.component';

@NgModule ({
    declarations: [ SignInWithPhoneComponent, SignInWithEmailComponent, RegisterWithEmailComponent, SetPasswordComponent ],
    imports     : [
        FormsModule,
        CommonModule,
        ReactiveFormsModule,
        RouterModule.forChild (auth_routes)
    ]
})
export class AuthenticationModule {
    
}