import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../../../shared/general/general.service';
import { UserService } from '../../../shared/user/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Md5 } from 'ts-md5';
import { UserRepository } from '../../../shared/user/user.repository';

@Component ({
    selector   : 'app-set-password',
    templateUrl: './set-password.component.html',
    styleUrls  : [ './set-password.component.scss' ]
})
export class SetPasswordComponent implements OnInit {
    password                = new FormControl ('', [ Validators.required ]);
    confirm_password        = new FormControl ('', [ Validators.required ]);
    set_password: FormGroup = this.formBuilder.group ({
        password        : this.password,
        confirm_password: this.confirm_password
    });
    
    constructor (private generalService: GeneralService, private userService: UserService, private activeRoute: ActivatedRoute, private formBuilder: FormBuilder, private router: Router) {
    }
    
    ngOnInit () {
        this.generalService.setTitle ('درخواست بازیابی رمز عبور');
    }
    
    setPassword (value) {
        if (value.password === value.confirm_password) {
            this.generalService.requestRouterUrl (false).subscribe (
                () => {
                    this.generalService.setCookie (this.generalService.configs.variableNames.user_token, this.activeRoute.snapshot.params.id);
                    this.userService.resetPassword (this.generalService.router_path, <string>Md5.hashStr (value.password)).subscribe (
                        (result) => {
                            if (result.status === 200) {
                                let body = result.body;
                                if (body.code === 200) {
                                    if (body.data[ 0 ].status_code === 1) {
                                        this.userService.getUserInfo (this.generalService.router_path).subscribe (
                                            (result) => {
                                                if (result.code === 200) {
                                                    this.generalService.current_user = UserRepository.setUser (result.data[ 0 ].user_info);
                                                }
                                            }
                                        );
                                        this.userService.user_is_sign_in = true;
                                        this.router.navigate ([ '/' ]);
                                    }
                                }
                            }
                        }
                    );
                }
            );
        }
    }
    
}
