import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../../../shared/general/general.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../../shared/user/user.service';
import { Router } from '@angular/router';
import { UserRepository } from '../../../shared/user/user.repository';

@Component ({
    selector   : 'app-sign-up',
    templateUrl: './sign-in-with-phone.component.html',
    styleUrls  : [ './sign-in-with-phone.component.scss' ]
})
export class SignInWithPhoneComponent implements OnInit {
    is_sign_in_form   = true;
    is_verify_section = false;
    
    name                    = new FormControl ('', [
        Validators.required
    ]);
    phone                   = new FormControl ('', [
        Validators.required
    ]);
    sign_in_form: FormGroup = this.formBuilder.group ({
        name : this.name,
        phone: this.phone
    });
    
    code                   = new FormControl ('', [
        Validators.required
    ]);
    verify_form: FormGroup = this.formBuilder.group ({
        code: this.code
    });
    
    constructor (private generalService: GeneralService, private formBuilder: FormBuilder, private userService: UserService, private router: Router) {
    }
    
    ngOnInit () {
        this.generalService.setTitle ('ورود به حساب کاربری');
    }
    
    signIn (value) {
        this.generalService.requestRouterUrl (false).subscribe (
            () => {
                this.userService.signInUserWithPhone (this.generalService.router_path, {
                    'phone': value.phone,
                    'name' : value.name
                }).subscribe (
                    (result) => {
                        if (result.status === 200) {
                            let body    = result.body;
                            let headers = result.headers;
                            this.generalService.updateUserToken (headers);
                            if (body.code === 200) {
                                if (body.data[ 0 ].status_code === 1) {
                                    this.is_sign_in_form   = false;
                                    this.is_verify_section = true;
                                } else if (body.data[ 0 ].status_code === 3) {
                                }
                            }
                        }
                    }
                );
            }
        );
    }
    
    verifyPhone (value) {
        this.generalService.requestRouterUrl (false).subscribe (
            () => {
                this.userService.verifyPhone (this.generalService.router_path, {
                    'verify_code': value.code
                }).subscribe (
                    (result) => {
                        if (result.status === 200) {
                            let body    = result.body;
                            let headers = result.headers;
                            this.generalService.updateUserToken (headers);
                            if (body.code === 200) {
                                if (body.data[ 0 ].status_code === 1) {
                                    this.userService.getUserInfo (this.generalService.router_path).subscribe (
                                        (result) => {
                                            if (result.code === 200) {
                                                this.generalService.current_user = UserRepository.setUser (result.data[ 0 ].user_info);
                                            }
                                        }
                                    );
                                    this.userService.user_is_sign_in = true;
                                    this.router.navigate ([ '/' ]);
                                }
                            }
                        }
                    }
                );
            }
        );
    }
}
