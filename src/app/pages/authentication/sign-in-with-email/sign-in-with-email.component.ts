import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../../../shared/general/general.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../../shared/user/user.service';
import { Md5 } from 'ts-md5';
import { Router } from '@angular/router';
import { UserRepository } from '../../../shared/user/user.repository';

@Component ({
    selector   : 'app-sign-in-with-email',
    templateUrl: './sign-in-with-email.component.html',
    styleUrls  : [ './sign-in-with-email.component.scss' ]
})
export class SignInWithEmailComponent implements OnInit {
    email                   = new FormControl ('', [ Validators.required ]);
    password                = new FormControl ('', [ Validators.required ]);
    sign_in_form: FormGroup = this.formBuilder.group ({
        email   : this.email,
        password: this.password
    });
    
    constructor (private generalService: GeneralService, private formBuilder: FormBuilder, private userService: UserService, private router: Router) {
    }
    
    ngOnInit () {
        this.generalService.setTitle ('ثبت‌‌نام و ورود با ایمیل');
    }
    
    signIn (value) {
        this.generalService.requestRouterUrl (false).subscribe (
            () => {
                this.userService.signInUserWithEmail (this.generalService.router_path, {
                    'email'   : value.email,
                    'password': <string>Md5.hashStr (value.password)
                }).subscribe (
                    (result) => {
                        if (result.status === 200) {
                            let body = result.body;
                            this.generalService.updateUserToken (result.headers);
                            if (body.code === 200) {
                                if (body.data[ 0 ].status_code === 1) {
                                    this.userService.getUserInfo (this.generalService.router_path).subscribe (
                                        (result) => {
                                            if (result.code === 200) {
                                                this.generalService.current_user = UserRepository.setUser (result.data[ 0 ].user_info);
                                            }
                                        }
                                    );
                                    this.userService.user_is_sign_in = true;
                                    this.router.navigate ([ '/' ]);
                                }
                            }
                        }
                    }
                );
            }
        );
    }
    
}
