import { SignInWithPhoneComponent } from './sign-in-with-phone/sign-in-with-phone.component';
import { SignInWithEmailComponent } from './sign-in-with-email/sign-in-with-email.component';
import { RegisterWithEmailComponent } from './register-with-email/register-with-email.component';
import { SetPasswordComponent } from './set-password/set-password.component';

export const auth_routes = [
    {
        path     : 'signin',
        component: SignInWithPhoneComponent
    },
    {
        path     : 'signin/email',
        component: SignInWithEmailComponent
    },
    {
        path     : 'register/email',
        component: RegisterWithEmailComponent
    },
    {
        path     : 'setPassword/:id',
        component: SetPasswordComponent
    }
];