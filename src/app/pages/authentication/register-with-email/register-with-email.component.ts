import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../../../shared/general/general.service';
import { UserService } from '../../../shared/user/user.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component ({
    selector   : 'app-register-with-email',
    templateUrl: './register-with-email.component.html',
    styleUrls  : [ './register-with-email.component.scss' ]
})
export class RegisterWithEmailComponent implements OnInit {
    name                     = new FormControl ('', [ Validators.required ]);
    email                    = new FormControl ('', [ Validators.required ]);
    register_form: FormGroup = this.formBuilder.group ({
        name : this.name,
        email: this.email
    });
    
    constructor (private generalService: GeneralService, private userService: UserService, private formBuilder: FormBuilder) {
    }
    
    ngOnInit () {
        this.generalService.setTitle ('ثبت‌‌نام و ورود با ایمیل');
    }
    
    register (value) {
        this.generalService.requestRouterUrl (false).subscribe (
            () => {
                this.userService.registerEmail (this.generalService.router_path, value.name, value.email).subscribe (
                    (result) => {
                        if (result.code === 200) {
                            if (result.data[ 0 ].status_code === 1) {
                            }
                        }
                    }
                );
            }
        );
    }
}
