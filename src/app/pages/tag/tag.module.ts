import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TagComponent } from './tag.component';
import { RouterModule } from '@angular/router';
import { tag_route } from './tag.routing';
import { ListViewModule } from '../../webparts/list-view/list-view.module';

@NgModule({
    imports     : [
        CommonModule,
        ListViewModule,
        RouterModule.forChild(tag_route)
    ],
    declarations: [ TagComponent ]
})
export class TagModule {
}
