import { TagComponent } from './tag.component';

export const tag_route = [
    {
        path     : 'tag/:descriptor',
        component: TagComponent
    }
];