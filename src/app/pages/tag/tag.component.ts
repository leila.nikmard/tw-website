import { Component, OnInit } from '@angular/core';
import { ProgramService } from '../../shared/program/program.service';
import { GeneralService } from '../../shared/general/general.service';
import { ActivatedRoute } from '@angular/router';
import { Program } from '../../shared/program/program.model';
import { ProgramRepository } from '../../shared/program/program.repository';
import { EpisodeService } from '../../shared/episode/episode.service';
import { Episode } from '../../shared/episode/episode.model';
import { EpisodeRepository } from '../../shared/episode/episode.repository';

@Component ({
    selector   : 'app-tag',
    templateUrl: './tag.component.html',
    styleUrls  : [ './tag.component.scss' ]
})
export class TagComponent implements OnInit {
    public tag_programs_page_number     = 0;
    public tag_episodes_page_number     = 0;
    public descriptor: string;
    public all_programs: Array<Program> = [];
    public all_episodes: Array<Program> = [];
    
    constructor (public generalService: GeneralService, private programService: ProgramService, private episodeService: EpisodeService, private route: ActivatedRoute) {
    }
    
    ngOnInit () {
        this.route.params.subscribe (
            (params) => {
                this.descriptor = params.descriptor;
            }
        );
        this.getTagPrograms (false, this.tag_programs_page_number);
        this.getTagEpisodes (false, this.tag_episodes_page_number);
    }
    
    getTagPrograms (force_request: boolean, page) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            () => {
                this.programService.getTagPrograms (this.generalService.router_path, page, this.descriptor).subscribe (
                    (result) => {
                        let programs: Array<Program> = [];
                        if (result.code === 200) {
                            programs          = ProgramRepository.setPrograms (result.data);
                            this.all_programs = this.generalService.mergeOneArrayInAnother (programs, this.all_programs);
                        }
                    },
                    () => {
                        if (this.programService.fetch_get_tag_error_retry_count < 6) {
                            this.programService.fetch_get_tag_error_retry_count += 1;
                            this.getTagPrograms (true, page);
                        } else {
                            if (!this.generalService.is_platform_server) {
                                this.generalService.showMessage (this.generalService.statics.error_message_500_id, 10);
                            }
                            this.generalService.show_normal_content = false;
                        }
                    },
                    () => {
                        this.setPageMetaData ();
                    }
                );
            }
        );
    }
    
    setPageMetaData () {
        this.generalService.setTitle (this.descriptor + ' | برچسب');
        //        this.generalService.setLinkTag([
        //            {
        //                rel : 'canonical',
        //                href: this.generalService.base_domain + 'live/' + this.channel.getDescriptor()
        //            }
        //        ]);
        //        this.generalService.setMetaTags([
        //            //            {
        //            //                name   : 'keywords',
        //            //                content: this.liveService.livepage_keywords + this.channelTransform.transform(this.channel)
        //            //            },
        //            //            {
        //            //                name   : 'description',
        //            //                content: this.liveService.getLivePageDescription(this.channelTransform.transform(this.channel))
        //            //            },
        //            //            {
        //            //                name   : 'title',
        //            //                content: ' پخش زنده ' + this.channel.name
        //            //            }
        //        ]);
    }
    
    getTagEpisodes (force_request: boolean, page) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            () => {
                this.episodeService.getTagEpisodes (this.generalService.router_path, page, this.descriptor).subscribe (
                    (result) => {
                        let episodes: Array<Episode> = [];
                        if (result.code === 200) {
                            episodes          = EpisodeRepository.setEpisodes (result.data);
                            this.all_episodes = this.generalService.mergeOneArrayInAnother (episodes, this.all_episodes);
                        }
                    },
                    () => {
                        if (this.episodeService.fetch_get_tag_episode_error_retry_count < 6) {
                            this.episodeService.fetch_get_tag_episode_error_retry_count += 1;
                            this.getTagEpisodes (true, page);
                        } else {
                            if (!this.generalService.is_platform_server) {
                                this.generalService.showMessage (this.generalService.statics.error_message_500_id, 10);
                            }
                            this.generalService.show_normal_content = false;
                        }
                    }
                );
            }
        );
    }
    
    loadMorePrograms () {
        this.getTagPrograms (false, ++this.tag_programs_page_number);
    }
    
    loadMoreEpisodes () {
        this.getTagEpisodes (false, ++this.tag_episodes_page_number);
    }
    
}
