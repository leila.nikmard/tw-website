import { Component, OnDestroy, OnInit } from '@angular/core';
import { GeneralService } from '../../../shared/general/general.service';
import { EpisodeService } from '../../../shared/episode/episode.service';
import { Episode } from '../../../shared/episode/episode.model';
import { EpisodeRepository } from '../../../shared/episode/episode.repository';
import { Promotion } from '../../../shared/promotion/promotion.model';
import { PromotionService } from '../../../shared/promotion/promotion.service';
import { PromotionRepository } from '../../../shared/promotion/promotion.repository';

@Component ({
    selector   : 'app-movies',
    templateUrl: './movies.component.html',
    styleUrls  : [ './movies.component.scss' ]
})
export class MoviesComponent implements OnInit, OnDestroy {
    
    all_movies: Array<Episode>          = [];
    page                        = 0;
    movies_promotions: Array<Promotion> = [];
    
    constructor (public generalService: GeneralService, private episodeService: EpisodeService, private promotionService: PromotionService) {
    }
    
    ngOnInit () {
        this.setPageHeaders();
        this.getMoviesPromotions (false, 6);
        this.getMovies (false);
    }
    
    getMoviesPromotions (force_request: boolean, type: number) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            (router) => {
                this.promotionService.getPromotions (this.generalService.router_path, type, '_FOR_MOVIES').subscribe (
                    (result) => {
                        if (result.code === 200) {
                            this.movies_promotions = PromotionRepository.setPromotions (result.data);
                        }
                    },
                    () => {
                        if (this.promotionService.fetch_get_movies_promotion_error_retry_count < 6) {
                            this.promotionService.fetch_get_movies_promotion_error_retry_count += 1;
                            this.getMoviesPromotions (true, type);
                        } else {
                            if (!this.generalService.is_platform_server) {
                                this.generalService.showMessage (this.generalService.statics.error_message_500_sub_queries_id, 10);
                            }
                            this.generalService.show_normal_content = false;
                        }
                    }
                );
            }
        );
    }
    
    getMovies (force_request: boolean) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            (router) => {
                this.episodeService.getEpisodesByCategoryGenre (this.generalService.router_path,
                    [ this.generalService.statics.pageCategoryIds.movies_first_category_id, this.generalService.statics.pageCategoryIds.movies_second_category_id ],
                    this.page, this.generalService.configs.variableNames.movies + this.page).subscribe (
                    (result) => {
                        let movies: Array<Episode> = [];
                        if (result.code === 200) {
                            movies          = EpisodeRepository.setEpisodes (result.data);
                            this.all_movies = this.generalService.mergeOneArrayInAnother (movies, this.all_movies);
                        }
                    },
                    () => {
                        if (this.generalService.fetch_router_error_retry_count < 6) {
                            this.generalService.fetch_router_error_retry_count += 1;
                            this.getMovies (true);
                        } else {
                            if (this.generalService.is_platform_browser) {
                                this.generalService.showMessage (this.generalService.statics.error_message_500_id, 10);
                            }
                            this.generalService.show_normal_content = false;
                        }
                    }
                );
            }
        );
    }
    
    loadMore () {
        this.page += 1;
        this.getMovies (false);
    }

    setPageHeaders() {
        this.generalService.removeMetaTags();
        this.generalService.setTitle (this.generalService.statics.pageTitles.MOVIES);
        this.generalService.setMetaTags (this.generalService.statics.pageMeta.MOVIES);
        this.generalService.setLinkTag (this.generalService.statics.pageLinkService.MOVIES);
    }

    ngOnDestroy(){
        this.generalService.resetErrorCounts();
    }
}
