import { Component, OnDestroy, OnInit } from '@angular/core';
import { Episode } from '../../../shared/episode/episode.model';
import { GeneralService } from '../../../shared/general/general.service';
import { EpisodeRepository } from '../../../shared/episode/episode.repository';
import { FavoriteListService } from '../../../shared/favorite_list/favorite-list.service';

@Component ({
    selector   : 'app-recommendation',
    templateUrl: './recommendation.component.html',
    styleUrls  : [ './recommendation.component.scss' ]
})
export class RecommendationComponent implements OnInit, OnDestroy {
    
    public episodes: Array<Episode> = [];
    public page: number;
    
    constructor (public generalService: GeneralService, private favoriteListService: FavoriteListService) {
        this.page = 0;
    }
    
    getPageData (force_request: boolean) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            () => {
                this.favoriteListService.getFavoriteListEpisodes (this.generalService.router_path, 404965, this.page, 'newest').subscribe (
                    (result) => {
                        this.episodes = this.generalService.mergeOneArrayInAnother (EpisodeRepository.setEpisodes (
                            result.data), this.episodes);
                    },
                    () => {
                        if (this.generalService.fetch_router_error_retry_count < 6) {
                            this.generalService.fetch_router_error_retry_count += 1;
                            this.getPageData (true);
                        } else {
                            if (this.generalService.is_platform_browser) {
                                this.generalService.showMessage (this.generalService.statics.error_message_500_id, 10);
                            }
                            this.generalService.show_normal_content = false;
                        }
                    },
                    () => {
                        this.setPageMetaData ();
                    }
                );
            }
        );
    }
    
    loadMore () {
        this.page++;
        this.getPageData (false);
    }
    
    setPageMetaData () {
        this.generalService.setTitle (this.generalService.statics.pageTitles.OUR_RECOMMENDATION);
        this.generalService.setMetaTags (this.generalService.statics.pageMeta.OUR_RECOMMENDATION);
        this.generalService.setLinkTag (this.generalService.statics.pageLinkService.OUR_RECOMMENDATION);
    }
    
    ngOnInit () {
        this.getPageData (false);
    }

    ngOnDestroy(){
        this.generalService.resetErrorCounts();
    }
    
}
