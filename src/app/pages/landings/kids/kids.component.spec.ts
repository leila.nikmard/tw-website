//import { async, ComponentFixture, TestBed } from '@angular/core/testing';
//import { KidsComponent } from './kids.component';
//import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
//import { RouterModule } from '@angular/router';
//import { NumberToPersianPipe } from '../../pipes/number-to-persian.pipe';
//import { HttpModule } from '@angular/http';
//import { AnalyticsComponentService } from '../../shared/analytics/analytics-component.service';
//import { JalaliDateService } from '../../services/jalali-date.service';
//import { AppGlobals } from '../../globals/globals';
//import { RouterTestingModule } from '@angular/router/testing';
//import { Program } from '../../shared/program/program';
//import { MockBackend } from '@angular/http/testing';
//
//describe('Kids Component Testing', () => {
//  let fixture: ComponentFixture<KidsComponent>;
//  let component: KidsComponent;
//  let el: HTMLElement;
//
//  beforeEach(async(() => {
//    TestBed.configureTestingModule({
//      declarations: [ KidsComponent, NumberToPersianPipe ],
//      schemas     : [ CUSTOM_ELEMENTS_SCHEMA ],
//      imports     : [ RouterModule, HttpModule, RouterTestingModule ],
//      providers   : [
//        {
//          provide : AnalyticsComponentService,
//          useClass: MockBackend
//        },
//        {
//          provide : JalaliDateService,
//          useClass: MockBackend
//        },
//        {
//          provide : AppGlobals,
//          useClass: MockBackend
//        }
//      ]
//    });
//  }));
//
//  beforeEach(() => {
//    fixture   = TestBed.createComponent(KidsComponent);
//    component = fixture.componentInstance;
//    el        = fixture.nativeElement;
//
//    component.kidsEpisodes     = [];
//    let kid1                   = new Program();
//    kid1.id                    = 59741;
//    kid1.title                 = 'داستان های آن شرلی';
//    kid1.cover_image_name      = 'http://static.televebion.net/web/content_images/program_images/cover_images/thumbs/2018-08-21/240_7bd15a6d58dc74207f89ce919ff52782.png';
//    kid1.background_image_name = 'http://static.televebion.net/web/content_images/program_images/background_images/thumbs/2018-08-21/220_7bd15a6d58dc74207f89ce919ff52782.png';
//
//
//    let kid2                   = new Program();
//    kid2.id                    = 59737;
//    kid2.title                 = 'دالتون ها';
//    kid2.cover_image_name      = 'http://static.televebion.net/web/content_images/program_images/cover_images/thumbs/2018-08-19/240_225ec06f8fe542cd04d824c79beeb5e7.png';
//    kid2.background_image_name = 'http://static.televebion.net/web/content_images/program_images/background_images/thumbs/2018-08-19/220_225ec06f8fe542cd04d824c79beeb5e7.png';
//
//    component.kidsEpisodes.push(kid1, kid2);
//    fixture.detectChanges();
//  });
//
//  it('Should initialize component without any error', async(() => {
//    expect(component).toBeTruthy();
//  }));
//
//  it('H1 rendering test', async(() => {
//    let h1 = ' دانلود و تماشای آنلاین کارتن ، انیمیشن و برنامه کودک';
//    expect(el.querySelector('h1').textContent).toBe(h1);
//  }));
//
//  describe('Items rendering test', () => {
//    it('Items correctly rendered', async(() => {
//      expect(el.querySelector('ul').children.length).toEqual(component.kidsEpisodes.length);
//    }));
//
//    it('Items title test', async(() => {
//      expect(el.querySelector('ul li a').getAttribute('title')).toBe(component.kidsEpisodes[ 0 ].title);
//    }));
//
//    it('Items href test', async(() => {
//      expect(el.querySelector('ul li a').getAttribute('href'))
//        .toBe('/program/' + component.kidsEpisodes[ 0 ].id);
//    }));
//  });
//});