import { Component, OnDestroy, OnInit } from '@angular/core';
import { GeneralService } from '../../../shared/general/general.service';
import { LinkService } from '../../../shared/general/link.service';
import { Program } from '../../../shared/program/program.model';
import { Promotion } from '../../../shared/promotion/promotion.model';
import { PromotionService } from '../../../shared/promotion/promotion.service';
import { PromotionRepository } from '../../../shared/promotion/promotion.repository';
import { ProgramService } from '../../../shared/program/program.service';
import { ProgramRepository } from '../../../shared/program/program.repository';

@Component ({
    selector   : 'app-kids',
    templateUrl: './kids.component.html',
    styleUrls  : [ './kids.component.scss' ],
    providers  : [ GeneralService, LinkService ]
})
export class KidsComponent implements OnInit, OnDestroy {
    
    all_cartoons: Array<Program>         = [];
    page                         = 0;
    cartoons_promotion: Array<Promotion> = [];
    
    constructor (public generalService: GeneralService, private promotionService: PromotionService, private  programService: ProgramService) {}
    
    ngOnInit () {
        this.setPageHeaders();
        this.getKidsPromotion (false, 7);
        this.getKids (false);
    }
    
    getKidsPromotion (force_request: boolean, type: number) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            (router) => {
                this.promotionService.getPromotions (this.generalService.router_path, type, '_FOR_KIDS').subscribe (
                    (result) => {
                        if (result.code === 200) {
                            this.cartoons_promotion = PromotionRepository.setPromotions (result.data);
                        }
                    },
                    () => {
                        if (this.generalService.fetch_sub_content_router_error_retry_count < 6) {
                            this.generalService.fetch_sub_content_router_error_retry_count += 1;
                            this.getKidsPromotion (true, type);
                        } else {
                            if (this.generalService.is_platform_browser) {
                                this.generalService.showMessage (this.generalService.statics.error_message_500_sub_queries_id, 10);
                            }
                            this.generalService.sub_content_is_loaded = false;
                        }
                    }
                );
            }
        );
        
    }
    
    getKids (force_request: boolean) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            (router) => {
                this.programService.getProgramByCategoryGenre (this.generalService.router_path,
                    [ this.generalService.statics.pageCategoryIds.kids_first_category_id,
                        this.generalService.statics.pageCategoryIds.kids_second_category_id ],
                    this.page, this.generalService.configs.variableNames.kids + this.page).subscribe (
                    (result) => {
                        let cartoons: Array<Program> = [];
                        if (result.code === 200) {
                            cartoons          = ProgramRepository.setPrograms (result.data);
                            this.all_cartoons = this.generalService.mergeOneArrayInAnother (cartoons, this.all_cartoons);
                        }
                    },
                    () => {
                        if (this.generalService.fetch_router_error_retry_count < 6) {
                            this.generalService.fetch_router_error_retry_count += 1;
                            this.getKids (true);
                        } else {
                            if (this.generalService.is_platform_browser) {
                                this.generalService.showMessage (this.generalService.statics.error_message_500_id, 10);
                            }
                            this.generalService.show_normal_content = false;
                        }
                    }
                );
            }
        );
    }
    
    loadMore () {
        this.page +=1;
        this.getKids (false);
    }

    setPageHeaders() {
        this.generalService.removeMetaTags();
        this.generalService.setTitle (this.generalService.statics.pageTitles.KIDS);
        this.generalService.setMetaTags (this.generalService.statics.pageMeta.KIDS);
        this.generalService.setLinkTag (this.generalService.statics.pageLinkService.KIDS);
    }

    ngOnDestroy(){
        this.generalService.resetErrorCounts();
    }
}
