import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { GeneralService } from '../../../shared/general/general.service';
import { HomeService } from '../../../shared/home/home.service';
import { Episode } from '../../../shared/episode/episode.model';
import { Channel } from '../../../shared/channel/channel.model';
import { Program } from '../../../shared/program/program.model';
import { EpisodeRepository } from '../../../shared/episode/episode.repository';
import { ChannelRepository } from '../../../shared/channel/channel.repository';
import { ProgramRepository } from '../../../shared/program/program.repository';
import { Promotion } from '../../../shared/promotion/promotion.model';
import { PromotionRepository } from '../../../shared/promotion/promotion.repository';

@Component ({
    selector   : 'app-home',
    templateUrl: './home.component.html',
    styleUrls  : [ './home.component.scss' ]
})

export class HomeComponent implements OnInit, OnDestroy {
    
    public movies: Array<Episode>              = [];
    public custom2: Array<Episode>             = [];
    public custom: Array<Episode>              = [];
    public our_recommendation: Array<Episode>  = [];
    public user_recommendation: Array<Episode> = [];
    public kids: Array<Program>                = [];
    public channels: Array<Channel>            = [];
    public promotions: Array<Promotion>        = [];
    public series: Array<Program>              = [];
    
    constructor (public generalService: GeneralService, private homeService: HomeService) {
    }
    
    ngOnInit () {
        this.setPageHeaders();
        this.getHomePageData (false);
    }
    
    getHomePageData (force_request: boolean) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            () => {
                this.homeService.getHomePage (this.generalService.router_path).subscribe (
                    (result) => {
                        this.generalService.request_body.message = result.message.toString();
                        this.generalService.request_body.code = parseInt(result.code);
                        if (this.generalService.request_body.code === 200) {
                            if(this.generalService.request_body.message != 'success') {
                                if(this.generalService.is_platform_browser){
                                    this.generalService.showMessage(this.generalService.request_body.message, 10);
                                }
                            }
                            this.promotions          = PromotionRepository.setPromotions (result.data[ 0 ].promotions);
                            this.channels            = ChannelRepository.setChannels (result.data[ 0 ].channels);
                            this.our_recommendation  = EpisodeRepository.setEpisodes ((result.data[ 0 ].our_recommendation));
                            this.custom2             = EpisodeRepository.setEpisodes ((result.data[ 0 ].custom2));
                            this.custom              = EpisodeRepository.setEpisodes ((result.data[ 0 ].custom));
                            this.movies              = EpisodeRepository.setEpisodes (result.data[ 0 ].movies);
                            this.series              = ProgramRepository.setPrograms (result.data[ 0 ].tv_series);
                            this.user_recommendation = EpisodeRepository.setEpisodes ((result.data[ 0 ].user_recommendation));
                            this.kids                = ProgramRepository.setPrograms ((result.data[ 0 ].kids));
                        }
                    },
                    () => {
                        if (this.generalService.fetch_router_error_retry_count < 6) {
                            this.generalService.fetch_router_error_retry_count += 1;
                            this.getHomePageData (true);
                        }else{
                            if (this.generalService.is_platform_browser) {
                                this.generalService.showMessage (this.generalService.statics.error_message_500_id, 10);
                            }
                            this.generalService.show_normal_content = false;
                        }
                    }
                );
            }
        );
    }
    
    scrollToItem ($event) {
        if (this.generalService.is_platform_browser) {
            document.getElementById ($event.toString ()).scrollIntoView ({ behavior: 'smooth' });
        }
    }

    setPageHeaders() {
        this.generalService.removeMetaTags();
        this.generalService.setTitle (this.generalService.statics.pageTitles.HOME_PAGE);
        this.generalService.setMetaTags (this.generalService.statics.pageMeta.HOME_PAGE);
        this.generalService.setLinkTag (this.generalService.statics.pageLinkService.HOME_PAGE);
    }

    ngOnDestroy() {
        this.generalService.resetErrorCounts();
    }
}
