import { SeriesComponent } from './series/series.component';
import { RecommendationComponent } from './recommendation/recommendation.component';
import { NewsComponent } from './news/news.component';
import { NewestComponent } from './newest/newest.component';
import { MoviesComponent } from './movies/movies.component';
import { KidsComponent } from './kids/kids.component';
import { HottestComponent } from './hottest/hottest.component';
import { HomeComponent } from './home/home.component';

export const landing_routes = [
    {
        path     : '',
        component: HomeComponent
    },
    {
        path     : 'series',
        component: SeriesComponent
    },
    {
        path     : 'recommendation',
        component: RecommendationComponent
    },
    {
        path     : 'movies',
        component: MoviesComponent
    },
    {
        path     : 'newest',
        component: NewestComponent
    },
    {
        path     : 'kids',
        component: KidsComponent
    },
    {
        path     : 'hottest',
        component: HottestComponent
    },
    {
        path     : 'news',
        component: NewsComponent
    }
];
