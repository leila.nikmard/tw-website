import { Component, OnInit } from '@angular/core';
import { Episode } from '../../../shared/episode/episode.model';
import { GeneralService } from '../../../shared/general/general.service';
import { EpisodeRepository } from '../../../shared/episode/episode.repository';
import { EpisodeService } from '../../../shared/episode/episode.service';

@Component ({
    selector   : 'app-news',
    templateUrl: './news.component.html',
    styleUrls  : [ './news.component.scss' ]
})
export class NewsComponent implements OnInit {
    
    public episodes: Array<Episode> = [];
    public page: number;
    
    constructor (public generalService: GeneralService, private episodeService: EpisodeService) {
        this.page = 0;
    }
    
    ngOnInit () {
        this.setPageHeaders();
        this.getPageData (false);
    }
    
    getPageData (force_request: boolean) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            () => {
                this.episodeService.getEpisodesByCategoryGenre (this.generalService.router_path, [ 7 ], this.page, this.page).subscribe (
                    (result) => {
                        this.episodes = this.generalService.mergeOneArrayInAnother (EpisodeRepository.setEpisodes (
                            result.data), this.episodes);
                    },
                    () => {
                        if (this.generalService.fetch_router_error_retry_count < 6) {
                            this.generalService.fetch_router_error_retry_count += 1;
                            this.getPageData (true);
                        } else {
                            if (this.generalService.is_platform_browser) {
                                this.generalService.showMessage (this.generalService.statics.error_message_500_id, 10);
                            }
                            this.generalService.show_normal_content = false;
                        }
                    },
                    () => {
                    }
                );
            }
        );
    }

    setPageHeaders () {
        this.generalService.setTitle (this.generalService.statics.pageTitles.NEWS);
        this.generalService.setMetaTags (this.generalService.statics.pageMeta.NEWS);
        this.generalService.setLinkTag (this.generalService.statics.pageLinkService.NEWS);
    }
    
    loadMore () {
        this.page++;
        this.getPageData (false);
    }
}
