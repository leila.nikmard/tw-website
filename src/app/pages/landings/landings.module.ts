import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { landing_routes } from './landings.routing';
import { SeriesComponent } from './series/series.component';
import { RecommendationComponent } from './recommendation/recommendation.component';
import { KidsComponent } from './kids/kids.component';
import { MoviesComponent } from './movies/movies.component';
import { NewestComponent } from './newest/newest.component';
import { HomeComponent } from './home/home.component';
import { HottestComponent } from './hottest/hottest.component';
import { NewsComponent } from './news/news.component';
import { PromotionCarouselModule } from '../../webparts/promotion-carousel/promotion-carousel.module';
import { CarouselModule } from '../../webparts/carousel/carousel.module';
import { NumberToPersianModule } from '../../pipes/numberToPersian/numberToPersian.module';
import { HomeService } from '../../shared/home/home.service';
import { ListViewModule } from '../../webparts/list-view/list-view.module';
import { PromotionService } from '../../shared/promotion/promotion.service';
import { SectionsModule } from '../../webparts/sections/sections.module';
import { InYourPocketModule } from '../../webparts/in-your-pocket/in-your-pocket.module';

@NgModule ({
    schemas     : [ CUSTOM_ELEMENTS_SCHEMA ],
    declarations: [
        HomeComponent,
        HottestComponent,
        KidsComponent,
        MoviesComponent,
        NewestComponent,
        NewsComponent,
        RecommendationComponent,
        SeriesComponent
    ],
    imports     : [
        FormsModule,
        HttpModule,
        CommonModule,
        PromotionCarouselModule,
        CarouselModule,
        SectionsModule,
        NumberToPersianModule,
        ListViewModule,
        PromotionCarouselModule,
        RouterModule.forChild (landing_routes),
        InYourPocketModule
    ],
    providers   : [ HomeService, PromotionService ]
})
export class LandingsModule {
}
