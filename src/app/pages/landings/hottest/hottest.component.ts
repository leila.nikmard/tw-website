import { Component, OnDestroy, OnInit } from '@angular/core';
import { GeneralService } from '../../../shared/general/general.service';
import { ProgramService } from '../../../shared/program/program.service';
import { Episode } from '../../../shared/episode/episode.model';
import { EpisodeRepository } from '../../../shared/episode/episode.repository';

@Component ({
    selector   : 'app-hottest',
    templateUrl: './hottest.component.html',
    styleUrls  : [ './hottest.component.scss' ]
})
export class HottestComponent implements OnInit, OnDestroy {
    
    public episodes: Array<Episode> = [];
    public page                     = 0;
    
    constructor (public generalService: GeneralService, private programService: ProgramService) {
    }

    ngOnInit () {
        this.setPageHeaders();
        this.getPageData (false);
    }
    
    getPageData (force_request: boolean) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            () => {
                this.programService.getMostViewedProgram (this.generalService.router_path, this.page).subscribe (
                    (result) => {
                        this.generalService.request_body.message = result.message.toString();
                        this.generalService.request_body.code = parseInt(result.code);
                        if (this.generalService.request_body.code === 200) {
                            let hottest = EpisodeRepository.setEpisodes (result.data);
                            this.episodes = this.generalService.mergeOneArrayInAnother(hottest, this.episodes);
                        }
                    },
                    () => {
                        if (this.generalService.fetch_router_error_retry_count < 6) {
                            this.generalService.fetch_router_error_retry_count += 1;
                            this.getPageData (true);
                        } else {
                            if (!this.generalService.is_platform_server) {
                                this.generalService.showMessage (this.generalService.statics.error_message_500_id, 10);
                            }
                            this.generalService.show_normal_content = false;
                        }
                    },
                    () => {
                    }
                );
            }
        );
    }
    
    loadMore () {
        this.page++;
        this.getPageData (false);
    }

    setPageHeaders() {
        this.generalService.removeMetaTags();
        this.generalService.setTitle (this.generalService.statics.pageTitles.HOTTEST);
        this.generalService.setMetaTags (this.generalService.statics.pageMeta.HOTTEST);
        this.generalService.setLinkTag (this.generalService.statics.pageLinkService.HOTTEST);
    }

    ngOnDestroy() {
        this.generalService.resetErrorCounts();
    }
    
}
