import { Component, OnDestroy, OnInit } from '@angular/core';
import { GeneralService } from '../../../shared/general/general.service';
import { ProgramService } from '../../../shared/program/program.service';
import { Program } from '../../../shared/program/program.model';
import { ProgramRepository } from '../../../shared/program/program.repository';
import { Promotion } from '../../../shared/promotion/promotion.model';
import { PromotionService } from '../../../shared/promotion/promotion.service';
import { PromotionRepository } from '../../../shared/promotion/promotion.repository';

@Component ({
    selector   : 'app-series',
    templateUrl: './series.component.html',
    styleUrls  : [ './series.component.scss' ]
})
export class SeriesComponent implements OnInit, OnDestroy {
    
    all_series: Array<Program>         = [];
    page                       = 0;
    series_promotion: Array<Promotion> = [];
    
    constructor (public generalService: GeneralService, private programService: ProgramService, private promotionService: PromotionService) {}
    
    ngOnInit () {
        this.setPageHeaders();
        this.getSeriesPromotion (false, 5);
        this.getSeries (false);
    }
    
    getSeriesPromotion (force_request: boolean, type: number) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            (router) => {
                this.promotionService.getPromotions (this.generalService.router_path, type, '_FOR_SERIES').subscribe (
                    (result) => {
                        if (result.code === 200) {
                            this.series_promotion = PromotionRepository.setPromotions (result.data);
                        }
                    },
                    () => {
                        if (this.generalService.fetch_sub_content_router_error_retry_count < 6) {
                            this.generalService.fetch_router_error_retry_count += 1;
                            this.getSeriesPromotion (true, type);
                        } else {
                            if (this.generalService.is_platform_browser) {
                                this.generalService.showMessage (this.generalService.statics.error_message_500_sub_queries_id, 10);
                            }
                            this.generalService.sub_content_is_loaded = false;
                        }
                    }
                );
            }
        );
    }
    
    getSeries (force_request: boolean) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            (router) => {
                this.programService.getProgramByCategoryGenre (this.generalService.router_path,
                    [ this.generalService.statics.pageCategoryIds.series_first_category_id,
                        this.generalService.statics.pageCategoryIds.series_second_category_id ],
                    this.page, this.generalService.configs.variableNames.series + this.page).subscribe (
                    (result) => {
                        let series: Array<Program> = [];
                        if (result.code === 200) {
                            series          = ProgramRepository.setPrograms (result.data);
                            this.all_series = this.generalService.mergeOneArrayInAnother (series, this.all_series);
                        }
                    },
                    () => {
                        if (this.generalService.fetch_router_error_retry_count < 6) {
                            this.generalService.fetch_router_error_retry_count += 1;
                            this.getSeries (true);
                        } else {
                            if (this.generalService.is_platform_browser) {
                                this.generalService.showMessage (this.generalService.statics.error_message_500_id, 10);
                            }
                            this.generalService.show_normal_content = false;
                        }
                    }
                );
            }
        );
    }
    
    loadMore () {
        this.page +=1;
        this.getSeries (false);
    }

    setPageHeaders() {
        this.generalService.removeMetaTags();
        this.generalService.setTitle (this.generalService.statics.pageTitles.SERIES);
        this.generalService.setMetaTags (this.generalService.statics.pageMeta.SERIES);
        this.generalService.setLinkTag (this.generalService.statics.pageLinkService.SERIES);
    }

    ngOnDestroy(){
        this.generalService.resetErrorCounts();
    }
}

