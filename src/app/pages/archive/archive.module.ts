import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ContentComponent } from './content/content.component';
import { archive_routes } from './archive.routing';
import { PromotionCarouselModule } from '../../webparts/promotion-carousel/promotion-carousel.module';
import { CarouselModule } from '../../webparts/carousel/carousel.module';
import { NumberToPersianModule } from '../../pipes/numberToPersian/numberToPersian.module';
import { GeneralService } from '../../shared/general/general.service';
import { ArchiveService } from '../../shared/archive/archive.service';
import { EpisodeRepository } from '../../shared/episode/episode.repository';
import { ChannelService } from '../../shared/channel/channel.service';
import { ChannelRepository } from '../../shared/channel/channel.repository';

@NgModule ({
    schemas     : [ CUSTOM_ELEMENTS_SCHEMA ],
    declarations: [
        ContentComponent
    ],
    imports     : [
        FormsModule,
        HttpModule,
        CommonModule,
        PromotionCarouselModule,
        CarouselModule,
        NumberToPersianModule,
        RouterModule.forChild (archive_routes)
    ],
    providers   : [ GeneralService, ArchiveService, EpisodeRepository, ChannelService, ChannelRepository ]
})
export class ArchiveModule {
}
