import { ContentComponent } from './content/content.component';


export const archive_routes = [
    {
        path     : 'archive/:channel_descriptor/:date',
        component: ContentComponent
    },
    {
        path     : 'archive/:channel_descriptor',
        component: ContentComponent
    },
];
