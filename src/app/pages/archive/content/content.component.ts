import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GeneralService } from '../../../shared/general/general.service';
import { Episode } from '../../../shared/episode/episode.model';
import { Channel } from '../../../shared/channel/channel.model';
import { ArchiveService } from '../../../shared/archive/archive.service';
import { EpisodeRepository } from '../../../shared/episode/episode.repository';
import { ChannelService } from '../../../shared/channel/channel.service';
import { ChannelRepository } from '../../../shared/channel/channel.repository';

@Component ({
    selector   : 'app-content',
    templateUrl: './content.component.html',
    styleUrls  : [ './content.component.scss' ]
})
export class ContentComponent implements OnInit {
    
    public result_episodes: Array<Episode> = [];
    public date: string;
    public channel: Channel;
    public channels: Array<Channel>        = [];
    public page                            = 0;
    
    constructor (public generalService: GeneralService, private route: ActivatedRoute,
                 private archiveService: ArchiveService, private channelService: ChannelService) {
        this.channel = new Channel ();
    }
    
    ngOnInit () {
        this.route.params.subscribe ((params) => {
            this.channel.setDescriptor (params[ 'channel_descriptor' ]);
            this.date = params[ 'date' ] ? params[ 'date' ] : this.generalService.getCurrentJalaliDate ();
            this.getChannelsList (false);
            this.getArchiveByDate (false);
        });
    }
    
    getArchiveByDate (force_request: boolean) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            (router) => {
                this.archiveService.getSelectedDateEpisodes (this.generalService.router_path,
                    this.channel.getDescriptor (), this.date, this.page).subscribe (
                    (result) => {
                        this.result_episodes = EpisodeRepository.setEpisodes (result.data);
                    },
                    (error) => {
                        if (this.generalService.fetch_router_error_retry_count < 6) {
                            this.generalService.fetch_router_error_retry_count += 1;
                            this.getArchiveByDate (true);
                        } else {
                            if (this.generalService.is_platform_browser) {
                                this.generalService.showMessage (this.generalService.statics.error_message_500_id, 10);
                            }
                            this.generalService.show_normal_content = false;
                        }
                    },
                    () => {
                    }
                );
            }
        );
    }
    
    getChannelsList (force_request: boolean) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            (router) => {
                this.channelService.getChannels (this.generalService.router_path).subscribe (
                    (channels) => {
                        if(channels.data.length > 0) {
                            this.channels = ChannelRepository.setChannels (channels.data);
                            this.generalService.sub_content_is_loaded = true;
                            this.channel = ChannelRepository.getChannelDataFromChannelListByDescriptor(this.channel.getDescriptor(), this.channels);
                            this.setPageHeaders();
                        }
                    },
                    (error) => {
                        if (this.generalService.fetch_router_error_retry_count < 6) {
                            this.generalService.fetch_router_error_retry_count += 1;
                            this.getChannelsList (true);
                        } else {
                            if (this.generalService.is_platform_browser) {
                                this.generalService.showMessage (this.generalService.statics.error_message_500_id, 10);
                            }
                            this.generalService.sub_content_is_loaded = false;
                        }
                    }
                );
            }
        );
    }

    setPageHeaders() {
        this.generalService.removeMetaTags();
        this.generalService.setTitle (this.archiveService.setArchivePageTitle(this.channel, this.date));
        this.generalService.setMetaTags (this.archiveService.setArchivePageMetaTags(this.channel, this.date));
        this.generalService.setLinkTag (this.archiveService.setArchivePageLinksTags(this.channel, this.date));
    }
    
}
