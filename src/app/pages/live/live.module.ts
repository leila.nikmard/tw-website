import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChannelComponent } from './channel/channel.component';
import { ChannelsComponent } from './channels/channels.component';
import { RouterModule } from '@angular/router';
import { live_routes } from './live.routing';
import { ListViewModule } from '../../webparts/list-view/list-view.module';
import { HourlyArchiveService } from '../../shared/hourly-archive/hourly-archive.service';
import { HourlyArchiveRepository } from '../../shared/hourly-archive/hourly-archive.repository';
import { ChannelService } from '../../shared/channel/channel.service';
import { ChannelRepository } from '../../shared/channel/channel.repository';
import { EpisodeRepository } from '../../shared/episode/episode.repository';
import { EpisodeService } from '../../shared/episode/episode.service';
import { GeneralService } from '../../shared/general/general.service';
import { EpgService } from '../../shared/epg/epg.service';
import { EpgRepository } from '../../shared/epg/epg.repository';
import { RadiantPlayerModule } from '../../webparts/radiant-player/radiant-player.module';
import { RefineChannelNameModule } from '../../pipes/refineChannelName/refineChannelName.module';
import { QualityBarModule } from '../../webparts/quality-bar/quality-bar.module';
import { CarouselModule } from '../../webparts/carousel/carousel.module';

@NgModule({
    imports     : [
        ListViewModule,
        CommonModule,
        RadiantPlayerModule,
        RefineChannelNameModule,
        QualityBarModule,
        CarouselModule,
        RouterModule.forChild(live_routes)
    ],
    declarations: [ ChannelComponent, ChannelsComponent ],
    providers: [HourlyArchiveService, HourlyArchiveRepository, ChannelService, ChannelRepository,
        EpisodeRepository, EpisodeService, GeneralService, EpgService, EpgRepository]
})
export class LiveModule {
}
