import { Component, OnDestroy, OnInit } from '@angular/core';
import { GeneralService } from '../../../shared/general/general.service';
import { ChannelService } from '../../../shared/channel/channel.service';
import { Channel } from '../../../shared/channel/channel.model';
import { ChannelRepository } from '../../../shared/channel/channel.repository';

@Component ({
    selector   : 'app-channels',
    templateUrl: './channels.component.html',
    styleUrls  : [ './channels.component.scss' ]
})
export class ChannelsComponent implements OnInit, OnDestroy {
    channels: Array<Channel>;
    
    constructor (public generalService: GeneralService, private channelService: ChannelService) {
    }
    
    ngOnInit () {
        this.setPageHeaders();
        this.getChannels (false);
    }
    
    getChannels (force_request: boolean = false) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            (router) => {
                this.channelService.getChannels (this.generalService.router_path).subscribe (
                    (result) => {
                        if (result.code === 200) {
                            this.channels = ChannelRepository.setChannels (result.data);
                        }
                    },
                    (error) => {
                        if (this.generalService.fetch_router_error_retry_count < 6) {
                            this.generalService.fetch_router_error_retry_count += 1;
                            this.getChannels (true);
                        } else {
                            if (this.generalService.is_platform_browser) {
                                this.generalService.showMessage (this.generalService.statics.error_message_500_id, 10);
                            }
                            this.generalService.show_normal_content = false;
                        }
                    }
                );
            }
        );
    }

    setPageHeaders() {
        this.generalService.removeMetaTags();
        this.generalService.setTitle (this.generalService.statics.pageTitles.CHANNELS);
        this.generalService.setMetaTags (this.generalService.statics.pageMeta.CHANNELS);
        this.generalService.setLinkTag (this.generalService.statics.pageLinkService.CHANNELS);
    }

    ngOnDestroy() {
        this.generalService.resetErrorCounts();
    }
    
}
