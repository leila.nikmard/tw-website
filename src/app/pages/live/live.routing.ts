import { ChannelComponent } from './channel/channel.component';
import { ChannelsComponent } from './channels/channels.component';

export const live_routes = [
    {
        path     : 'live/:channel_descriptor',
        component: ChannelComponent
    },
    {
        path     : 'channels',
        component: ChannelsComponent
    }
];
