import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Channel } from '../../../shared/channel/channel.model';
import { ChannelService } from '../../../shared/channel/channel.service';
import { GeneralService } from '../../../shared/general/general.service';
import { ChannelRepository } from '../../../shared/channel/channel.repository';
import { Episode } from '../../../shared/episode/episode.model';
import { HourlyArchive } from '../../../shared/hourly-archive/hourly-archive.model';
import { Epg } from '../../../shared/epg/epg.model';
import { EpisodeRepository } from '../../../shared/episode/episode.repository';
import { HourlyArchiveRepository } from '../../../shared/hourly-archive/hourly-archive.repository';
import { EpgRepository } from '../../../shared/epg/epg.repository';
import { HourlyArchiveService } from '../../../shared/hourly-archive/hourly-archive.service';
import { ChannelLink } from '../../../shared/channel/channel-link.model';
import { RadiantPlayerComponent } from '../../../webparts/radiant-player/radiant-player.component';

@Component({
    selector: 'app-channel',
    templateUrl: './channel.component.html',
    styleUrls: [ './channel.component.scss' ]
})
export class ChannelComponent implements OnInit, OnDestroy {
    public channel = new Channel();
    public channels: Channel[];
    public latest_episodes: Episode[];
    public hourly_archives: HourlyArchive[];
    public epg: Epg[];
    public default_channel_link: ChannelLink;
    public is_live: boolean;
    public hourly_archive_title;

    @ViewChild(RadiantPlayerComponent) player: RadiantPlayerComponent;

    constructor (public generalService: GeneralService, private route: ActivatedRoute, private channelService: ChannelService,
                 private hourlyArchiveService: HourlyArchiveService, private router: Router) {
    }

    ngOnInit () {
        this.route.params.subscribe((params) => {
            this.is_live = true;
            this.channel.setDescriptor(params[ 'channel_descriptor' ]);
            this.getChannelLinks(false);
        });
    }

    getChannelLinks (force_request: boolean) {
        this.generalService.requestRouterUrl(force_request).subscribe(
            () => {
                this.channelService.getChannelLinks(this.generalService.router_path, this.channel.getDescriptor()).subscribe(
                    (result) => {
                        this.generalService.request_body.message = result.message.toString();
                        this.generalService.request_body.code = parseInt(result.code);
                        if(this.generalService.request_body.code == 200) {
                            if (this.generalService.request_body.message != 'success') {
                                if (this.generalService.is_platform_browser) {
                                    this.generalService.showMessage(this.generalService.request_body.message, 10);
                                }
                            }
                            this.channel = ChannelRepository.setChannel(result.data[ 0 ].channel);
                            this.channel.setLinks(result.data[ 0 ].links);
                            this.default_channel_link = this.channelService.get500VodLink(this.channel.getLinks());
                            this.setPlayerLink(this.default_channel_link.getLink());
                        } else if (this.generalService.request_body.code === 404) {
                            this.router.navigateByUrl ('/404');
                        } else {
                            this.channel = ChannelRepository.setChannel(result.data[ 0 ].channel);
                            this.setPageHeaders();
                        }

                    },
                    () => {
                        if (this.channelService.fetch_get_channel_links_error_retry_count < 6) {
                            this.channelService.fetch_get_channel_links_error_retry_count += 1;
                            this.getChannelLinks(true);
                        } else {
                            if (!this.generalService.is_platform_server) {
                                this.generalService.showMessage(this.generalService.statics.error_message_500_id, 10);
                            }
                            this.generalService.show_normal_content = false;
                        }
                    },
                    () => {
                        this.getHourlyArchive(false);
                        this.getLivePageData(false);
                        this.setPageHeaders();
                    }
                );
            },
            () => {
            }
        );
    }

    setPlayerLink ($event) {
        setTimeout(
            () => {
                if (this.player) {
                    try {
                        this.player.destroyPlayer();
                    } catch (e) {

                    }
                }
                this.player.setPlayLinkAndInit($event);
            }, 5
        );
    }

    getLivePageData (force_request: boolean) {
        this.generalService.requestRouterUrl(force_request).subscribe(
            () => {
                this.channelService.getLivePage(this.generalService.router_path, this.channel.getDescriptor()).subscribe(
                    (result) => {
                        this.latest_episodes = EpisodeRepository.setEpisodes(result.data[ 0 ].newest);
                        this.channels = ChannelRepository.setChannels(result.data[ 0 ].channels);
                        this.generalService.sub_content_is_loaded = true;
                        this.epg = EpgRepository.setEpges(result.data[ 0 ].epg);
                    },
                    () => {
                        if (this.generalService.fetch_router_error_retry_count < 12) {
                            this.generalService.fetch_router_error_retry_count += 1;
                            this.getLivePageData(true);
                        } else {
                            if (this.generalService.is_platform_browser) {
                                this.generalService.showMessage(this.generalService.statics.error_message_500_sub_queries_id, 10);
                            }
                            this.generalService.sub_content_is_loaded = false;
                        }
                    }
                );
            }
        );
    }

    getHourlyArchive (force_request: boolean) {
        this.generalService.requestRouterUrl(force_request).subscribe(
            () => {
                this.hourlyArchiveService.getHourlyArchive(this.generalService.router_path,
                    this.channel.getId(), this.generalService.getCurrentJalaliDate()).subscribe(
                    (result) => {
                        this.hourly_archives = HourlyArchiveRepository.setHourlyArchives(result.data);
                    },
                    () => {
                        if (this.generalService.fetch_router_error_retry_count < 12) {
                            this.generalService.fetch_router_error_retry_count += 1;
                            this.getHourlyArchive(true);
                        } else {
                            if (this.generalService.is_platform_browser) {
                                this.generalService.showMessage(this.generalService.statics.error_message_500_sub_queries_id, 10);
                            }
                            this.generalService.sub_content_is_loaded = false;
                        }
                    }
                );
            }
        );
    }
    setPageHeaders() {
        this.generalService.removeMetaTags();
        this.generalService.setTitle (this.channelService.setChannelPageTitle(this.channel));
        this.generalService.setMetaTags (this.channelService.setChannelPageMetaTags(this.channel));
        this.generalService.setLinkTag (this.channelService.setChannelPageLinksTags(this.channel));
    }

    selectHourlyArchive ($event) {
        this.hourly_archive_title = $event.getTitle();
        this.setPlayerLink($event.getVodLink());
        window.scrollTo(0, 0);
        this.is_live = false;
    }

    ngOnDestroy () {
        if (this.player) {
            try {
                this.player.destroyPlayer();
            } catch (e) {

            }
        }
        this.generalService.resetErrorCounts();
    }

}
