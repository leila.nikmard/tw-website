import { EpisodeComponent } from './details/episode.component';

export const episode_route = [
    {
        path     : 'episode/:id',
        component: EpisodeComponent
    }
];
