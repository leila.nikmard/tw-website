import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EpisodeComponent } from './details/episode.component';
import { RouterModule } from '@angular/router';
import { episode_route } from './episode.routing';
import { SocialInfoBarModule } from '../../webparts/social-info-bar/social-info-bar.module';
import { ProgramDescriptionModule } from '../../webparts/program-description/program-description.module';
import { CarouselModule } from '../../webparts/carousel/carousel.module';
import { RemoveApostrophePipe } from '../../pipes/rempveApostrophe/removeApostrophe.pipe';
import { GenerateVideoDuration } from '../../pipes/generateVideoDuration/generateVideoDuration';
import { RadiantPlayerModule } from '../../webparts/radiant-player/radiant-player.module';
import { EpisodeService } from '../../shared/episode/episode.service';
import { BreadcrumbKnowledgeGraphModule } from '../../webparts/breadcrumb-knowledge-graph/breadcrumb-knowledge-graph.module';
import { TagBarModule } from '../../webparts/tag-bar/tag-bar.module';

@NgModule({
    imports     : [
        CommonModule,
        RadiantPlayerModule,
        SocialInfoBarModule,
        ProgramDescriptionModule,
        CarouselModule,
        BreadcrumbKnowledgeGraphModule,
        TagBarModule,
        RouterModule.forChild(episode_route)
    ],
    declarations: [ EpisodeComponent, RemoveApostrophePipe, GenerateVideoDuration ],
    providers   : [ EpisodeService ]
})
export class EpisodeModule {
}
