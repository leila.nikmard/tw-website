import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { GeneralService } from '../../../shared/general/general.service';
import { EpisodeService } from '../../../shared/episode/episode.service';
import { Episode } from '../../../shared/episode/episode.model';
import { EpisodeRepository } from '../../../shared/episode/episode.repository';
import { FavoriteList } from '../../../shared/favorite_list/favorite-list.model';
import { FavoriteListRepository } from '../../../shared/favorite_list/favorite-list.repository';
import { RadiantPlayerComponent } from '../../../webparts/radiant-player/radiant-player.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component ({
    selector   : 'app-episode',
    templateUrl: './episode.component.html',
    styleUrls  : [ './episode.component.scss' ]
})
export class EpisodeComponent implements OnInit, OnDestroy {
    public episode: Episode;
    public other_episodes: Array<Episode>               = [];
    public related_episodes: Array<Episode>             = [];
    public episode_fav_list: Array<Array<FavoriteList>> = [];
    public program_fav_list: Array<Array<FavoriteList>> = [];
    public moments_list;
    
    @ViewChild (RadiantPlayerComponent) player: RadiantPlayerComponent;
    
    constructor (public generalService: GeneralService, private episodeService: EpisodeService, private route: ActivatedRoute, private router: Router) {
        this.episode = new Episode ();
    }
    
    ngOnInit () {
        this.route.params.subscribe ((params) => {
            this.episode.setId (params[ 'id' ]);
            this.getEpisodeDetails (false);
        });
    }
    
    getEpisodeDetails (force_request: boolean) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            () => {
                this.episodeService.getEpisodeDetails (this.generalService.router_path, this.episode.getId (),
                    'desktop', null)
                    .subscribe (
                        (result) => {
                            this.generalService.request_body.message = result.message.toString();
                            this.generalService.request_body.code = parseInt(result.code);
                            if (this.generalService.request_body.code === 200) {
                                if (this.generalService.request_body.message != 'success') {
                                    if (this.generalService.is_platform_browser) {
                                        this.generalService.showMessage(this.generalService.request_body.message, 10);
                                    }
                                }
                                this.episode = EpisodeRepository.setEpisode(result.data[ 0 ]);
                                this.setPageHeaders();
                                this.generalService.setTitle(this.episode.getTitle());
                                setTimeout(() => {
                                    this.setPlayerLink(this.episode.getVodLink()[ 0 ].link);
                                }, 5);
                            } else if (this.generalService.request_body.code === 404) {
                                this.router.navigateByUrl ('/404');
                            } else {
                                this.episode = EpisodeRepository.setEpisode(result.data[ 0 ]);
                                this.getEpisodePageData (false);
                                this.setPageHeaders();
                            }
                        },
                        (error) => {
                            if (this.generalService.fetch_router_error_retry_count < 6) {
                                this.generalService.fetch_router_error_retry_count += 1;
                                this.getEpisodeDetails (true);
                            } else {
                                if (!this.generalService.is_platform_browser) {
                                    this.generalService.showMessage (this.generalService.statics.error_message_500_id, 10);
                                }
                                this.generalService.show_normal_content = false;
                            }
                        },
                        () => {
                            this.setPageHeaders();
                            this.getEpisodePageData (false);
                        });
            });
    }

    setPageHeaders() {
        this.generalService.removeMetaTags();
        this.generalService.setTitle (this.episodeService.setEpisodePageTitle(this.episode));
        this.generalService.setMetaTags (this.episodeService.setEpisodePageMetaTags(this.episode));
        this.generalService.setLinkTag (this.episodeService.setEpisodePageLinksTags(this.episode));
    }
    
    getEpisodePageData (force_request: boolean) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            () => {
                this.episodeService.getEpisodePage (this.generalService.router_path, this.episode.getId (), 14, this.episode.getProgram ().getId ())
                    .subscribe ((result) => {
                        if (result.code === 200) {
                            this.other_episodes   = EpisodeRepository.setEpisodes (result.data[ 0 ].other_episodes);
                            this.related_episodes = EpisodeRepository.setEpisodes (result.data[ 0 ].related_episodes);
                            this.episode_fav_list = FavoriteListRepository.setFavoriteListTwoDimension (result.data[ 0 ].episode_favlists);
                            this.program_fav_list = FavoriteListRepository.setFavoriteListTwoDimension (result.data[ 0 ].program_favlists);
                            this.moments_list     = result.data[ 0 ].moments;
                            this.generalService.sub_content_is_loaded = true;
                        }
                    },
            (error) => {
                        if (this.generalService.fetch_sub_content_router_error_retry_count < 6) {
                            this.generalService.fetch_sub_content_router_error_retry_count += 1;
                            this.getEpisodePageData (true);
                        } else {
                            this.generalService.sub_content_is_loaded = false;
                        }
                    });
            });
    }
    
    setPlayerLink ($event) {
        setTimeout (
            () => {
                if (this.player) {
                    try {
                        this.player.destroyPlayer ();
                    } catch (e) {}
                }
                this.player.setPlayLinkAndInit ($event);
            }, 5
        );
    }
    
    setPlayerMoment (second) {
        this.player.setPlayerMoment (second);
    }
    
    ngOnDestroy () {
        if (this.player) {
            try {
                this.player.destroyPlayer ();
            } catch (e) {}
        }
        this.generalService.resetErrorCounts();
    }
}
