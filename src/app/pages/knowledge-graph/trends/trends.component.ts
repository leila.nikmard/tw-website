import { Component, OnInit } from '@angular/core';
import { FavoriteList } from '../../../shared/favorite_list/favorite-list.model';
import { Episode } from '../../../shared/episode/episode.model';
import { Program } from '../../../shared/program/program.model';
import { GeneralService } from '../../../shared/general/general.service';
import { ActivatedRoute } from '@angular/router';
import { FavoriteListService } from '../../../shared/favorite_list/favorite-list.service';
import { FavoriteListRepository } from '../../../shared/favorite_list/favorite-list.repository';
import { EpisodeRepository } from '../../../shared/episode/episode.repository';
import { ProgramRepository } from '../../../shared/program/program.repository';

@Component ({
    selector   : 'app-trends',
    templateUrl: './trends.component.html',
    styleUrls  : [ './trends.component.scss' ]
})
export class TrendsComponent implements OnInit {
    
    public favorite_list: FavoriteList;
    public id_is_descriptor: boolean;
    public newest: Array<Episode>;
    public movies: Array<Episode>;
    public parents: Array<FavoriteList>;
    public programs: Array<Program>;
    public children: Array<FavoriteList>;
    
    constructor (public generalService: GeneralService, private route: ActivatedRoute,
                 private favoriteListService: FavoriteListService) {
        this.favorite_list = new FavoriteList ();
    }
    
    ngOnInit () {
        this.route.params.subscribe ((params) => {
            if (Number (params[ 'item_id' ])) {
                this.favorite_list.setId (params[ 'item_id' ]);
                this.id_is_descriptor = false;
            } else {
                this.favorite_list.setUrlDescriptor (params[ 'item_id' ]);
                this.id_is_descriptor = true;
            }
            this.getFavoriteListData (false);
        });
    }
    
    getFavoriteListData (force_request: boolean) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            (router) => {
                this.favoriteListService.getFavoriteList (this.generalService.router_path, this.id_is_descriptor,
                    this.favorite_list.getId (), this.favorite_list.getUrlDescriptor ()).subscribe (
                    (result) => {
                        this.favorite_list = FavoriteListRepository.setFavoriteList (result.data[ 0 ]);
                        this.parents       = FavoriteListRepository.setFavoriteLists (result.data[ 0 ].parents);
                        this.children      = FavoriteListRepository.setFavoriteLists (result.data[ 0 ].children);
                        this.newest        = EpisodeRepository.setEpisodes (result.data[ 0 ].newest);
                        this.movies        = EpisodeRepository.setEpisodes (result.data[ 0 ].movies);
                        this.programs      = ProgramRepository.setPrograms (result.data[ 0 ].programs);
                    },
                    (error) => {
                        if (this.favoriteListService.fetch_get_trends_error_retry_count < 6) {
                            this.favoriteListService.fetch_get_trends_error_retry_count += 1;
                            this.getFavoriteListData (true);
                        } else {
                            if (!this.generalService.is_platform_server) {
                                this.generalService.showMessage (this.generalService.statics.error_message_500_id, 10);
                            }
                            this.generalService.show_normal_content = false;
                        }
                    }
                );
            }
        );
    }
}
