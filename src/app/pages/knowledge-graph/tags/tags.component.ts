import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../../../shared/general/general.service';
import { ActivatedRoute } from '@angular/router';
import { FavoriteList } from '../../../shared/favorite_list/favorite-list.model';
import { FavoriteListService } from '../../../shared/favorite_list/favorite-list.service';
import { Episode } from '../../../shared/episode/episode.model';
import { Program } from '../../../shared/program/program.model';
import { FavoriteListRepository } from '../../../shared/favorite_list/favorite-list.repository';
import { EpisodeRepository } from '../../../shared/episode/episode.repository';
import { ProgramRepository } from '../../../shared/program/program.repository';

@Component ({
    selector   : 'app-tags',
    templateUrl: './tags.component.html',
    styleUrls  : [ './tags.component.scss' ]
})
export class TagsComponent implements OnInit {
    public favorite_list = new FavoriteList ();
    public id_is_descriptor: boolean;
    public action_type: string;
    public newest: Array<Episode>;
    public movies: Array<Episode>;
    public parents: Array<FavoriteList>;
    public programs: Array<Program>;
    public page          = 1;
    public children: Array<FavoriteList>;
    
    constructor (public generalService: GeneralService, private route: ActivatedRoute,
                 private favoriteListService: FavoriteListService) {
    }
    
    ngOnInit () {
        this.route.params.subscribe ((params) => {
            if (Number (params[ 'item_id' ])) {
                this.favorite_list.setId (params[ 'item_id' ]);
                this.id_is_descriptor = false;
            } else {
                this.favorite_list.setUrlDescriptor (params[ 'item_id' ]);
                this.id_is_descriptor = true;
            }
            this.action_type = params[ 'type_id' ] ? params[ 'type_id' ] : null;
            this.getFavoriteListData (false);
        });
    }
    
    getFavoriteListData (force_request: boolean) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            (router) => {
                this.favoriteListService.getFavoriteList (this.generalService.router_path, this.id_is_descriptor,
                    this.favorite_list.getId (), this.favorite_list.getUrlDescriptor ()).subscribe (
                    (result) => {
                        this.favorite_list = FavoriteListRepository.setFavoriteList (result.data[ 0 ]);
                        this.parents       = FavoriteListRepository.setFavoriteLists (result.data[ 0 ].parents);
                        this.children      = FavoriteListRepository.setFavoriteLists (result.data[ 0 ].children);
                        if (!this.action_type) {
                            this.newest   = EpisodeRepository.setEpisodes (result.data[ 0 ].newest);
                            this.movies   = EpisodeRepository.setEpisodes (result.data[ 0 ].movies);
                            this.programs = ProgramRepository.setPrograms (result.data[ 0 ].programs);
                        }
                    },
                    (error) => {
                        if (this.favoriteListService.fetch_get_tags_error_retry_count < 6) {
                            this.favoriteListService.fetch_get_tags_error_retry_count += 1;
                            this.getFavoriteListData (true);
                        } else {
                            if (!this.generalService.is_platform_server) {
                                this.generalService.showMessage (this.generalService.statics.error_message_500_id, 10);
                            }
                            this.generalService.show_normal_content = false;
                        }
                    },
                    () => {
                        this.setPageMetaData ();
                        if (this.action_type === 'programs') {
                            this.favoriteListService.getFavoriteListProgram (this.generalService.router_path,
                                this.favorite_list.getId (), this.page, 'newest', 0).subscribe (
                                (result) => {
                                    this.programs = result.data;
                                },
                                () => {
                                },
                                () => {
                                }
                            );
                        } else if (this.action_type === 'movies') {
                            this.favoriteListService.getFavoriteListEpisodes (this.generalService.router_path,
                                this.favorite_list.getId (), this.page, 'newest', 1).subscribe (
                                (result) => {
                                    this.movies = result.data;
                                },
                                () => {
                                },
                                () => {
                                }
                            );
                        }
                    }
                );
            }
        );
    }
    
    setPageMetaData () {
        if (this.action_type === 'movies') {
            this.generalService.setTitle (this.favorite_list.getNameFa () + ' | فیلم ها');
        } else if (this.action_type === 'programs') {
            this.generalService.setTitle (this.favorite_list.getNameFa () + ' | برنامه ها');
        } else {
            this.generalService.setTitle (this.favorite_list.getNameFa () + ' | صفحه برچسب');
        }
        // this.generalService.setMetaTags(this.generalService.statics.pageMeta.DMCA);
    }
    
}
