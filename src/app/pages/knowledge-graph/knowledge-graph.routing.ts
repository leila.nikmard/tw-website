import { TagsComponent } from './tags/tags.component';
import { TrendsComponent } from './trends/trends.component';
import { ArtistsComponent } from './artists/artists.component';

export const knowlege_graph_routes = [
    {
        path     : 'tags/:item_id',
        component: TagsComponent
    },
    {
        path     : 'tags/:item_id/:type_id',
        component: TagsComponent
    },
    {
        path     : 'trends/:descriptor',
        component: TrendsComponent
    },
    {
        path     : 'trends/:id',
        component: TrendsComponent
    },
    {
        path     : 'artist',
        component: ArtistsComponent
    }
];
