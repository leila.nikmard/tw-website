import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TagsComponent } from './tags/tags.component';
import { TrendsComponent } from './trends/trends.component';
import { RouterModule } from '@angular/router';
import { knowlege_graph_routes } from './knowledge-graph.routing';
import { GeneralService } from '../../shared/general/general.service';
import { FavoriteListRepository } from '../../shared/favorite_list/favorite-list.repository';
import { FavoriteListService } from '../../shared/favorite_list/favorite-list.service';
import { ArtistsComponent } from './artists/artists.component';
import { PosterImageModule } from '../../webparts/poster-image/poster-image.module';
import { BreadcrumbModule } from '../../webparts/breadcrumb/breadcrumb.module';
import { TagsBarModule } from '../../webparts/tags-bar/tags-bar.module';
import { ListViewModule } from '../../webparts/list-view/list-view.module';

@NgModule({
    imports     : [
        CommonModule,
        PosterImageModule,
        BreadcrumbModule,
        TagsBarModule,
        ListViewModule,
        RouterModule.forChild(knowlege_graph_routes)
    ],
    declarations: [ TagsComponent, TrendsComponent, ArtistsComponent ],
    providers   : [ GeneralService, FavoriteListRepository, FavoriteListService ]
})
export class KnowledgeGraphModule {
}
