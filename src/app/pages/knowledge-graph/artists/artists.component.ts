import { Component, OnInit } from '@angular/core';
import { FavoriteListRepository } from '../../../shared/favorite_list/favorite-list.repository';
import { FavoriteList } from '../../../shared/favorite_list/favorite-list.model';
import { GeneralService } from '../../../shared/general/general.service';
import { ActivatedRoute } from '@angular/router';
import { FavoriteListService } from '../../../shared/favorite_list/favorite-list.service';

@Component({
  selector: 'app-artists',
  templateUrl: './artists.component.html',
  styleUrls: ['./artists.component.scss']
})
export class ArtistsComponent implements OnInit {


    public favorite_list: FavoriteList;
    public artists: Array<FavoriteList>;

    constructor(public generalService: GeneralService, private route: ActivatedRoute,
                private favoriteListService: FavoriteListService) {
        this.favorite_list = new FavoriteList();
    }

    getFavoriteListData(force_request: boolean) {
        this.generalService.requestRouterUrl(force_request).subscribe(
            (router) => {
                this.favoriteListService.getFavoriteList(this.generalService.router_path, false,
                    405262, this.favorite_list.getUrlDescriptor()).subscribe(
                    (result) => {
                        this.favorite_list = FavoriteListRepository.setFavoriteList(result.data[0]);
                        this.artists = FavoriteListRepository.setFavoriteLists(result.data[0].children);
                    },
                    (error) => {}
                );
            },
            (error) => {}
        );
    }

  ngOnInit() {
        this.getFavoriteListData(false);
  }

}
