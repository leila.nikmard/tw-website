import { NgModule } from '@angular/core';
import { ProgramModule } from './pages/program/program.module';
import { TagModule } from './pages/tag/tag.module';
import { EpisodeModule } from './pages/episode/episode.module';
import { LiveModule } from './pages/live/live.module';
import { KnowledgeGraphModule } from './pages/knowledge-graph/knowledge-graph.module';
import { LandingsModule } from './pages/landings/landings.module';
import { ArchiveModule } from './pages/archive/archive.module';
import { NotFoundModule } from './pages/not-found/not-found.module';
import { SearchModule } from './pages/search/search.module';
import { AuthenticationModule } from './pages/authentication/authentication.module';
import { CompanyModule } from './pages/company/company.module';

@NgModule ({
    imports: [
        LandingsModule, ProgramModule, TagModule, EpisodeModule, KnowledgeGraphModule, ArchiveModule,
        LiveModule, NotFoundModule, SearchModule, AuthenticationModule, CompanyModule
    ]
})

export class Modules {
}
