import { Base } from '../general/base.model';

export class Promotion extends Base {
    private category: string;
    private obj_type: string;
    private valid_from: string;
    private image_path_large: string;
    private description_fa: string;
    private image_path: string;
    private title: string;
    private scope: string;
    private description_en: string;
    private order: number;
    private favorite_list_id;
    private enable: number;
    private link: string;
    private valid_to: string;
    private image_path_small: string;
    private occasion: string;
    private play_time: string;
    private type: string;
    private id: number;
    private is_mobile: number;
    private show_time: string;
    private current_past: boolean;
    
    constructor () {
        super();
    }
    
    setCategory (_category: string): void {
        this.category = _category;
    }
    
    getCategory (): string {
        return this.category ? this.category : '';
    }
    
    setObjType (_obj_type: string): void {
        this.obj_type = _obj_type;
    }
    
    getObjType (): string {
        return this.obj_type ? this.obj_type : '';
    }
    
    setValidFrom (_valid_from: string): void {
        this.valid_from = _valid_from;
    }
    
    getValidFrom (): string {
        return this.valid_from ? this.valid_from : '';
    }
    
    setImagePathLarge (_image_path_large: string): void {
        this.image_path_large = _image_path_large;
    }
    
    getImagePathLarge (): string {
        return this.image_path_large ? this.image_path_large : '';
    }
    
    setDescriptionFa (_description_fa: string): void {
        this.description_fa = _description_fa;
    }
    
    getDescriptionFa (): string {
        return this.description_fa ? this.description_fa : '';
    }
    
    setImagePath (_image_path: string): void {
        this.image_path = _image_path;
    }
    
    getImagePath (): string {
        return this.image_path ? this.image_path : '';
    }
    
    setTitle (_title: string): void {
        this.title = _title;
    }
    
    getTitle (): string {
        return this.title ? this.title : '';
    }
    
    setScope (_scope: string): void {
        this.scope = _scope;
    }
    
    getScope (): string {
        return this.scope ? this.scope : '';
    }
    
    setDescriptionEn (_description_en: string): void {
        this.description_en = _description_en;
    }
    
    getDescriptionEn (): string {
        return this.description_en ? this.description_en : '';
    }
    
    setOrder (_order: number): void {
        this.order = _order;
    }
    
    getOrder (): number {
        return this.order ? this.order : -1;
    }
    
    setFavoriteListId (_favorite_list_id: string): void {
        this.favorite_list_id = _favorite_list_id;
    }
    
    getFavoriteListId (): string {
        return this.favorite_list_id ? this.favorite_list_id : '';
    }
    
    setEnable (_enable: number): void {
        this.enable = _enable;
    }
    
    getEnable (): number {
        return this.enable ? this.enable : -1;
    }
    
    setLink (_link: string): void {
        this.link = _link;
    }
    
    getLink (): string {
        return this.link ? this.link : '';
    }
    
    setValidTo (_valid_to: string): void {
        this.valid_to = _valid_to;
    }
    
    getValidTo (): string {
        return this.valid_to ? this.valid_to : '';
    }
    
    setImagePathSmall (_image_path_small: string): void {
        this.image_path_small = _image_path_small;
    }
    
    getImagePathSmall (): string {
        return this.image_path_small ? this.image_path_small : '';
    }
    
    setOccasion (_occasion: string): void {
        this.occasion = _occasion;
    }
    
    getOccasion (): string {
        return this.occasion ? this.occasion : '';
    }
    
    setPlayTime (_play_time: string): void {
        this.play_time = _play_time;
    }
    
    getPlayTime (): string {
        return this.play_time ? this.play_time : '';
    }
    
    setType (_type: string): void {
        this.type = _type;
    }
    
    getType (): string {
        return this.type ? this.type : '';
    }
    
    setId (_id: number): void {
        this.id = _id;
    }
    
    getId (): number {
        return this.id ? this.id : 0;
    }
    
    setIsMobile (_is_mobile: number): void {
        this.is_mobile = _is_mobile;
    }
    
    getIsMobile (): number {
        return this.is_mobile ? this.is_mobile : 0;
    }
    
    setShowTime (_show_time: string): void {
        this.show_time = _show_time;
    }
    
    getShowTime (): string {
        return this.show_time ? this.show_time : '';
    }
    
    setCurrentPast (_current_past: boolean): void {
        this.current_past = _current_past;
    }
    
    getCurrentPast (): boolean {
        return this.current_past ? this.current_past : false;
    }
}
