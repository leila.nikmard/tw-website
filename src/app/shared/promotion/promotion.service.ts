import { Injectable } from '@angular/core';
import { GeneralService } from '../general/general.service';

@Injectable ()
export class PromotionService {
    fetch_get_kids_promotion_error_retry_count   = 0;
    fetch_get_movies_promotion_error_retry_count = 0;
    fetch_get_series_promotion_error_retry_count = 0;
    
    constructor (private generalService: GeneralService) {
    }
    
    getPromotions (router_url: string, type: number, name: string) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url, this.generalService.configs.baseURLs.GET_PROMOTIONS_LIST, {
            'is_mobile': 0,
            'place'    : type,
            'scope'    : 'banner'
        });
        return this.generalService.makeGETRequest (final_service_url, true,
            this.generalService.configs.getBaseUrlsKey (this.generalService.configs.baseURLs.GET_PROMOTIONS_LIST) + name);
    }
}