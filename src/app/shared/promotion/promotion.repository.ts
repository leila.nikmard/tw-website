import { Promotion } from './promotion.model';

export class PromotionRepository extends Promotion {
    
    public static setPromotion (obj: any): Promotion {
        const promotion = new Promotion();
        if (obj) {
            obj.category ? promotion.setCategory(obj.category) : promotion.doNothing();
            obj.obj_type ? promotion.setType(obj.obj_type) : promotion.doNothing();
            obj.valid_from ? promotion.setValidFrom(obj.valid_from) : promotion.doNothing();
            obj.image_path_large ? promotion.setImagePathLarge(obj.image_path_large) : promotion.doNothing();
            obj.description_fa ? promotion.setDescriptionFa(obj.description_fa) : promotion.doNothing();
            obj.image_path ? promotion.setImagePath(obj.image_path) : promotion.doNothing();
            obj.title ? promotion.setTitle(obj.title) : promotion.doNothing();
            obj.scope ? promotion.setScope(obj.scope) : promotion.doNothing();
            obj.description_en ? promotion.setDescriptionEn(obj.description_en) : promotion.doNothing();
            obj.order ? promotion.setOrder(obj.order) : promotion.doNothing();
            obj.favorite_list_id ? promotion.setFavoriteListId(obj.favorite_list_id) : promotion.doNothing();
            obj.enable ? promotion.setEnable(obj.enable) : promotion.doNothing();
            obj.link ? promotion.setLink(obj.link) : promotion.doNothing();
            obj.valid_to ? promotion.setValidTo(obj.valid_to) : promotion.doNothing();
            obj.image_path_small ? promotion.setImagePathSmall(obj.image_path_small) : promotion.doNothing();
            obj.occasion ? promotion.setOccasion(obj.occasion) : promotion.doNothing();
            obj.play_time ? promotion.setPlayTime(obj.play_time) : promotion.doNothing();
            obj.type ? promotion.setType(obj.type) : promotion.doNothing();
            obj.id ? promotion.setId(obj.id) : promotion.doNothing();
            obj.is_mobile ? promotion.setIsMobile(obj.is_mobile) : promotion.doNothing();
            obj.show_time ? promotion.setShowTime(obj.show_time) : promotion.doNothing();
            obj.current_past ? promotion.setCurrentPast(obj.current_past) : promotion.doNothing();
        }
        return promotion;
    }
    
    public static setPromotions (objects: Promotion[]): Promotion[] {
        const items: Promotion[] = [];
        for (let i = 0; i < objects.length; i++) {
            items.push(this.setPromotion(objects[ i ]));
        }
        return items;
    }
    
}