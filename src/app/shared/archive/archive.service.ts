import { Injectable } from '@angular/core';
import { GeneralService } from '../general/general.service';

@Injectable ()
export class ArchiveService {
    fetch_get_archive_details_error_retry_count = 0;
    
    constructor (private generalService: GeneralService) {
    }
    
    getSelectedDateEpisodes (router_url, channel_desc, selected_date_string, page) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url,
            this.generalService.configs.baseURLs.GET_SELECTED_DATE_EPISODES, {
                'channel_desc': channel_desc,
                'date'        : selected_date_string,
                'page'        : page
            });
        return this.generalService.makeGETRequest (final_service_url, true,
            this.generalService.configs.getBaseUrlsKey (this.generalService.configs.baseURLs.GET_SELECTED_DATE_EPISODES));
    }

    generateFriendlyDate(date) {
        let str_temp = date.split('-');
        return str_temp[2] + '-' + str_temp[1] + '-' + str_temp[0];
    }

    setArchivePageTitle(channel, date) {
        let output_title: string;
        output_title = 'آرشیو ' + channel.getName(true)  + " در تاریخ " + this.generateFriendlyDate(date);
        return output_title;
    }

    setArchivePageMetaTags(channel, date) {
        let output : any;
        output = [
            {
                name   : 'title',
                content: this.setArchivePageTitle(channel, date)
            },
            {
                name   : 'keywords',
                content: ',' + channel.getName(true) + 'آرشیو محتوایی صدا و سیما, آرشیو رایگان صدا و سیما, آرشیو شبکه '
            },
            {
                name   : 'description',
                content: 'آرشیو محتوای ' + channel.getName(true) + " در تاریخ " + '،' + ' آرشیو محتوای تمام شبکه های صدای و سیمای ' +
                    'ایران شامل شبکه های استانی و شبکه های ملی'
            },
        ];
        return output;
    }

    setArchivePageLinksTags(channel, date) {
        let output : any;
        output = [{ rel: 'canonical', href: 'http://www.telewebion.com/archive/' + channel.getDescriptor() + '/' + date }];
        return output;
    }
    
}
