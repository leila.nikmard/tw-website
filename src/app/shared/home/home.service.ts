import { Injectable } from '@angular/core';
import { GeneralService } from '../general/general.service';

@Injectable()
export class HomeService {
  custom_carousel_id_1 = 400390;
  custom_carousel_id_2 = 405193;
  custom_carousel_id_3 = 404847;

  constructor (private generalService: GeneralService) {}

  getHomePage (router_url) {
    const final_service_url = this.generalService.makeFinalRequestURL(router_url,
      this.generalService.configs.baseURLs.GET_HOME_PAGE_SERVICE_URL, {
        'custom1'     : this.custom_carousel_id_1,
        'custom2'     : this.custom_carousel_id_2,
        'custom3'     : this.custom_carousel_id_3,
        'logo_version': this.generalService.configs.liveLogoVersion,
        'thumb_size'  : this.generalService.configs.defaultEpisodeThumbSize
      });
    return this.generalService.makeGETRequest(final_service_url, true,
      this.generalService.configs.getBaseUrlsKey(this.generalService.configs.baseURLs.GET_HOME_PAGE_SERVICE_URL));
  }
}
