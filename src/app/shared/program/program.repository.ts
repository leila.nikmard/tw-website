import { Program } from './program.model';
import { ProgramTypesRepository } from '../program_type/program_types.repository';
import { ProgramGenreRepository } from '../program_genre/program_genre.repository';
import { CountryRepository } from '../country/country.repository';

export class ProgramRepository extends Program {
    
    constructor () {
        super();
    }
    
    public static setProgram (obj: any): Program {
        const program = new Program();
        if (obj) {
            obj.id ? program.setId(obj.id) : program.doNothing();
            obj.view_count ? program.setViewCount(obj.view_count) : program.doNothing();
            obj.cover_image_name ? program.setCoverImageName(obj.cover_image_name) : program.doNothing();
            obj.background_image_name ? program.setBackgroundImageName(obj.background_image_name) : program.doNothing();
            obj.tags ? program.setTags(obj.tags) : program.doNothing();
            obj.summary_fa ? program.setSummaryfa(obj.summary_fa) : program.doNothing();
            obj.summary_en ? program.setSummaryEn(obj.summary_en) : program.doNothing();
            obj.title ? program.setTitle(obj.title) : program.doNothing();
            obj.title_english ? program.setTitleEnglish(obj.title_english) : program.doNothing();
            obj.channel ? program.setChannel(obj.channel) : program.doNothing();
            obj.region ? program.setRegion(obj.region) : program.doNothing();
            obj.region_inclusion ? program.setRegionInclusion(obj.region_inclusion) : program.doNothing();
            obj.is_singleton ? program.setIsSingleton(obj.is_singleton) : program.doNothing();
            obj.program_genres ? program.setProgramGenres(ProgramGenreRepository.setProgramGenres(obj.program_genres)) : program.doNothing();
            obj.program_types ? program.setProgramTypes(ProgramTypesRepository.setProgramTypes(obj.program_types)) : program.doNothing();
            obj.producer_country ? program.setProducerCountry(CountryRepository.setCountry(obj.producer_country)) : program.doNothing();
        }
        return program;
    }
    
    public static setPrograms (objects: Program[]): Program[] {
        const items: Program[] = [];
        for (let i = 0; i < objects.length; i++) {
            items.push(ProgramRepository.setProgram(objects[ i ]));
        }
        return items;
    }
    
}