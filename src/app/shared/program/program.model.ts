import { Channel } from '../channel/channel.model';
import { ProgramGenre } from '../program_genre/program_genre.model';
import { Country } from '../country/country.model';
import { Base } from '../general/base.model';
import { ProgramType } from '../program_type/program_types.model';

export class Program extends Base {
    private title: string;
    private id: number;
    private view_count: number;
    private cover_image_name: string;
    private background_image_name: string;
    private tags: string;
    private summary_fa: string;
    private summary_en: string;
    private title_english: string;
    private page_description: string;
    private channel: Channel;
    private region: string;
    private region_inclusion: string;
    private producer_country: Country;
    private program_genres: ProgramGenre[];
    private program_types: ProgramType[];
    private is_singleton: number;
    
    constructor () {
        super();
    }
    
    setId (_id: number): void {
        this.id = _id;
    }
    
    getId (): number {
        return this.id;
    }
    
    setViewCount (_view_count: number): void {
        this.view_count = _view_count;
    }
    
    getViewCount (): number {
        return this.view_count ? this.view_count : 0;
    }
    
    setCoverImageName (_cover_image_name: string): void {
        this.cover_image_name = _cover_image_name;
    }
    
    getCoverImageName (): string {
        return this.cover_image_name;
    }
    
    setBackgroundImageName (_background_image_name: string): void {
        this.background_image_name = _background_image_name;
    }
    
    getBackgroundImageName (): string {
        return this.background_image_name;
    }
    
    setTags (_tags: string): void {
        this.tags = _tags;
    }
    
    getTags (): string {
        return this.tags ? this.tags : '';
    }
    
    setSummaryfa (_summary_fa: string): void {
        this.summary_fa = _summary_fa;
    }
    
    getSummaryFa (): string {
        return this.summary_fa;
    }
    
    setSummaryEn (_summary_en: string): void {
        this.summary_en = _summary_en;
    }
    
    getSummaryEn (): string {
        return this.summary_en;
    }
    
    setTitleEnglish (_title_english: string): void {
        this.title_english = _title_english;
    }
    
    getTitleEnglish (): string {
        return this.title_english;
    }
    
    setTitle (_title: string): void {
        this.title = _title;
    }
    
    getTitle (): string {
        return this.title;
    }
    
    setPageDescription (_page_description: string): void {
        this.page_description = _page_description;
    }
    
    getPageDescription (): string {
        return this.page_description;
    }
    
    setChannel (_channel: Channel): void {
        this.channel = _channel;
    }
    
    getChannel (): Channel {
        return this.channel;
    }
    
    setRegion (_region: string): void {
        this.region = _region;
    }
    
    getRegion (): string {
        return this.region;
    }
    
    setRegionInclusion (_region_inclusion: string): void {
        this.region_inclusion = _region_inclusion;
    }
    
    getRegionInclusion (): string {
        return this.region_inclusion;
    }
    
    setProducerCountry (_producer_country: Country): void {
        this.producer_country = _producer_country;
    }
    
    getProducerCountry (): Country {
        return this.producer_country ? this.producer_country : new Country();
    }
    
    setProgramGenres (_program_genres: ProgramGenre[]): void {
        this.program_genres = _program_genres;
    }
    
    getProgramGenres (): ProgramGenre[] {
        return this.program_genres ? this.program_genres : new Array(new ProgramGenre());
    }
    
    setProgramTypes (_program_types: ProgramType[]): void {
        this.program_types = _program_types;
    }
    
    getProgramTypes (): ProgramType[] {
        return this.program_types;
    }
    
    setIsSingleton (_is_singleton: number): void {
        this.is_singleton = _is_singleton;
    }
    
    getIsSingleton (): number {
        return this.is_singleton ? this.is_singleton : 0;
    }
}
