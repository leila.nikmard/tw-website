import { Injectable } from '@angular/core';
import { GeneralService } from '../general/general.service';

@Injectable ()
export class ProgramService {
    fetch_get_program_details_error_retry_count = 0;
    fetch_get_hottest_error_retry_count         = 0;
    fetch_get_kids_error_retry_count            = 0;
    fetch_get_newest_error_retry_count          = 0;
    fetch_get_series_error_retry_count          = 0;
    fetch_get_not_found_error_retry_count       = 0;
    fetch_get_search_error_retry_count          = 0;
    fetch_get_tag_error_retry_count             = 0;
    
    constructor (public generalService: GeneralService) {
    }
    
    getProgramPage (router_url, program_id) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url,
            this.generalService.configs.baseURLs.GET_PROGRAM_DETAILS_SERVICE_URL, {
                'program_id': program_id,
                'thumb_size': this.generalService.configs.defaultEpisodeThumbSize
            });
        return this.generalService.makeGETRequest (final_service_url, true,
            this.generalService.configs.getBaseUrlsKey (this.generalService.configs.baseURLs.GET_PROGRAM_DETAILS_SERVICE_URL) + '-' +
            program_id.toString ());
    }
    
    getProgramEpisodes (router_url, program_id, page) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url,
            this.generalService.configs.baseURLs.GET_PROGRAM_OTHER_EPISODES_SERVICE_URL, {
                'program_id': program_id,
                'page'      : page,
                'thumb_size': this.generalService.configs.defaultEpisodeThumbSize
            });
        return this.generalService.makeGETRequest (final_service_url, true,
            this.generalService.configs.getBaseUrlsKey (this.generalService.configs.baseURLs.GET_PROGRAM_OTHER_EPISODES_SERVICE_URL) +
            '-' + program_id.toString ());
    }
    
    getNewestProgram (router_url, page) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url,
            this.generalService.configs.baseURLs.GET_NEWEST_PROGRAM_SERVICE_URL, {
                'page'      : page,
                'thumb_size': this.generalService.configs.defaultEpisodeThumbSize
            });
        return this.generalService.makeGETRequest (final_service_url, true,
            this.generalService.configs.getBaseUrlsKey (this.generalService.configs.baseURLs.GET_PROGRAM_OTHER_EPISODES_SERVICE_URL));
    }
    
    getMostViewedProgram (router_url, page) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url,
            this.generalService.configs.baseURLs.GET_MOST_VIEWED_PROGRAM_SERVICE_URL, {
                'page'      : page,
                'type'      : this.generalService.configs.MostViewedType.DAILY,
                'thumb_size': this.generalService.configs.defaultEpisodeThumbSize
            });
        const is_cached         = page > 0 ? false : true;
        return this.generalService.makeGETRequest (final_service_url, is_cached,
            this.generalService.configs.getBaseUrlsKey (this.generalService.configs.baseURLs.GET_PROGRAM_OTHER_EPISODES_SERVICE_URL));
    }
    
    getProgramByCategoryGenre (router_url, categories, page, type = '') {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url,
            this.generalService.configs.baseURLs.GET_PROGRAMS_BY_CATEGORY_GENRE, {
                'category_id': this.generalService.arrayToString (categories, ','),
                'page'       : page,
                'order_type' : 'newest',
                'prod_ex'    : '',
                'thumb_size' : this.generalService.configs.defaultEpisodeThumbSize
            });
        return this.generalService.makeGETRequest (final_service_url, true,
            this.generalService.configs.getBaseUrlsKey (this.generalService.configs.baseURLs.GET_PROGRAMS_BY_CATEGORY_GENRE) + type);
    }
    
    getTagPrograms (router_url, page, tag) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url, this.generalService.configs.baseURLs.GET_TAG_PROGRAMS, {
            'page'    : page,
            'tag'     : tag,
            'order_by': 'all',
            'device'  : 'desktop'
        });
        return this.generalService.makeGETRequest (final_service_url, false);
    }
    
    getProgramSearchResult (router_url, context, page) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url, this.generalService.configs.baseURLs.GET_PROGRAM_SEARCH_RESULT_SERVICE_URL, {
            'page'        : page,
            'search_query': context,
            'thumb_size'  : this.generalService.configs.defaultEpisodeThumbSize
        });
        return this.generalService.makeGETRequest (final_service_url, false,
            this.generalService.configs.getBaseUrlsKey (this.generalService.configs.baseURLs.GET_PROGRAM_SEARCH_RESULT_SERVICE_URL));
    }

    setProgramPageTitle(program) {
        return program.getProgramTypes()[0].getId() === 10 || program.getProgramTypes()[0].getId() === 11 ? 'سریال ' +
            program.getTitle() : program.getTitle()
    }

    setProgramPageMetaTags(program) {
        let output : any;
        output = [
            {
                name: 'title', content: program.getProgramTypes()[0].getId() === 10 || program.getProgramTypes()[0].getId() === 11 ?
                    'سریال ' + program.getTitle() : program.getTitle()
            },
            {
                name: 'keywords', content: program.getProgramTypes()[0].getId() === 10 || program.getProgramTypes()[0].getId() === 11 ?
                    'سریال ' + program.getTitle() : program.getTitle() + ',' + program.getTags()
            },
            {
                name: 'description', content: program.getPageDescription()?
                    program.getPageDescription : program.getProgramTypes()[0].getId() === 10 || program.getProgramTypes()[0].getId() === 11 ?
                        'سریال ' + program.getTitle() + ' ، ' + 'تلوبیون مرجع پخش زنده شبکه های تلویزیون ، دانلود رایگان فیلم ایرانی و خارجی ، دانلود انیمیشن و کارتن ، سریال آرشیو و برناهه های صدا و سیما' : program.getTitle() + ' ، ' +
                        'تلوبیون مرجع پخش زنده شبکه های تلویزیون ، دانلود رایگان فیلم ایرانی و خارجی ، دانلود انیمیشن و کارتن ، سریال آرشیو و برناهه های صدا و سیما'
            },
            {name: 'og:type', content: 'website'},
            {name: 'DC.Identifier', content: 'http://www.telewebion.com/program/' + program.getId()},
            {name: 'og:image', content: program.getCoverImageName()},
            {name: 'og:image:secure_url', content: program.getCoverImageName()},
            {name: 'og:image:type', content: 'image/jpg'},
            {
                name: 'DC.Title', content: program.getProgramTypes()[0].getId() === 10 || program.getProgramTypes()[0].getId() === 11 ?
                    'سریال ' + program.getTitle() : program.getTitle()
            },
            {name: 'DC.Language', content: 'fa'},
            {name: 'DC.Publisher', content: 'تلوبیون'},
            {
                name: 'og:description', content: program.getPageDescription() ?
                    program.getPageDescription() : program.getProgramTypes()[0].getId() === 10 || program.getProgramTypes()[0].getId() === 11 ?
                        'سریال ' + program.getTitle() + ' ، ' + 'تلوبیون مرجع پخش زنده شبکه های تلویزیون ، دانلود رایگان فیلم ایرانی و خارجی ، دانلود انیمیشن و کارتن ، سریال آرشیو و برناهه های صدا و سیما' : program.getTitle() + ' ، ' +
                        'تلوبیون مرجع پخش زنده شبکه های تلویزیون ، دانلود رایگان فیلم ایرانی و خارجی ، دانلود انیمیشن و کارتن ، سریال آرشیو و برناهه های صدا و سیما'
            },
            {
                name: 'DC.Description', content: program.getPageDescription() ?
                    program.getPageDescription() : program.getProgramTypes()[0].getId() === 10 || program.getProgramTypes()[0].getId() === 11 ?
                        'سریال ' + program.getTitle() + ' ، ' + 'تلوبیون مرجع پخش زنده شبکه های تلویزیون ، دانلود رایگان فیلم ایرانی و خارجی ، دانلود انیمیشن و کارتن ، سریال آرشیو و برناهه های صدا و سیما' : program.getTitle() + ' ، ' +
                        'تلوبیون مرجع پخش زنده شبکه های تلویزیون ، دانلود رایگان فیلم ایرانی و خارجی ، دانلود انیمیشن و کارتن ، سریال آرشیو و برناهه های صدا و سیما'
            }
        ];
        return output;
    }

    setProgramPageLinksTags(program) {
        let output : any;
        output = [{ rel: 'canonical', href: 'http://www.telewebion.com/program/' + program.getId()}];
        return output;
    }
}
