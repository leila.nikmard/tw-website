import { Base } from '../general/base.model';

export class ProgramGenre extends Base {
    private name_fa: string;
    private name_en: string;
    private id: number;

    constructor () {
        super();
    }

    setId (_id: number): void {
        this.id = _id;
    }

    getId (): number {
        return this.id;
    }

    setName (_name_fa: string): void {
        this.name_fa = _name_fa;
    }

    getName (): string {
        return this.name_fa;
    }

    setNameEnglish (_name_en: string): void {
        this.name_en = _name_en;
    }

    getNameEnglish (): string {
        return this.name_en;
    }

}
