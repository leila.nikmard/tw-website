import { ProgramGenre } from './program_genre.model';

export class ProgramGenreRepository extends ProgramGenre {
    constructor () {
        super();
    }


    public static setProgramGenre (obj: any): ProgramGenre {
        const program_genre = new ProgramGenre();
        obj && obj.name_en ? program_genre.setNameEnglish(obj.name_en) : program_genre.doNothing();
        obj && obj.name_fa ? program_genre.setName(obj.name_fa) : program_genre.doNothing();
        obj && obj.id ? program_genre.setId(obj.id) : program_genre.doNothing();
        return program_genre;
    }

    public static setProgramGenres (objects: ProgramGenre[]): ProgramGenre[] {
        const items: ProgramGenre[] = [];
        for (let i = 0; i < objects.length; i++) {
            items.push(ProgramGenreRepository.setProgramGenre(objects[ i ]));
        }
        return items;
    }
}