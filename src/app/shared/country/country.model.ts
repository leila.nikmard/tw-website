import { Base } from '../general/base.model';

export class Country extends Base {
    private descriptor: string;
    private enable: boolean;
    private name: string;
    private weight: number;
    private id: number;

    constructor () {
        super();
    }

    setDescriptor (_descriptor): void {
        this.descriptor = _descriptor;
    }

    getDescriptor (): string {
        return this.descriptor;
    }

    setEnable (_enable: boolean): void {
        this.enable = _enable;
    }

    getEnable (): boolean {
        return this.enable;
    }

    setName (_name: string): void {
        this.name = _name;
    }

    getName (): string {
        return this.name;
    }

    setWeight (_weight: number): void {
        this.weight = _weight;
    }

    getWeight (): number {
        return this.weight;
    }

    setId (_id: number): void {
        this.id = _id;
    }

    getId (): number {
        return this.id;
    }

    toString (): string {
        return name;
    }

}
