import { Country } from './country.model';

export class CountryRepository extends Country {
    constructor () {
        super();
    }

    public static setCountry (obj: any): Country {
        const country = new Country();
        obj && obj.descriptor ? country.setDescriptor(obj.descriptor) : country.doNothing();
        obj && obj.name ? country.setName(obj && obj.name) : country.doNothing();
        obj && obj.enable ? country.setEnable(obj && obj.enable) : country.doNothing();
        obj && obj.weight ? country.setWeight(obj && obj.weight) : country.doNothing();
        obj && obj.id ? country.setId(obj && obj.id) : country.doNothing();
        return country;
    }

    public static setCountries (objects: Country[]): Country[] {
        const items: Country[] = [];
        for (let i = 0; i < objects.length; i++) {
            items.push(CountryRepository.setCountry(objects[ i ]));
        }
        return items;
    }

}
