import { User } from './user.model';

export class UserRepository extends User {
    
    public static setUser (obj: any): User {
        const user = new User();
        if (obj) {
            obj.name ? user.setName(obj.name) : user.doNothing();
            obj.phone ? user.setPhone(obj.phone) : user.doNothing();
            obj.email ? user.setEmail(obj.email) : user.doNothing();
            obj.id ? user.setId(obj.id) : user.doNothing();
        }
        return user;
    }
    
    public static setUsers (objects: User[]): User[] {
        const items: User[] = [];
        for (let i = 0; i < objects.length; i++) {
            items.push(this.setUser(objects[ i ]));
        }
        return items;
    }
    
}