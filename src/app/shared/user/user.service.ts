import { Injectable } from '@angular/core';
import { GeneralService } from '../general/general.service';

@Injectable ()
export class UserService {
    user_is_sign_in: boolean;
    
    constructor (private generalService: GeneralService) {
    }
    
    isSignInUser() {
        return this.generalService.getCookieValue (this.generalService.configs.variableNames.user_token) !== this.generalService.configs.variableNames.null_cookie;
    }
    
    getUserInfo (router_url: string) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url, this.generalService.configs.baseURLs.GET_USER_INFO, {});
        return this.generalService.makeGETRequest (final_service_url, true,
            this.generalService.configs.getBaseUrlsKey (this.generalService.configs.baseURLs.GET_USER_INFO));
    }
    
    signInUserWithPhone (router_url: string, params: Object): any {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url, this.generalService.configs.baseURLs.POST_SIGN_IN_WITH_PHONE, {});
        return this.generalService.makePOSTRequest (final_service_url, params);
    }
    
    verifyPhone (router_url: string, params: Object): any {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url, this.generalService.configs.baseURLs.POST_VERIFY_PHONE, {});
        return this.generalService.makePOSTRequest (final_service_url, params);
    }
    
    signInUserWithEmail (router_url: string, params: Object): any {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url, this.generalService.configs.baseURLs.POST_SIGN_IN_WITH_EMAIL, {});
        return this.generalService.makePOSTRequest (final_service_url, params);
    }
    
    registerEmail (router_url: string, name: string, email: string) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url, this.generalService.configs.baseURLs.GET_REGISTER_EMAIL, {
            'name' : name,
            'email': email
        });
        return this.generalService.makeGETRequest (final_service_url);
    }
    
    resetPassword (router_url: string, password: string) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url, this.generalService.configs.baseURLs.POST_RESET_PASSWORD, {
            'new_password': password
        });
        return this.generalService.makePOSTRequest (final_service_url, {});
    }
}
