import { Base } from '../general/base.model';

export class User extends Base {
    private id: number;
    private email: string;
    private name: string;
    private phone: string;
    
    constructor () {
        super();
    }
    
    setEmail (_email: string): void {
        this.email = _email;
    }
    
    getEmail (): string {
        return this.email ? this.email : '';
    }
    
    setName (_name: string): void {
        this.name = _name;
    }
    
    getName (): string {
        return this.name ? this.name : '';
    }
    
    setPhone (_phone: string): void {
        this.phone = _phone;
    }
    
    getPhone (): string {
        return this.phone ? this.phone : '';
    }
    
    setId (_id: number): void {
        this.id = _id;
    }
    
    getId (): number {
        return this.id ? this.id : 0;
    }

}
