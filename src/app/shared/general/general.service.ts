import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { AnalyticsService } from '../analytic/analytics.service';
import { JalaliDateService } from '../jalali-date/jalali.service';
import { isPlatformServer } from '@angular/common';
import { map } from 'rxjs/operators';
import { CookiesService } from './cookies.service';
import { Observable, of } from 'rxjs';
import { Meta, MetaDefinition, Title } from '@angular/platform-browser';
import { LinkDefinition, LinkService } from './link.service';
import { Configs, StaticPagesData } from './configs';
import { LocalStorageService } from './local-storage.service';
import { User } from '../user/user.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable ()
export class GeneralService {
    tsdb_url                       = 'http://tsdb.telewebion.com:4242/api/put';
    route: any;
    url: string;
    agent                          = 'desktop';
    current_user: User             = new User ();
    is_mobile                      = false;
    router_path: string;
    is_android                     = false;
    is_ios                         = false;
    indexForServer                 = 0;
    show_normal_content            = true;
    sub_content_is_loaded            = false;
    randomNumber: number;
    prob                           = 0.20;
    request_body                          = {
        code   : 200,
        message: ''
    };
    is_platform_server             = false;
    is_platform_browser            = true;
    fetch_router_error_retry_count = 0;
    fetch_sub_content_router_error_retry_count = 0;
    server_list                    = [
        'http://api.s1.telewebion.com/op',
        'http://api.st1.telewebion.com/op',
        'http://api.abroadorigin.telewebion.com/op',
        'http://api.w1.telewebion.com/op'
    ];
    
    constructor (private http: HttpClient, private analyticsService: AnalyticsService, private cookieService: CookiesService, private title: Title, public jalaliDateService: JalaliDateService,
                 private linkService: LinkService, private localStorage: LocalStorageService, @Inject (PLATFORM_ID) private platformId: Object, private meta: Meta, public configs: Configs,
                 public statics: StaticPagesData) {
        if (isPlatformServer (this.platformId)) {
            this.is_platform_server  = true;
            this.is_platform_browser = false;
        } else {
            this.is_mobile = this.checkUserAgent ();
            if (this.is_mobile) {
                this.agent = 'mobile';
            }
        }
        if (this.is_platform_browser) {
            const ua        = navigator.userAgent.toLowerCase ();
            this.is_android = ua.indexOf ('android') > -1;
            if (ua.indexOf ('ipad') > -1 || ua.indexOf ('ipod') > -1 || ua.indexOf ('iphone') > -1) {
                this.is_ios = true;
            }
        }
    }
    
    setTitle (title: string) {
        this.title.setTitle (title);
    }
    
    setMetaTags (meta_tags: Array<MetaDefinition>) {
        this.meta.addTags (meta_tags);
    }
    
    setLinkTag (tags: LinkDefinition[]) {
        for (let i = 0; i < tags.length; i++) {
            this.linkService.addTag (tags[ i ]);
        }
    }
    
    checkUserAgent (): boolean {
        return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test (navigator.userAgent));
    }
    
    getRouterHttpRequestMaker () {
        let headers = this.setHttpHeader ();
        if(this.is_platform_browser){
            headers = this.setHttpHeader (this.getCookieValue (this.configs.variableNames.user_token));
        }
        return this.http.get<any> (this.configs.baseURLs.ROUTER_BASE_URL, { headers: headers }).pipe (
            map (
                (res) => {
                    if (res.code === 200) {
                        this.router_path = res.data[ 0 ].webserver.split ('/op')[ 0 ];
                        this.setCookie (this.configs.variableNames.route_path, this.router_path);
                        return of ({ router: { value: this.router_path } });
                    }
                }
            )
        );
    }
    
    requestRouterUrl (force_request: boolean): any {
        if (this.is_platform_server) {
            return this.getRouterHttpRequestMaker ();
        } else if (this.is_platform_browser) {
            if (force_request) {
                return this.getRouterHttpRequestMaker ();
            } else {
                const router_path = this.getCookieValue (this.configs.variableNames.route_path);
                if (router_path === this.configs.variableNames.null_cookie) {
                    return this.getRouterHttpRequestMaker ();
                } else {
                    this.router_path = router_path;
                    return of ({ router: { value: this.router_path } });
                }
            }
        }
    }
    
    probGeneration (prob: number): boolean {
        this.randomNumber = Math.random ();
        return (this.randomNumber < prob);
    }
    
    makeHttpRequest (url, header, key, is_cached) {
        return this.http.get<any> (url, { headers: header }).pipe (
            map (
                (res) => {
                    //                if (res.headers.get (this.configs.variableNames.user_token) && res.headers.get (this.configs.variableNames.user_token).length > 10) {
                    //                    this.setCookie (this.configs.variableNames.user_status, res.headers.get (this.configs.variableNames.user_token), 365);
                    //                }
                    if (is_cached) {
                        let start_random_string = this.generateRandomString (14);
                        let end_random_string   = this.generateRandomString (7);
                        this.setLocalStorage (key, btoa (escape (JSON.stringify (res))), start_random_string, end_random_string);
                    }
                    return res;
                })
        );
    }
    
    makeGETRequest (url, is_cached: boolean = false, key: string = this.configs.variableNames.null_local_storage) {
        this.url    = url;
        let headers = this.setHttpHeader (this.getCookieValue (this.configs.variableNames.user_token));
        if (this.getCookieValue (this.configs.variableNames.user_token) === this.configs.variableNames.null_cookie) {
            this.setCookie (this.configs.variableNames.user_token, this.configs.variableNames.null_cookie);
        }
        if(this.is_platform_server) {
            is_cached = false;
        }
        if (is_cached) {
            if (this.getLocalStorage (key) &&
                this.getLocalStorage (key) !== this.configs.variableNames.null_local_storage &&
                this.getLocalStorage (key).length > 20) {
                let decoded_string = this.getLocalStorage (key).substring (14);
                return of (JSON.parse (unescape (atob (decoded_string.substring (0, decoded_string.length - 7)))));
            } else {
                return this.makeHttpRequest (url, headers, key, is_cached);
            }
        } else {
            return this.makeHttpRequest (url, headers, key, is_cached);
        }
    }
    
    makePOSTRequest (url, params) {
        let headers = this.setHttpHeader (this.getCookieValue (this.configs.variableNames.user_token));
        return this.http.post<any> (url, params, {
            headers: headers,
            observe: 'response'
        }).pipe (
            map (
                (res) => {
                    res.headers.getAll ('');
                    return {
                        body   : res.body,
                        headers: res.headers,
                        status : res.status
                    };
                }
            )
        );
    }
    
    updateUserToken (header) {
        header.forEach ((key, value) => {
            if (key === 'user-token') {
                this.setCookie (this.configs.variableNames.user_token, value[ 0 ]);
            }
        });
    }
    
    handleRequestError (error: Response) {
        let service_name: string;
        service_name = this.url.split ('/')[ 5 ].split ('?')[ 0 ];
        if (service_name === 'getChannelLinks' || service_name === 'getEpisodeDetails') {
            if (this.probGeneration (this.prob)) {
                this.analyticsService.emitEvent ('webservice', service_name, 'error');
            }
        }
        if (this.indexForServer <= 3) {
            if (this.is_platform_browser) {
                this.setCookie (this.configs.variableNames.route_path, this.server_list[ this.indexForServer ], 5);
            }
            this.indexForServer++;
        } else {
            return Observable.throw (error.json || 'Server error');
        }
    }
    
    setHttpHeader (user_token: string = this.configs.variableNames.null_cookie) {
        let headers = new HttpHeaders ();
        if (user_token !== this.configs.variableNames.null_cookie) {
            headers = headers.append ('User-Token', user_token);
        }
        return headers;
        //        const new_header = new HttpHeaders (
        //            {
        //                'Content-Type': 'application/json'
        //            }
        //        );
        //        if (userToken !== this.configs.variableNames.null_cookie) {
        //            new_header.append ('User-Token', userToken);
        //        }
        //        return new_header;
        //        if (this.getCookieValue (this.configs.variableNames.user_status) &&
        //            this.getCookieValue (this.configs.variableNames.user_status).length > 20) {
        //            new_header.append ('User-Token', this.getCookieValue (this.configs.variableNames.user_status));
        //        }
        //        if (userToken !== this.configs.variableNames.null_cookie) {
        //            new_header.append ('User-Token', userToken);
        //        }
    }
    
    makeQueryStringFromDictionary (params: Object) {
        let output = '?';
        for (const key of Object.keys (params)) {
            output += key + '=' + params[ key ] + '&';
        }
        return output;
    }
    
    makeFinalRequestURL (router_url, service_url, parameters) {
        return router_url + this.configs.urlPrefix + service_url + this.makeQueryStringFromDictionary (parameters);
    }
    
    setCookie (key: string, value: string, expire_time: number = 15, path: string = '/') {
        const date = new Date ();
        date.setTime (date.getTime () + (expire_time * 24 * 60 * 60 * 1000));
        this.cookieService.setCookie (key, value, date, path);
    }
    
    removeCookie (key: string, expire_time: string = '14', path: string = '/') {
        this.cookieService.deleteCookie (key, expire_time, path);
    }
    
    removeAllCookies () {
        this.cookieService.deleteAllCookies ();
    }
    
    getCookieValue (key: string): string {
        return this.cookieService.getCookieValue (key);
    }
    
    getAllCookie (): Object {
        return this.cookieService.getAllCookie ();
    }
    
    setLocalStorage (key: string, data: string, start_concat_string = '', end_concat_string = '') {
        this.localStorage.setLocalStorage (key, start_concat_string + data + end_concat_string);
    }
    
    getLocalStorage (key: string): string {
        return this.localStorage.getLocalStorage (key);
    }
    
    clearLocalStorage () {
        this.localStorage.clearLocalStorage ();
    }
    
    removeLocalStorage (key: string) {
        this.localStorage.removeLocalStorage (key);
    }
    
    getLocalStorageSize (): void {
        this.localStorage.getLocalStorageSize ();
    }
    
    mergeStringWithVariables (string, parameters) {
        for (let i = 0; i <= parameters.length - 1; i++) {
            string = string.replace ('%s', parameters[ i ]);
        }
        return string;
    }
    
    /**
     * It gets two array and merge them and output as one
     * Input: array_1[1,2,3], array+2[4,5,6]
     * Output: array[1,2,3,4,5,6]
     * */
    mergeOneArrayInAnother (items, all_items): any {
        for (let i = 0; i < items.length; i++) {
            all_items.push (items[ i ]);
        }
        return all_items;
    }
    
    /**
     * It returns current date in Jalali
     * Output: 1397/12/12
     * */
    getCurrentJalaliDate () {
        const temp_date = new Date ();
        return this.jalaliDateService.PersianCalendar (temp_date);
    }
    
    /**
     * It make a simple array to string
     * Input: array[1,2,3], separator = ,
     * Output: 1,2,3
     * */
    arrayToString (array, separator) {
        let output = '';
        for (let i = 0; i <= array.length - 1; i++) {
            output += array[ i ] + separator;
        }
        return output;
    }
    
    generateRandomString (len) {
        let text     = '';
        let possible = 'ABCDEF2634GHIJKLM23454g84OPQRg4gSThUVWXYZ4abcdef5345ghijklmn9op9qrstu69wxyz01hp2t34ed56f7b8N9';
        
        for (let i = 0; i < len; i++)
            text += possible.charAt (Math.floor (Math.random () * possible.length));
        
        return text;
    }
    
    showMessage (message_id, duration_second) {
        if (this.is_platform_browser) {
            let message = document.getElementById (message_id);
            if (message) {
                message.style.display = 'flex';
                setTimeout (() => {
                    this.hiddenMessage (message_id);
                }, duration_second * 1000);
            }
        }
    }
    
    hiddenMessage (message_id) {
        if (this.is_platform_browser) {
            document.getElementById (message_id).style.display = 'none';
        }
    }

    removeMetaTags() {
        let element = document.getElementsByTagName("meta"), index;
        let canonical = document.getElementsByTagName("link"), index_2;
        for (index = element.length - 1; index >= 0; index--) {
            if (element[index]['content'] != '#d10000' && element[index]['content'] != 'fa' && element[index]['content'] != 'text/html; charset=utf-8' && element[index]['content'] != 'ouX_weGqbVqZgAwap-1-Q5459gzIUjEl-xnRN8vo2yQ' && element[index]['content'] != 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0') {
                element[index].parentNode.removeChild(element[index]);
            }
        }
        for (index_2 = canonical.length -1; index_2>=0; index_2--) {
            if (canonical[index_2]['rel'] == 'canonical'){
                canonical[index_2].parentNode.removeChild(canonical[index_2]);
            }
        }
    }

    replaceDateMonthDigitToName (date_name) {
        let splity_date = date_name.split(' ');
        let date: string;
        if (splity_date.length > 0) {
            date = splity_date[ 0 ];
        }
        else {
            date = date_name;
        }
        splity_date    = date.split('-');
        let month_name = this.configs.monthToName[ String(splity_date[ 1 ]) ];
        return splity_date[ 2 ] + ' ' + month_name + ' ' + splity_date[ 0 ];
    }

    resetErrorCounts() {
        this.fetch_router_error_retry_count = 0;
        this.fetch_sub_content_router_error_retry_count = 0;
        this.request_body.message = '';
        this.request_body.code = 200;
        this.show_normal_content = true;
        this.sub_content_is_loaded = false;
    }
    //
    //  getCurrentMiladiiDate () {
    //    const current_date = new Date();
    //    return current_date.getFullYear() + '-' + (current_date.getMonth() + 1) +
    //      '-' + current_date.toDateString().split(' ')[ 2 ];
    //  }
    //
    //  replaceString (str, to_find, to_replace) {
    //    const str_temp = str.split(to_find);
    //    return str_temp.join(to_replace);
    //  }
    //
    //  convertPersianNumberToLatin (string) {
    //    string = this.replaceString(string, '۱', '1');
    //    string = this.replaceString(string, '۲', '2');
    //    string = this.replaceString(string, '۳', '3');
    //    string = this.replaceString(string, '۴', '4');
    //    string = this.replaceString(string, '۵', '5');
    //    string = this.replaceString(string, '۶', '6');
    //    string = this.replaceString(string, '۷', '7');
    //    string = this.replaceString(string, '۸', '8');
    //    string = this.replaceString(string, '۹', '9');
    //    string = this.replaceString(string, '۰', '0');
    //    return string;
    //  }
    //
    //  jalaliToGregorian (jy, jm, jd) {
    //    let gy;
    //    let days;
    //    let gm;
    //    if (jy > 979) {
    //      gy = 1600;
    //      jy -= 979;
    //    } else {
    //      gy = 621;
    //    }
    //    days = (365 * jy) + ((parseInt((jy / 33) + '')) * 8) + (parseInt((((jy % 33) + 3) / 4) + '')) + 78 + jd +
    //      ((jm < 7) ? (jm - 1) * 31 : ((jm - 7) * 30) + 186);
    //    gy += 400 * (parseInt((days / 146097) + ''));
    //    days %= 146097;
    //    if (days > 36524) {
    //      gy += 100 * (parseInt((--days / 36524) + ''));
    //      days %= 36524;
    //      if (days >= 365) {
    //        days++;
    //      }
    //    }
    //    gy += 4 * (parseInt((days / 1461) + ''));
    //    days %= 1461;
    //    if (days > 365) {
    //      gy += parseInt(((days - 1) / 365) + '');
    //      days = (days - 1) % 365;
    //    }
    //    let gd      = days + 1;
    //    const sal_a = [ 0, 31, ((gy % 4 === 0 && gy % 100 !== 0) || (gy % 400 === 0)) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
    //    for (gm = 0; gm < 13; gm++) {
    //      const v = sal_a[ gm ];
    //      if (gd <= v) {
    //        break;
    //      }
    //      gd -= v;
    //    }
    //    return [ gy, gm, gd ];
    //  }
    //
    //  generalItemParser (object_array, model) {
    //    const output: any[] = [];
    //    for (const item of object_array) {
    //      if (model === 'program') {
    //        item.image_to_show = item.background_image_name;
    //        item.item_link     = '/program/' + item.id;
    //        item.main_title    = item.title;
    //        output.push(item);
    //      }
    //      if (model === 'knowledge-graph') {
    //        item.image_to_show = item.poster_image;
    //        item.item_link     = '/trends/' + item.url_descriptor;
    //        item.main_title    = item.name_fa;
    //        output.push(item);
    //      }
    //    }
    //    return output;
    //  }
    
    //    getUserInfo(router) {
    //        let user_token = readCookie("tw_status");
    //        let user_info = readCookie("_gapd");
    //        if (user_token != this.configs.variableNames.null_cookie) {
    //            if(user_info == this.configs.variableNames.null_cookie) {
    //              const headers = new Headers();
    //              headers.append('Content-Type', 'application/json');
    //              const options2 = new RequestOptions({'headers': headers});
    //              return this.http.get(router.split("/op")[ 0 ] + this.get_user_info_url, options2).map(res => {
    //                const result = res.json();
    //                createCookie('_gapd', btoa(JSON.stringify(result.data[ 0 ])), 10);
    //                return {user: result.data[ 0 ]};
    //              }).catch(this.handleRequestError);
    //            } else {
    //              let user_data = JSON.parse(atob(this.readCookie('_gapd')));
    //              return Observable.of({
    //                "user": {user_data}
    //              });
    //            }
    //        } else {
    //          let user_data = JSON.parse(atob(this.readCookie('_gapd')));
    //          return Observable.of({
    //            "user": {}
    //          });
    //        }
    //    }
}



