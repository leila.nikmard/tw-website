import { Inject, Injectable } from '@angular/core';
import { Configs } from './configs';
import { DOCUMENT } from '@angular/platform-browser';
import { CookieService } from 'ngx-cookie-service';

@Injectable ()
export class CookiesService {
    
    private ZERO_CHAR = '0';
    
    constructor (private cookieService: CookieService, private configs: Configs, @Inject (DOCUMENT) private document) {
    }
    
    setCookie (key, value, expire_time, path) {
        //    this.cookieService.put(key, value, {
        //      expires: this.setExpireTime(expire_time),
        //      path   : path
        //    });
        //        this.document.cookie = key + '=' + value + '; expires=' + expire_time + '; path=' + path;
        this.cookieService.set (key, value, expire_time, path);
    }
    
    deleteCookie (key, expire_time, path) {
        this.cookieService.delete (key, path);
        //        (key, {
        //            expires: expire_time,
        //            path   : path
        //        });
    }
    
    deleteAllCookies () {
        this.cookieService.deleteAll ();
    }
    
    getCookieValue (key): string {
        if (this.cookieService.get (key) === '') {
            return this.configs.variableNames.null_cookie;
        } else {
            return this.cookieService.get (key);
        }
    }
    
    getAllCookie (): Object {
        return this.cookieService.getAll ();
    }
    
    setExpireTime (expire_time: number): string {
        const date = new Date ();
        date.setSeconds (0);
        date.setTime (date.getTime () + (expire_time * 60 * 1000));
        const time = date.toString ().split (' ')[ 4 ];
        const d    = date.getUTCDate ().toString ();
        const m    = date.getMonth ().toString ();
        const y    = date.getFullYear ().toString ();
        return this.addZeroDigit (y) + '-' + this.addZeroDigit (d) + '-' + this.addZeroDigit (m) + 'T' + time + '.000Z';
    }
    
    addZeroDigit (digit: string): string {
        if (digit.length <= 1) {
            return this.ZERO_CHAR + digit;
        }
        return digit;
    }
}
