import { Injectable } from '@angular/core';

@Injectable ()
export class Configs {
    baseURLs = {
        'ROUTER_BASE_URL'                            : 'http://router.televebion.net/v2/route',
        'GET_EPISODE_DETAILS_SERVICE_URL'            : 'programs/getEpisodeDetails',
        'GET_EPISODE_PAGE_SERVICE_URL'               : 'home/getEpisodePage',
        'GET_HOME_PAGE_SERVICE_URL'                  : 'home/getHomepage',
        'GET_CHANNEL_LINKS'                          : 'channels/getChannelLinks',
        'GET_SELECTED_DATE_EPISODES'                 : 'programs/getContentArchivePrograms',
        'GET_CHANNELS_LIST'                          : 'channels/getChannels',
        'GET_LIVE_PAGE'                              : 'home/getLivePage',
        'GET_HOURLY_ARCHIVE_EPISODES'                : 'hourly_archive/getHourlyArchivePrograms',
        'GET_PROGRAM_DETAILS_SERVICE_URL'            : 'programs/getProgramDetails',
        'GET_PROGRAM_OTHER_EPISODES_SERVICE_URL'     : 'programs/getOtherEpisodes',
        'GET_NEWEST_PROGRAM_SERVICE_URL'             : 'programs/getNewestPrograms',
        'GET_MOST_VIEWED_PROGRAM_SERVICE_URL'        : 'programs/getMostViewedPrograms',
        'GET_TAG_PROGRAMS'                           : 'programs/getTagProgram',
        'GET_TAG_EPISODES'                           : 'programs/getTagEpisodes',
        'GET_EPG_BY_DATE_SERVICE_URL'                : 'epg/getEpg',
        'GET_FAVORITE_LIST_EPISODES_SERVICE_URL'     : 'programs/getFavoritListEpisodes',
        'GET_FAVORITE_LIST_PROGRAMS_SERVICE_URL'     : 'programs/getFavoritListPrograms',
        'GET_FAVORITE_LIST_DATA_SERVICE_URL'         : 'home/getTrends',
        'GET_EPISODES_BY_CATEGORY_GENRE'             : 'programs/getEpisodesByCategoryGenre',
        'GET_PROGRAMS_BY_CATEGORY_GENRE'             : 'programs/getProgramsByCategoryGenre',
        'GET_PROMOTIONS_LIST'                        : 'home/getPromotions',
        'GET_USER_INFO'                              : 'user/getUserInfo',
        'GET_EPISODES_SEARCH_RESULT_SERVICE_URL'     : 'programs/getSearchEpisodes',
        'GET_PROGRAM_SEARCH_RESULT_SERVICE_URL'      : 'programs/getSearchPrograms',
        'GET_FAVORITE_LIST_SEARCH_RESULT_SERVICE_URL': 'favorite_list/search',
        'POST_SIGN_IN_WITH_PHONE'                    : 'user/signin',
        'POST_VERIFY_PHONE'                          : 'user/verify',
        'POST_SIGN_IN_WITH_EMAIL'                    : 'user/authenticate',
        'GET_REGISTER_EMAIL'                         : 'user/register',
        'POST_RESET_PASSWORD'                        : 'user/password/reset'
    };
    
    MostViewedType = {
        'DAILY'  : 4,
        'WEEKLY' : 3,
        'MONTHLY': 2,
        'YEARLY' : 1
    };
    
    urlPrefix               = '/v2/';
    liveLogoVersion         = '4';
    defaultEpisodeThumbSize = '240';
    cdnLiveIconURL          = 'http://static.televebion.net/web/images/../content_images/channel_images/thumbs/new/240/v4/';
    default_image_poster    = 'http://static.televebion.net/web/content_images/default_pic.jpg';
    
    timeout_services = 4000;
    timeout_request  = 1000;
    
    monthToName   = {
        '01': 'فروردین',
        '1' : 'فروردین',
        '02': 'اردیبهشت',
        '2' : 'اردیبهشت',
        '03': 'خرداد',
        '3' : 'خرداد',
        '04': 'تیر',
        '4' : 'تیر',
        '05': 'مرداد',
        '5' : 'مرداد',
        '06': 'شهریور',
        '6' : 'شهریور',
        '07': 'مهر',
        '7' : 'مهر',
        '08': 'آبان',
        '8' : 'آبان',
        '09': 'آذر',
        '9' : 'آذر',
        '10': 'دی',
        '11': 'بهمن',
        '12': 'اسفند'
    };
    variableNames = {
        expire_time       : 'EXPIRE_TIME_LOCAL_STORAGE',
        null_local_storage: 'NULL_LOCAL_STORAGE',
        null_cookie       : 'NULL_COOKIE',
        user_status       : 'USER_STATUS',
        user_token        : 'USER_TOKEN',
        route_path        : 'ROUTE_PATH',
        series            : '_FOR_SERIES_PAGE:',
        movies            : '_FOR_MOVIES_PAGE:',
        kids              : '_FOR_KIDS_PAGE:'
    };
    carouselTypes = {
        channels      : 'CHANNELS',
        movies        : 'MOVIES',
        series        : 'SERIES',
        episodes      : 'EPISODES',
        suggestion    : 'SUGGESTION',
        hourly_archive: 'HOURLY_ARCHIVE'
    };
    viewTypes     = {
        episode_cover_style               : 'EPISODE_COVER_STYLE',
        episode_poster_style              : 'EPISODE_POSTER_STYLE',
        episode_cover_style_with_promotion: 'EPISODE_COVER_STYLE_WITH_PROMOTION',
        program_cover_style               : 'PROGRAM_COVER_STYLE',
        program_poster_style              : 'PROGRAM_POSTER_STYLE',
        program_cover_style_with_promotion: 'PROGRAM_COVER_STYLE_WITH_PROMOTION',
        live                              : 'LIVE',
        live_with_select                  : 'LIVE_WITH_SELECT',
        hourly_archive                    : 'HOURLY_ARCHIVE'
    };
    
    getBaseUrlsKey (value: string): string {
        const ret = {};
        for (let i = 0; i < Object.keys (this.baseURLs).length; i++) {
            ret[ Object.values (this.baseURLs)[ i ] ] = Object.keys (this.baseURLs)[ i ];
        }
        return ret[ value ];
    }
}

@Injectable ()
export class StaticPagesData {
    pageTitles               = {
        'HOME_PAGE'         : 'تلوبیون | مرجع پخش زنده و دانلود فیلم ، سریال و سایر برنامه های تلویزیون',
        'HOTTEST'           : 'داغ ترین های تلوبیون',
        'CHANNELS'           : 'لیست پخش زنده شبکه های صدا و سیما',
        'NEWEST'            : 'جدیدترین های تلوبیون ',
        'NEWS'              : 'جدیدترین اخبار ایران و جهان',
        'OUR_RECOMMENDATION': 'پیشنهاد تلوبیون ، ویدئو ها و کلیپ ها',
        '404'               : 'صفحه مورد نظر یافت نشد',
        'MOVIES'            : 'آرشیو کامل فیلم های ایرانی و خارجی',
        'SERIES'            : 'آرشیو کامل سریال های ایرانی و خارجی',
        'KIDS'              : 'کارتون',
        'CONTACT_US'        : 'تماس با بخش پشتیبانی سایت تلوبیون',
        'DMCA'              : 'قانون حق تکثیر هزارهٔ دیجیتال',
        'JOBS'              : 'فرصت های شغلی تلوبیون',
        'PARTNERS'          : 'شرکای تجاری تلوبیون',
        'TERMS'             : 'قوانین و مقررات'
    };
    pageDescriptionsTemplate = {
        'LIVE_PAGE': 'پخش زنده و آرشیو %s ، تماشای آنلاین تلویزیون در اینترنت،' +
            ' پخش زنده فوتبال و تمامی شبکه های صدا و سیما در تلوبیون، مرجع پخش زنده تلویزیون '
    };
    pageMeta                 = {
        'HOME_PAGE'         : [
            {
                name   : 'keywords',
                content: 'پخش زنده شبکه ۳, پخش زنده شبکه ورزش, پخش زنده شبکه, دانلود سریال' +
                    ' ,دانلود فیلم, خندوانه, دورهمی, نود, اخبار بیست و سی, اخبار ایران و جهان'
            },
            {
                name   : 'description',
                content: 'تلوبیون مرجع پخش زنده و آرشیو شبکه های صدا و سیمای ایران ' +
                    'و آرشیو فیلم، سریال، فوتبال، خندوانه، دورهمی، نود و تمامی برنامه های شاد و مفرح ایران'
            }
        ],
        'HOTTEST'           : [
            {
                name   : 'description',
                content: 'داغترین های تلوبیون ، محبوبترین اخبار ، فیلم  و کلیپ های داغ اجتماعی ، سیاسی و فرهنگی ، خبرسا' +
                    'ز ترین سریال ها و فیلم های صدا و سیما ، تلوبیون مرجع پخش زنده شبکه های تلویزیون ، دانلود' +
                    ' رایگان فیلم ایرانی و خارجی ، دانلود انیمیشن و کارتن ، سریال آرشیو و برناهه های صدا و سیما'
            },
            {
                name   : 'keywords',
                content: 'داغترین های تلوبیون'
            }
        ],
        'OUR_RECOMMENDATION': [
            {
                name   : 'description',
                content: 'کلیپ ها و ویدئو های پیشنهادی تلوبیون شامل' +
                    ' ، اخبار ، سریال ، فیلم ، برنامه های صدا و سیما ، ویدئو های اجتماعی و فرهنگی' +
                    ' ایران و جهان ، تلوبیون مرجع پخش زنده شبکه های تلویزیون ، دانلود رایگان فیلم' +
                    ' ایرانی و خارجی ، دانلود انیمیشن و کارتن ، سریال آرشیو و برناهه های صدا و سیما'
            }
        ],
        'NEWEST'            : [
            {
                name   : 'description',
                content: 'دیسکریپشن : جدیدترین فیلم ها و سریال های تلویزیون ، کلیپ ها و رخداد های خبری اجتماعی سیاسی' +
                    'در ایران ، جدید ترین ویدئو های تلوبیون تلوبیون مرجع پخش زنده شبکه های تلویزیون ، دانلود' +
                    ' رایگان فیلم ایرانی و خارجی ، دانلود انیمیشن و کارتن ، سریال آرشیو و برناهه های صدا و سیما‎'
            }
        ],
        'NEWS'              : [
            {
                name   : 'description',
                content: 'اخبار ایران و جهان ، تیتر روزنامه های روز ایران ' +
                    '، اخبار روز و فیلم و کلیپ و ویدئو های خبری تلویزیون ، اخبار سیاسی ، اجتماعی ' +
                    'و فرهنگی در تلوبیون مرجع پخش زنده شبکه های تلویزیون ، دانلود رایگان فیلم ایرانی و خارجی' +
                    ' ، دانلود انیمیشن و کارتن ، سریال آرشیو و برناهه های صدا و سیما'
            }
        ],
        'CONTACT_US'        : [
            {
                name   : 'keywords',
                content: 'تماس با تلوبیون, پشتیبانی تلوبیون'
            },
            {
                name   : 'description',
                content: 'نظر سنجی و تماس با پشتیبانی وب سایت تلوبیون'
            }
        ],
        'DMCA'              : [
            {
                name   : 'keywords',
                content: 'قانون حق تکثیر'
            },
            {
                name   : 'description',
                content: 'قانون حق تکثیر وب سایت تلوبیون'
            }
        ],
        'JOBS'              : [
            {
                name   : 'keywords',
                content: 'فرصت های شغلی'
            },
            {
                name   : 'description',
                content: 'فرصت های شرکت سیمارایان شریف (‌تلوبیون)'
            }
        ],
        'PARTNERS'          : [
            {
                name   : 'keywords',
                content: 'شرکای تجاری, شرکای تجاری تلوبیون'
            },
            {
                name   : 'description',
                content: 'لیست شرکاتی تجاری شرکت تلوبیون'
            }
        ],
        'TERMS'             : [
            {
                name   : 'keywords',
                content: 'قوانین و مقررات'
            },
            {
                name   : 'description',
                content: 'قوانین و مقررات'
            }
        ],
        'MOVIES'             : [
            {
                name   : 'keywords',
                content: 'فیلم ایرانی, فیلم خارجی,فیلم آمریکایی,فیلم هندی, دانلود رایگان فیلم, آرشیو رایگان فیلم'
            },
            {
                name   : 'description',
                content: 'آرشیو کامل و دانلود کاملا رایگان فیلم های ایرانی و خارجی'
            }
        ],
        'KIDS'             : [
            {
                name   : 'keywords',
                content: 'کارتون, انیمیشن, باب اسفجی, دانلود کارتون, کارتون دوبله فارسی, دانلود کارتون دوبله'
            },
            {
                name   : 'description',
                content: 'کارتن ، دانلود کارتون ، دانلود کارتون دوبله فارسی' +
                    ' ، دانلود انیمیشن ، انیمیشن دوبله فارسی ، کارتون و انیمیشن های تلوزیون'
            }
        ],
        'SERIES'             : [
            {
                name   : 'keywords',
                content: 'سریال ایرانی, سریال خارجی,سریال آمریکایی' +
                    ',سریال هندی, دانلود رایگان سریال, آرشیو رایگان سریال'
            },
            {
                name   : 'description',
                content: 'آرشیو کامل و دانلود کاملا رایگان سریال های ایرانی و خارجی'
            }
        ],
        'CHANNELS'             : [
            {
                name   : 'keywords',
                content: 'پخش زنده شبکه ۳, پخش زنده شبکه ورزش, پخش زنده شبکه خبر, اخبار لحظه ایی'
            },
            {
                name   : 'description',
                content:  'پخش زنده تمامی شبکه های ملی و استانی صدا و سیما'
            }
        ]
    };
    pageLinkService          = {
        'HOME_PAGE'         : [
            {
                rel : 'canonical',
                href: 'www.telewebion.com'
            }
        ],
        'CHANNELS'         : [
            {
                rel : 'canonical',
                href: 'www.telewebion.com/channels'
            }
        ],
        'HOTTEST'           : [
            {
                rel : 'canonical',
                href: 'www.telewebion.com/hottest'
            }
        ],
        'NEWS'              : [
            {
                rel : 'canonical',
                href: 'www.telewebion.com/news'
            }
        ],
        'OUR_RECOMMENDATION': [
            {
                rel : 'canonical',
                href: 'www.telewebion.com/recommendation'
            }
        ],
        'NEWEST'            : [
            {
                rel : 'canonical',
                href: 'www.telewebion.com/newest'
            }
        ],
        'MOVIES'            : [
            {
                rel : 'canonical',
                href: 'www.telewebion.com/movies'
            }
        ],
        'KIDS'            : [
            {
                rel : 'canonical',
                href: 'www.telewebion.com/kids'
            }
        ],
        'SERIES'            : [
            {
                rel : 'canonical',
                href: 'www.telewebion.com/series'
            }
        ]
    };
    pageCategoryIds          = {
        'movies_first_category_id' : 8,
        'movies_second_category_id': 9,
        'series_first_category_id' : 10,
        'series_second_category_id': 11,
        'kids_first_category_id'   : 12,
        'kids_second_category_id'  : 0
    };
    error_message_500        = 'در ارتباط با سرور با مشکل مواجه شدیم، لطفا بعد از اطمینان وضعیت اینترنت خود چند لحظه بعد مجددا تلاش نمایید';
    error_message_500_sub_queries        = 'در دریافت اطلاعات دیگر اپیزودهای صفحه با خطا موجه شدیم، لحظاتی بعد مجددا تلاش میکنیم';
    error_message_500_id     = 'error-message-500';
    error_message_500_sub_queries_id     = 'error_message_500_sub_queries';
}
