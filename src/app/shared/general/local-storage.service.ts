import { Injectable } from '@angular/core';
import { Configs } from './configs';

@Injectable()
export class LocalStorageService {
    constructor (private configs: Configs) {}

    setLocalStorage (key, data) {
        localStorage.setItem(key, data);
    }

    getLocalStorage (key): string {
        if (localStorage.getItem(key) === null) {
            return this.configs.variableNames.null_local_storage;
        } else {
            return localStorage.getItem(key);
        }
    }

    clearLocalStorage () {
        localStorage.clear();
    }

    removeLocalStorage (key) {
        localStorage.removeItem(key);
    }

    getLocalStorageSize () {
        let _lsTotal = 0, _xLen, _x;
        for (_x of Object.keys(localStorage)) {
            _xLen = ((localStorage[ _x ].length + _x.length) * 2);
            if (isNaN((_xLen / 1024))) {
                _xLen = 0;
            }
            _lsTotal += parseFloat((_xLen / 1024).toFixed(2));
            console.log(_x.substr(0, 50) + ' = ' + (_xLen / 1024).toFixed(2) + ' KB');
        }
        console.log('Total = ' + _lsTotal + ' KB');
    }

}
