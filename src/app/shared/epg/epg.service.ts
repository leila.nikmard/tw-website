import { Injectable } from '@angular/core';
import { GeneralService } from '../general/general.service';
import { JalaliDateService } from '../jalali-date/jalali.service';

@Injectable()
export class EpgService {

    constructor (private generalService: GeneralService, private jalaliDateService: JalaliDateService) { }

    getEpgByDate (router_url, string_date, channel_id) {
        const date              = new Date(parseInt(string_date.split('-')[ 0 ]),
            parseInt(string_date.split('-')[ 1 ]) - 1, parseInt(string_date.split('-')[ 2 ]));
        const datePersian       = this.jalaliDateService.PersianCalendar(date);
        const final_service_url = this.generalService.makeFinalRequestURL(router_url,
            this.generalService.configs.baseURLs.GET_CHANNEL_LINKS, {
                'channel_id': channel_id,
                'date'      : datePersian
            });
        return this.generalService.makeGETRequest(final_service_url);
    }
}
