import { Epg } from './epg.model';

export class EpgRepository extends Epg {
    constructor () {
        super();
    }

    public static setEpg(obj: any): Epg {
        const epg = new Epg();
        obj && obj.name_item ? epg.setNameItem(obj.name_item) : epg.doNothing();
        obj && obj.name_media ? epg.setNameMedia(obj.name_media) : epg.doNothing();
        obj && obj.repeat ? epg.setRepeat(obj.repeat) : epg.doNothing();
        obj && obj.id_kelaket ? epg.setIdKelaket(obj.id_kelaket) : epg.doNothing();
        obj && obj.id_ghesmat ? epg.setIdGhesmat(obj.id_ghesmat) : epg.doNothing();
        obj && obj.id_kind ? epg.setIdKelaket(obj.id_kelaket) : epg.doNothing();
        obj && obj.desc_full ? epg.setDescFull(obj.desc_full) : epg.doNothing();
        obj && obj.logourl ? epg.setLogoUrl(obj.logourl) : epg.doNothing();
        obj && obj.logo ? epg.setLogo(obj.logo) : epg.doNothing();
        obj && obj.dec_web ? epg.setDecWeb(obj.dec_web) : epg.doNothing();
        obj && obj.id ? epg.setId(obj.id) : epg.doNothing();
        obj && obj.desc_summary ? epg.setDescSummary(obj.desc_summary) : epg.doNothing();
        obj && obj.num_kelaket ? epg.setNumKelaket(obj.num_kelaket) : epg.doNothing();
        obj && obj.epg_code ? epg.setEpgCode(obj.epg_code) : epg.doNothing();
        obj && obj.start_time ? epg.setStartTime(obj.start_time) : epg.doNothing();
        obj && obj.end_time ? epg.setEndTime(obj.end_time) : epg.doNothing();
        obj && obj.length_min ? epg.setLengthMin(obj.length_min) : epg.doNothing();
        obj && obj.id_typemedia ? epg.setIdTypemedia(obj.id_typemedia) : epg.doNothing();
        obj && obj.length ? epg.setLength(obj.length) : epg.doNothing();
        obj && obj.id_day_item ? epg.setIdDayItem(obj.id_day_item) : epg.doNothing();
        obj && obj.id_program ? epg.setIdProgram(obj.id_program) : epg.doNothing();
        return epg;
    }

    public static setEpges (objects: Epg[]): Epg[] {
        const items: Epg[] = [];
        for (let i = 0; i < objects.length; i++) {
            items.push(EpgRepository.setEpg(objects[ i ]));
        }
        return items;
    }
}
