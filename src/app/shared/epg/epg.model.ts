import { Base } from '../general/base.model';

export class Epg extends Base {
    private name_item: string;
    private name_media: string;
    private repeat: string;
    private id_kelaket: number;
    private id_ghesmat: number;
    private id_kind: number;
    private desc_full: string;
    private logourl: string;
    private logo: string;
    private dec_web: string;
    private id: number;
    private desc_summary: string;
    private num_kelaket: number;
    private epg_code: number;
    private start_time: string;
    private end_time: string;
    private length_min: string;
    private id_typemedia: number;
    private length: string;
    private id_day_item: string;
    private id_program: number;

    constructor () {
        super();
    }

    setNameItem (_name_item): void {
        this.name_item = _name_item;
    }

    getNameItem (): string {
        return this.name_item;
    }

    setNameMedia (_name_media): void {
        this.name_media = _name_media;
    }

    getNameMedia (): string {
        return this.name_media;
    }

    setRepeat (_repeat): void {
        this.repeat = _repeat;
    }

    getRepeat (): string {
        return this.repeat;
    }

    setIdKelaket (_id_kelaket): void {
        this.id_kelaket = _id_kelaket;
    }

    getIdKelaket (): number {
        return this.id_kelaket;
    }

    setIdGhesmat (_id_ghesmat): void {
        this.id_ghesmat = _id_ghesmat;
    }

    getIdGhesmat (): number {
        return this.id_ghesmat;
    }

    setIdKind (_id_kind): void {
        this.id_kind = _id_kind;
    }

    getIdKind (): number {
        return this.id_kind;
    }

    setDescFull (_desc_full): void {
        this.desc_full = _desc_full;
    }

    getDescFull (): string {
        return this.desc_full;
    }

    setLogoUrl (_logourl): void {
        this.logourl = _logourl;
    }

    getLogoUrl (): string {
        return this.logourl;
    }

    setLogo (_logo): void {
        this.logo = _logo;
    }

    getLogo (): string {
        return this.logo;
    }

    setDecWeb (_dec_web): void {
        this.dec_web = _dec_web;
    }

    getDecWeb (): string {
        return this.dec_web;
    }

    setId (_id): void {
        this.id = _id;
    }

    getId (): number {
        return this.id;
    }

    setDescSummary (_desc_summary): void {
        this.desc_summary = _desc_summary;
    }

    getDescSummary (): string {
        return this.desc_summary;
    }

    setNumKelaket (_num_kelaket): void {
        this.num_kelaket = _num_kelaket;
    }

    getNumKelaket (): number {
        return this.num_kelaket;
    }

    setEpgCode (_epg_code): void {
        this.epg_code = _epg_code;
    }

    getEpgCode (): number {
        return this.epg_code;
    }

    setStartTime (_start_time): void {
        this.start_time = _start_time;
    }

    getStartTime (): string {
        return this.start_time;
    }

    setEndTime (_end_time): void {
        this.end_time = _end_time;
    }

    getEndTime (): string {
        return this.end_time;
    }

    setLengthMin (_length_min): void {
        this.length_min = _length_min;
    }

    getLengthMin (): string {
        return this.length_min;
    }

    setIdTypemedia (_id_typemedia): void {
        this.id_typemedia = _id_typemedia;
    }

    getIdTypemedia (): number {
        return this.id_typemedia;
    }

    setLength (_length): void {
        this.length = _length;
    }

    getLength (): string {
        return this.length;
    }

    setIdDayItem (_id_day_item): void {
        this.id_day_item = _id_day_item;
    }

    getIdDayItem (): string {
        return this.id_day_item;
    }

    setIdProgram (_id_program): void {
        this.id_program = _id_program;
    }

    getIdProgram (): number {
        return this.id_program;
    }

    toString (): string {
        return this.name_item;
    }

}
