import { FavoriteList } from './favorite-list.model';

export class FavoriteListRepository extends FavoriteList {
    constructor () {
        super();
    }
    
    public static setFavoriteList (obj: any): FavoriteList {
        const fav_list = new FavoriteList();
        obj && obj.child_description ? fav_list.setChildDescription(obj.child_description) : fav_list.doNothing();
        obj && obj.id ? fav_list.setId(obj.id) : fav_list.doNothing();
        obj && obj.image ? fav_list.setImage(obj.image) : fav_list.doNothing();
        obj && obj.name_en ? fav_list.setNameEn(obj.name_en) : fav_list.doNothing();
        obj && obj.name_fa ? fav_list.setNameFa(obj.name_fa) : fav_list.doNothing();
        obj && obj.poster_image ? fav_list.setPosterImage(obj.poster_image) : fav_list.doNothing();
        obj && obj.poster_style ? fav_list.setPosterStyle(obj.poster_style) : fav_list.doNothing();
        obj && obj.url_descriptor ? fav_list.setUrlDescriptor(obj.url_descriptor) : fav_list.doNothing();
        return fav_list;
    }
    
    public static setFavoriteListTwoDimension (obj: FavoriteList[][]): FavoriteList[][] {
        const item: FavoriteList[][] = [];
        for (let i = 0; i < obj.length; i++) {
            item.push(FavoriteListRepository.setFavoriteLists(obj[ i ]));
        }
        return item;
    }
    
    public static setFavoriteLists (objects: any[]): FavoriteList[] {
        const items: FavoriteList[] = [];
        for (let i = 0; i < objects.length; i++) {
            items.push(FavoriteListRepository.setFavoriteList(objects[ i ]));
        }
        return items;
    }
}
