import { Injectable } from '@angular/core';
import { GeneralService } from '../general/general.service';

@Injectable ()
export class FavoriteListService {
    fetch_get_recommendation_details_error_retry_count = 0;
    fetch_get_tags_error_retry_count                   = 0;
    fetch_get_trends_error_retry_count                 = 0;
    
    constructor (public generalService: GeneralService) {
    }
    
    getFavoriteListProgram (router_url, favorite_list_id, page, order_type, singletons = null) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url,
            this.generalService.configs.baseURLs.GET_FAVORITE_LIST_PROGRAMS_SERVICE_URL, {
                'favorite_list_id': favorite_list_id,
                'singleton'       : singletons,
                'page'            : page,
                'thumb_size'      : this.generalService.configs.defaultEpisodeThumbSize,
                'order_type'      : order_type
            });
        return this.generalService.makeGETRequest (final_service_url, true,
            this.generalService.configs.getBaseUrlsKey (this.generalService.configs.baseURLs.GET_FAVORITE_LIST_PROGRAMS_SERVICE_URL));
    }
    
    getFavoriteListEpisodes (router_url, favorite_list_id, page, order_type, singletons = null) {
        let parameters: any;
        if (singletons === false || singletons === true) {
            parameters = {
                'favorite_list_id': favorite_list_id,
                'singleton'       : singletons,
                'page'            : page,
                'thumb_size'      : this.generalService.configs.defaultEpisodeThumbSize,
                'order_type'      : order_type
            };
        } else {
            parameters = {
                'favorite_list_id': favorite_list_id,
                'page'            : page,
                'thumb_size'      : this.generalService.configs.defaultEpisodeThumbSize,
                'order_type'      : order_type
            };
        }
        const final_service_url = this.generalService.makeFinalRequestURL (router_url,
            this.generalService.configs.baseURLs.GET_FAVORITE_LIST_EPISODES_SERVICE_URL, parameters);
        return this.generalService.makeGETRequest (final_service_url, true,
            this.generalService.configs.getBaseUrlsKey (this.generalService.configs.baseURLs.GET_FAVORITE_LIST_EPISODES_SERVICE_URL));
    }
    
    getFavoriteList (router_url, id_is_descriptor, favorite_list_id = null, favorite_list_descriptor = null) {
        let final_service_url: string;
        if (id_is_descriptor) {
            final_service_url = this.generalService.makeFinalRequestURL (router_url,
                this.generalService.configs.baseURLs.GET_FAVORITE_LIST_DATA_SERVICE_URL, {
                    'trend_descriptor': favorite_list_descriptor
                });
        } else {
            final_service_url = this.generalService.makeFinalRequestURL (router_url,
                this.generalService.configs.baseURLs.GET_FAVORITE_LIST_DATA_SERVICE_URL, {
                    'trend_id': favorite_list_id
                });
        }
        return this.generalService.makeGETRequest (final_service_url, false,
            this.generalService.configs.getBaseUrlsKey (this.generalService.configs.baseURLs.GET_FAVORITE_LIST_DATA_SERVICE_URL));
    }
    
    getFavoriteListSearchResult (router_url, context, page) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url, this.generalService.configs.baseURLs.GET_FAVORITE_LIST_SEARCH_RESULT_SERVICE_URL, {
            'page'        : page,
            'search_query': context,
            'thumb_size'  : this.generalService.configs.defaultEpisodeThumbSize
        });
        return this.generalService.makeGETRequest (final_service_url, false,
            this.generalService.configs.getBaseUrlsKey (this.generalService.configs.baseURLs.GET_FAVORITE_LIST_SEARCH_RESULT_SERVICE_URL));
    }
}
