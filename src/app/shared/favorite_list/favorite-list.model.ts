import { Base } from '../general/base.model';

export class FavoriteList extends Base {
    private child_description: string;
    private id: number;
    private image: string;
    private name_en: string;
    private name_fa: string;
    private poster_image: string;
    private poster_style: number;
    private url_descriptor: string;

    constructor () {
        super();
    }

    setChildDescription (_child_description: string): void {
        this.child_description = _child_description;
    }

    setId (_id: number): void {
        this.id = _id;
    }

    setImage (_image: string): void {
        this.image = _image;
    }

    setNameEn (_name_en: string): void {
        this.name_en = _name_en;
    }

    setNameFa (_name_fa: string): void {
        this.name_fa = _name_fa;
    }

    setPosterImage (_poster_image: string): void {
        this.poster_image = _poster_image;
    }

    setPosterStyle (_poster_style: number): void {
        this.poster_style = _poster_style;
    }

    setUrlDescriptor (_url_descriptor: string): void {
        this.url_descriptor = _url_descriptor;
    }

    getChildDescription (): string {
        return this.child_description ? this.child_description : '';
    }

    getId (): number {
        return this.id ? this.id : -1;
    }

    getImage (): string {
        return this.image ? this.image : '';
    }

    getNameEn (): string {
        return this.name_en ? this.name_en : '';
    }

    getNameFa (): string {
        return this.name_fa ? this.name_fa : '';
    }

    getPosterImage (): string {
        return this.poster_image ? this.poster_image : '';
    }

    getPosterStyle (): number {
        return this.poster_style ? this.poster_style : -1;
    }

    getUrlDescriptor (): string {
        return this.url_descriptor ? this.url_descriptor : '';
    }
}