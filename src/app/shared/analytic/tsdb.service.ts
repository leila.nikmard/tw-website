import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import { GeneralService } from '../general/general.service';

@Injectable()
export class TsdbService {

    constructor(private http: Http, private general: GeneralService) { }
    sendEvent(value, host , metric, service) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        const time = new Date();
        return this.http
            .put(this.general.tsdb_url, JSON.stringify({'metric': metric, 'timestamp': time.getTime(),
                'value': value, 'tags': {'host': host, 'service': service}}), {headers: headers})
            .subscribe();
    }
}
