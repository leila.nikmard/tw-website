import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

declare var ga: Function;

@Injectable()
export class AnalyticsService {

  constructor (@Inject(PLATFORM_ID) private platformId: Object) { }

  public emitEvent (eventCategory: string, eventAction: string, eventLabel: string = null, eventValue: number = null) {
    if (isPlatformBrowser(this.platformId)) {
      ga('send', 'event', {
        eventCategory: eventCategory,
        eventAction  : eventAction,
        eventLabel   : eventLabel,
        eventValue   : eventValue
      });
    }
  }
}
