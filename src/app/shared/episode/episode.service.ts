import { Injectable } from '@angular/core';
import { GeneralService } from '../general/general.service';

@Injectable ()
export class EpisodeService {
    fetch_get_movies_episode_error_retry_count = 0;
    fetch_get_news_episode_error_retry_count   = 0;
    fetch_get_search_episode_error_retry_count = 0;
    fetch_get_tag_episode_error_retry_count    = 0;
    
    constructor (private generalService: GeneralService) {
    }
    
    getEpisodeDetails (router_url, episode_id, device, user_token) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url,
            this.generalService.configs.baseURLs.GET_EPISODE_DETAILS_SERVICE_URL, {
                'episode_id'       : episode_id,
                'device'           : device,
                'download_link'    : '1',
                'user_token'       : user_token ? user_token : '',
                'enforce_copyright': this.generalService.is_platform_server ? 1 : 0,
                'src'              : 'pm'
            });
        return this.generalService.makeGETRequest (final_service_url);
    }
    
    getEpisodePage (router_url, episode_id, category_id, program_id) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url,
            this.generalService.configs.baseURLs.GET_EPISODE_PAGE_SERVICE_URL, {
                'episode_id': episode_id,
                'program_id': program_id,
                'thumb_size': this.generalService.configs.defaultEpisodeThumbSize
            });
        return this.generalService.makeGETRequest (final_service_url);
    }
    
    getEpisodesByCategoryGenre (router_url, categories, page, type) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url, this.generalService.configs.baseURLs.GET_EPISODES_BY_CATEGORY_GENRE, {
            'category_id': this.generalService.arrayToString (categories, ','),
            'page'       : page,
            'order_type' : 'newest',
            'prod_ex'    : '',
            'thumb_size' : this.generalService.configs.defaultEpisodeThumbSize
        });
        return this.generalService.makeGETRequest (final_service_url, true,
            this.generalService.configs.getBaseUrlsKey (this.generalService.configs.baseURLs.GET_EPISODES_BY_CATEGORY_GENRE) + type);
    }
    
    getEpisodeSearchResult (router_url, context, page) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url, this.generalService.configs.baseURLs.GET_EPISODES_SEARCH_RESULT_SERVICE_URL, {
            'page'        : page,
            'search_query': context,
            'thumb_size'  : this.generalService.configs.defaultEpisodeThumbSize
        });
        return this.generalService.makeGETRequest (final_service_url, false,
            this.generalService.configs.getBaseUrlsKey (this.generalService.configs.baseURLs.GET_EPISODES_SEARCH_RESULT_SERVICE_URL));
    }
    
    getTagEpisodes (router_url, page, tag) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url, this.generalService.configs.baseURLs.GET_TAG_EPISODES, {
            'page'    : page,
            'tag'     : tag,
            'order_by': 'all',
            'device'  : 'desktop'
        });
        return this.generalService.makeGETRequest (final_service_url, false);
    }
    
    refineFavoriteList (fav_list) {
        if (fav_list.length > 0) {
            let longest_array_temp: any[] = [];
            const single_arrays: any[]    = [];
            const final_result: any[]     = [];
            const temp_longest_ids: any[] = [];
            for (let i = 0; i < fav_list.length; i++) {
                if (fav_list[ i ].length === 1) {
                    single_arrays.push (fav_list[ i ]);
                }
                if (fav_list[ i ].length > longest_array_temp.length) {
                    longest_array_temp = [];
                    longest_array_temp.push (fav_list[ i ]);
                }
            }
            final_result.push (longest_array_temp[ 0 ]);
            for (let k = 0; k < longest_array_temp[ 0 ].length; k++) {
                temp_longest_ids.push (longest_array_temp[ 0 ][ k ].id);
            }
            for (let j = 0; j < single_arrays.length; j++) {
                if (!temp_longest_ids.includes (single_arrays[ j ][ 0 ].id)) {
                    final_result.push (single_arrays[ j ]);
                }
            }
            return final_result;
        }
    }
    
    setEpisodePageTitle (episode) {
        let output_title: string;
        let tv_series = [ '10', '11' ];
        if (episode.getProgram ().getIsSingleton () == true) {
            output_title = 'فیلم سینمایی ' + episode.getProgram ().getTitle ();
        } else {
            output_title = episode.getTitle () + ' - ' + episode.getProgram ().getTitle () + ' - ' + this.generalService.replaceDateMonthDigitToName (episode.getShowTime ());
            for (let item of episode.getProgramTypes ()) {
                if (tv_series.indexOf (String (item.getId ())) >= 0) {
                    output_title = episode.getTitle () + ' - ' + episode.getProgram ().getTitle ();
                    break;
                }
            }
        }
        return output_title;
    }
    
    setEpisodePageMetaTags (episode) {
        let output: any;
        let title = this.setEpisodePageTitle (episode);
        output    = [
            {
                name   : 'title',
                content: title
            },
            {
                name   : 'keywords',
                content: episode.getkeywordsEnglish () ?
                    episode.getkeywordsEnglish ().split ('#').join (',') : episode.getProgram ().getTitle () + ' - ' + episode.getTitle ()
            },
            {
                name   : 'description',
                content: episode.getPageDescription () ? episode.getPageDescription () : title + ' ،' +
                    ' ' + ' تلوبیون مرجع پخش زنده شبکه های تلویزیون ،' +
                    ' دانلود رایگان فیلم ایرانی و خارجی ، دانلود انیمیشن و کارتن ، سریال آرشیو و برناهه های صدا و سیما' + ' - ' + episode.getShowTime ().split (' ')[ 1 ]
            },
            {
                name   : 'og:title',
                content: title
            },
            {
                name   : 'og:type',
                content: 'video'
            },
            {
                name   : 'og:video:height',
                content: '600'
            },
            {
                name   : 'og:locale',
                content: 'fa_IR'
            },
            {
                name   : 'og:locale:alternate',
                content: 'en_US'
            },
            {
                name   : 'og:video:width',
                content: '800'
            },
            {
                name   : 'og:video:type',
                content: 'video.movie'
            },
            {
                name   : 'og:video',
                content: episode.getVodLink () && episode.getVodLink ().length > 0 ? episode.getVodLink ()[ 1 ].link : null
            },
            {
                name   : 'og:image',
                content: episode.getLargePicturePath ()
            },
            {
                name   : 'og:image:secure_url',
                content: episode.getLargePicturePath ()
            },
            {
                name   : 'og:image:type',
                content: 'image/jpg'
            },
            {
                name   : 'video:release_date',
                content: ''
            },
            {
                name   : 'DC.Identifier',
                content: 'http://www.telewebion.com/episode/' + episode.getId ()
            },
            {
                name   : 'DC.Date.Created',
                content: ''
            },
            {
                name   : 'DC.Type',
                content: 'video'
            },
            {
                name   : 'DC.Title',
                content: title
            },
            {
                name   : 'DC.Language',
                content: 'fa'
            },
            {
                name   : 'DC.Publisher',
                content: episode.getChannel (true)
            },
            {
                name   : 'og:video:url',
                content: episode.getVodLink () && episode.getVodLink ().length > 0 ? episode.getVodLink ()[ 1 ].link : null
            },
            {
                name   : 'og:video:type',
                content: 'video/mp4'
            },
            {
                name   : 'og:description',
                content: episode.getPageDescription () ? episode.getPageDescription () : title + ' ،' +
                    ' ' + ' تلوبیون مرجع پخش زنده شبکه های تلویزیون ،' +
                    ' دانلود رایگان فیلم ایرانی و خارجی ، دانلود انیمیشن و کارتن ، سریال آرشیو و برناهه های صدا و سیما' + ' - ' + episode.getShowTime ().split (' ')[ 1 ]
            },
            {
                name   : 'DC.Description',
                content: episode.getPageDescription () ? episode.getPageDescription () : title + ' ،' +
                    ' ' + ' تلوبیون مرجع پخش زنده شبکه های تلویزیون ،' +
                    ' دانلود رایگان فیلم ایرانی و خارجی ، دانلود انیمیشن و کارتن ، سریال آرشیو و برناهه های صدا و سیما' + ' - ' + episode.getShowTime ().split (' ')[ 1 ]
            }
        ];
        return output;
    }
    
    setEpisodePageLinksTags (episode) {
        let output: any;
        output = [ {
            rel : 'canonical',
            href: 'http://www.telewebion.com/episode/' + episode.getId ()
        } ];
        return output;
    }
    
    
    //
    //    generateSchemaDate (date) {
    //        let y            = parseInt(date.split(' ')[ 0 ].split('-')[ 0 ]);
    //        let m            = parseInt(date.split(' ')[ 0 ].split('-')[ 1 ]);
    //        let d            = parseInt(date.split(' ')[ 0 ].split('-')[ 2 ]);
    //        let jalaliObject = this.generalService.jalaliToGregorian(y, m, d);
    //        let hourMinute   = date.split(' ')[ 1 ];
    //        return jalaliObject.join('-') + ' ' + hourMinute;
    //    }
}
