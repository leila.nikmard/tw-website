import { Program } from '../program/program.model';
import { Channel } from '../channel/channel.model';
import { Moment } from './moments.model';
import { Base } from '../general/base.model';
import { ProgramGenre } from '../program_genre/program_genre.model';
import { ProgramType } from '../program_type/program_types.model';
import { FavoriteList } from '../favorite_list/favorite-list.model';
import { RefineChannelNamePipe } from '../../pipes/refineChannelName/refineChannelName.pipe';
import { RemoveApostrophePipe } from '../../pipes/rempveApostrophe/removeApostrophe.pipe';
import { GenerateVideoDuration } from '../../pipes/generateVideoDuration/generateVideoDuration';

export class Episode extends Base {
    private id: number;
    private show_time: string;
    private title: string;
    private title_english: string;
    private page_description: string;
    private description_fa: string;
    private view_count: number;
    private vod_link: any;
    private download_link: any;
    private keywords_english: string;
    private tags: string;
    private duration_mins: number;
    private has_video: boolean;
    private picture_path: string;
    private large_picture_path: string;
    private program: Program;
    private channel: Channel;
    private program_types: ProgramType[];
    private program_genres: ProgramGenre[];
    private moments: Moment;
    private file_path: string;
    private favorite_list: FavoriteList[][];
    
    constructor () {
        super ();
    }
    
    setId (_id: number): void {
        this.id = _id;
    }
    
    getId (): number {
        return this.id;
    }
    
    setShowTime (_show_time: string): void {
        this.show_time = _show_time;
    }
    
    getShowTime (): string {
        return this.show_time;
    }
    
    setTitle (_title: string): void {
        this.title = _title;
    }
    
    getTitle (): string {
        return this.title;
    }
    
    setTitleEnglish (_title_english: string): void {
        this.title_english = _title_english;
    }
    
    getTitleEnglish (): string {
        return this.title_english;
    }
    
    setPageDescription (_page_description: string): void {
        this.page_description = _page_description;
    }
    
    getPageDescription (): string {
        return this.page_description;
    }
    
    setDescription (_description_fa: string): void {
        this.description_fa = _description_fa;
    }
    
    getDescription (): string {
        return this.description_fa;
    }
    
    setViewCount (_view_count: number): void {
        this.view_count = _view_count;
    }
    
    getViewCount (): number {
        return this.view_count ? this.view_count : 0;
    }
    
    setVodLink (_vod_link: any): void {
        this.vod_link = _vod_link;
    }
    
    getVodLink (): any {
        return this.vod_link;
    }
    
    setDownloadLink (_download_link: any): void {
        this.download_link = _download_link;
    }
    
    getDownloadLink (): any {
        return this.download_link;
    }
    
    setKeywordsEnglish (_keywords_english: string): void {
        this.keywords_english = _keywords_english;
    }
    
    getkeywordsEnglish (): string {
        return this.keywords_english;
    }
    
    setTags (_tags: string): void {
        this.tags = _tags;
    }
    
    getTags (): string {
        return this.tags ? this.tags : '';
    }
    
    setDurationMins (_duration_mins: number): void {
        this.duration_mins = _duration_mins;
    }
    
    getDurationMins (format_vidoe = false): number {
        let removeApostrophePipe  = new RemoveApostrophePipe ();
        let generateVideoDuration = new GenerateVideoDuration ();
        
        if (format_vidoe) {
            return this.duration_mins ? removeApostrophePipe.transform (this.duration_mins) : 0;
        } else {
            return this.duration_mins ? generateVideoDuration.transform (removeApostrophePipe.transform (this.duration_mins)) : 0;
        }
    }
    
    setHasVideo (_has_video: boolean): void {
        this.has_video = _has_video;
    }
    
    getHasVideo (): boolean {
        return this.has_video;
    }
    
    setPicturePath (_picture_path: string): void {
        this.picture_path = _picture_path;
    }
    
    getPicturePath (): string {
        return this.picture_path;
    }
    
    setLargePiturePath (_large_picture_path: string): void {
        this.large_picture_path = _large_picture_path;
    }
    
    getLargePicturePath (): string {
        return this.large_picture_path ? this.large_picture_path : '';
    }
    
    setProgram (_program: Program): void {
        this.program = _program;
    }
    
    getProgram (): Program {
        return this.program;
    }
    
    setChannel (_channel: Channel): void {
        this.channel = _channel;
    }
    
    getChannel (should_refine = false): Channel {
        if (should_refine) {
            let refineChannelName = new RefineChannelNamePipe ();
            return refineChannelName.transform (this.channel);
        } else {
            return this.channel;
        }
    }
    
    setProgramTypes (_program_types: ProgramType[]): void {
        this.program_types = _program_types;
    }
    
    getProgramTypes (): ProgramType [] {
        return this.program_types;
    }
    
    setProgramGenres (_program_genres: ProgramGenre[]): void {
        this.program_genres = _program_genres;
    }
    
    getProgramGeneres (): ProgramGenre [] {
        return this.program_genres;
    }
    
    setMoment (_moment: Moment): void {
        this.moments = _moment;
    }
    
    getMoment (): Moment {
        return this.moments;
    }
    
    setFilePath (_file_path: string): void {
        this.file_path = _file_path;
    }
    
    getFilePath (): string {
        return this.file_path;
    }
    
    setFavoriteList (_favorite_list: FavoriteList[][]): void {
        this.favorite_list = _favorite_list;
    }
    
    getFavoriteList (): FavoriteList[][] {
        return this.favorite_list;
    }
    
    toString () {
        return this.title;
    }
    
}
