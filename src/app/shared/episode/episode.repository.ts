import { Episode } from './episode.model';
import { ProgramRepository } from '../program/program.repository';
import { ProgramTypesRepository } from '../program_type/program_types.repository';
import { ChannelRepository } from '../channel/channel.repository';

export class EpisodeRepository extends Episode {
    constructor () {
        super();
    }

    public static setEpisode (obj: any): Episode {
        const episode = new Episode();
        obj && obj.id ? episode.setId(obj.id) : episode.doNothing();
        obj && obj.description_fa ? episode.setDescription(obj.description_fa) : episode.doNothing();
        obj && obj.title ? episode.setTitle(obj.title) : episode.doNothing();
        obj && obj.title_english ? episode.setTitleEnglish(obj.title_english) : episode.doNothing();
        obj && obj.page_description ? episode.setPageDescription(obj.page_description) : episode.doNothing();
        obj && obj.view_count ? episode.setViewCount(obj.view_count) : episode.doNothing();
        obj && obj.show_time ? episode.setShowTime(obj.show_time) : episode.doNothing();
        obj && obj.vod_link ? episode.setVodLink(obj.vod_link) : episode.doNothing();
        obj && obj.download_link ? episode.setDownloadLink(obj.download_link) : episode.doNothing();
        obj && obj.keywords_english ? episode.setKeywordsEnglish(obj.keywords_english) : episode.doNothing();
        obj && obj.tags ? episode.setTags(obj.tags) : episode.doNothing();
        obj && obj.duration_mins ? episode.setDurationMins(obj.duration_mins) : episode.doNothing();
        obj && obj.has_video ? episode.setHasVideo(obj.has_video) : episode.doNothing();
        obj && obj.picture_path ? episode.setPicturePath(obj.picture_path) : episode.doNothing();
        obj && obj.large_picture_path ? episode.setLargePiturePath(obj.large_picture_path) : episode.doNothing();
        obj && obj.program ? episode.setProgram(ProgramRepository.setProgram(obj.program)) : episode.doNothing();
        obj && obj.channel ? episode.setChannel(ChannelRepository.setChannel(obj.channel)) : episode.doNothing();
        obj && obj.program_types ? episode.setProgramTypes(ProgramTypesRepository.setProgramTypes(obj.program_types)) : episode.doNothing();
        obj && obj.moments ? episode.setMoment(obj.moments) : episode.doNothing();
        obj && obj.program_genres ? episode.setProgramGenres(obj.program_genres) : episode.doNothing();
        obj && obj.file_path ? episode.setFilePath(obj.file_path) : episode.doNothing();
        obj && obj.favorite_list ? episode.setFavoriteList(obj.episode_favlists) : episode.doNothing();
        return episode;
    }

    public static setEpisodes (objects: Episode[]): Episode[] {
        const items: Episode[] = [];
        if (objects && objects.length > 0) {
            for (let i = 0; i < objects.length; i++) {
                items.push(EpisodeRepository.setEpisode(objects[ i ]));
            }
        }
        return items;
    }
}
