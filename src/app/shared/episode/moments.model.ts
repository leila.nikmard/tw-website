import { Base } from '../general/base.model';

export class Moment  extends Base {
    private id: number;
    private episode: number;
    private description: string;
    private created_at: string;
    private title_en: string;
    private title_fa: string;
    private source: string;
    private position_seconds: number;

    constructor () {
        super();
    }

    public static setObject (obj: Moment): Moment {
        const moment = new Moment();
        obj && obj.id ? moment.setId(obj.id) : moment.doNothing();
        obj && obj.episode ? moment.setEpisodeId(obj.episode) : moment.doNothing();
        obj && obj.description ? moment.setDescription(obj.description) : moment.doNothing();
        obj && obj.created_at ? moment.setCreatedAt(obj.created_at) : moment.doNothing();
        obj && obj.title_en ? moment.setTitleEnglish(obj.title_en) : moment.doNothing();
        obj && obj.title_fa ? moment.setTitle(obj.title_fa) : moment.doNothing();
        obj && obj.source ? moment.setSource(obj.source) : moment.doNothing();
        obj && obj.position_seconds ? moment.setPositionSeconds(obj.position_seconds) : moment.doNothing();
        return moment;
    }

    setId (_id: number): void {
        this.id = _id;
    }

    getId (): number {
        return this.id;
    }

    setEpisodeId (_episode: number): void {
        this.episode = _episode;
    }

    getEpisodeId (): number {
        return this.episode;
    }

    setDescription (_description: string): void {
        this.description = _description;
    }

    getDescription (): string {
        return this.description;
    }

    setCreatedAt (_created_at: string): void {
        this.created_at = _created_at;
    }

    getCreatedAt (): string {
        return this.created_at;
    }

    setTitleEnglish (_title_en: string): void {
        this.title_en = _title_en;
    }

    getTitleEnglish (): string {
        return this.title_en;
    }

    setTitle (_title: string): void {
        this.title_fa = _title;
    }

    getTitle (): string {
        return this.title_fa;
    }

    setSource (_source: string): void {
        this.source = _source;
    }

    getSource (): string {
        return this.source;
    }

    setPositionSeconds (_position_seconds: number): void {
        this.position_seconds = _position_seconds;
    }

    getPositionSeconds (): number {
        return this.position_seconds;
    }

    toString (): string {
        return this.title_fa;
    }

}
