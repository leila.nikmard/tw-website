import { Injectable } from '@angular/core';
import { GeneralService } from '../general/general.service';

@Injectable ()
export class HourlyArchiveService {
    
    constructor (private generalService: GeneralService) {
    }
    
    getHourlyArchive (router_url, channel_id, persian_date) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url,
            this.generalService.configs.baseURLs.GET_HOURLY_ARCHIVE_EPISODES, {
                'channel_id': channel_id,
                'date'      : persian_date,
                'src'       : 'pm'
            });
        return this.generalService.makeGETRequest (final_service_url, false);
    }
    
}
