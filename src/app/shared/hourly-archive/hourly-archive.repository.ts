import { HourlyArchive } from './hourly-archive.model';

export class HourlyArchiveRepository extends HourlyArchive {
    constructor () {
        super();
    }
    
    public static setHourlyArchive (obj: any): HourlyArchive {
        const item = new HourlyArchive();
        if (obj) {
            obj.title ? item.setTitle(obj.title) : item.doNothing();
            obj.picture_path ? item.setPicturePath(obj.picture_path) : item.doNothing();
            obj.vod_link ? item.setVodLink(obj.vod_link) : item.doNothing();
            obj.download_link ? item.setDownloadLink(obj.download_link) : item.doNothing();
        }
        return item;
    }
    
    public static setHourlyArchives (objects: any[]): HourlyArchive[] {
        const items: HourlyArchive[] = [];
        if (objects.length > 0) {
            for (let i = 0; i < objects.length; i++) {
                items.push(this.setHourlyArchive(objects[ i ]));
            }
        }
        return items;
    }
}
