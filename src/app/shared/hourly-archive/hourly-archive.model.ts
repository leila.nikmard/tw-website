import { Base } from '../general/base.model';

export class HourlyArchive extends Base {
    private title: string;
    private picture_path: string;
    private vod_link: string;
    private download_link: string;
    
    constructor () {
        super();
    }
    
    setTitle (_title): void {
        this.title = _title;
    }
    
    getTitle (): string {
        return this.title;
    }
    
    setPicturePath (_picture_path): void {
        this.picture_path = _picture_path;
    }
    
    getPicturePath (): string {
        return this.picture_path;
    }
    
    setVodLink (_vod_link): void {
        this.vod_link = _vod_link;
    }
    
    getVodLink (): string {
        return this.vod_link;
    }
    
    setDownloadLink (_download_link): void {
        this.download_link = _download_link;
    }
    
    getDownloadLink (): string {
        return this.download_link ? this.download_link : '';
    }
    
    toString (): string {
        return this.title;
    }
    
}
