import { Base } from '../general/base.model';

export class ChannelLink extends Base {
    private bitrate: number;
    private link: string;
    private isDefault: boolean;
    private title: string;

    constructor() {
        super();
    }

    getBitrate(): number {
        return this.bitrate;
    }

    setBitrate(_bitrate: number): void {
        this.bitrate = _bitrate;
    }

    getLink(): string {
        return this.link;
    }

    setLink(_link): void {
        this.link = _link;
    }

    getIsDefault(): boolean {
        return this.isDefault;
    }

    setIsDefault(_isDefault: boolean): void {
        this.isDefault = _isDefault;
    }

    getTitle(): string {
        return this.title;
    }

    setTitle(_title: string): void {
        this.title = _title;
    }
}
