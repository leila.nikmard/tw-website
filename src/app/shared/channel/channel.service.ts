import { Injectable } from '@angular/core';
import { GeneralService } from '../general/general.service';
import { JalaliDateService } from '../jalali-date/jalali.service';
import { ChannelLink } from './channel-link.model';

@Injectable ()
export class ChannelService {
    fetch_get_channel_links_error_retry_count            = 0;
    fetch_get_channels_error_retry_count                 = 0;
    fetch_get_channels_in_archive_page_error_retry_count = 0;
    fetch_get_live_error_retry_count                     = 0;
    
    constructor (private generalService: GeneralService, private jalaliDate: JalaliDateService) {
    }
    
    getChannels (router_url) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url,
            this.generalService.configs.baseURLs.GET_CHANNELS_LIST, {
                'logo_version': this.generalService.configs.liveLogoVersion,
                'thumb_size'  : this.generalService.configs.defaultEpisodeThumbSize
            });
        return this.generalService.makeGETRequest (final_service_url, true,
            this.generalService.configs.getBaseUrlsKey (this.generalService.configs.baseURLs.GET_CHANNELS_LIST));
    }
    
    getChannelLinks (router_url, descriptor) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url,
            this.generalService.configs.baseURLs.GET_CHANNEL_LINKS, {
                'channel_desc': descriptor,
                'device'      : 'desktop',
                'logo_version': this.generalService.configs.liveLogoVersion,
                'src'         : 'pm'
            });
        return this.generalService.makeGETRequest (final_service_url);
    }
    
    getLivePage (router_url, descriptor) {
        const final_service_url = this.generalService.makeFinalRequestURL (router_url,
            this.generalService.configs.baseURLs.GET_LIVE_PAGE, {
                'channel_desc': descriptor,
                'logo_version': this.generalService.configs.liveLogoVersion,
                'thumb_size'  : this.generalService.configs.defaultEpisodeThumbSize
            });
        return this.generalService.makeGETRequest (final_service_url, true,
            this.generalService.configs.getBaseUrlsKey (this.generalService.configs.baseURLs.GET_LIVE_PAGE));
    }
    
    getLiveActiveLink (links) {
        let active_link: string;
        let has_active = false;
        for (let i = 0; i < links.length; i++) {
            if (links[ i ].isDefault) {
                active_link = links[ i ].link;
                has_active  = true;
            }
        }
        if (!has_active) {
            active_link = links[ 0 ].link;
        }
        return active_link;
    }
    
    getLivePageDescription (channel_name) {
        return this.generalService.mergeStringWithVariables (this.generalService.statics.pageDescriptionsTemplate.LIVE_PAGE,
            [ channel_name ]);
    }
    
    generateLivePosterImage (descriptor) {
        return this.generalService.configs.cdnLiveIconURL + descriptor + '.png';
    }
    
    getLastDays (days) {
        const temp_date     = new Date ();
        const temp_array    = [];
        const readable_date = this.jalaliDate.PersianCalendar2 (temp_date);
        const value_date    = this.jalaliDate.PersianCalendar (temp_date);
        const temp_data     = {
            'id'   : value_date,
            'value': readable_date
        };
        temp_array[ 0 ]     = temp_data;
        for (let i = 0; i < days; i++) {
            temp_date.setDate (temp_date.getDate () - 1);
            const readable_date = this.jalaliDate.PersianCalendar2 (temp_date);
            const value_date    = this.jalaliDate.PersianCalendar (temp_date);
            const temp_data     = {
                'id'   : value_date,
                'value': readable_date
            };
            temp_array.push (temp_data);
        }
        return temp_array;
    }
    
    get500VodLink (vod_links: ChannelLink[]): ChannelLink {
        for (let i = 0; i < vod_links.length; i++) {
            if (vod_links[ i ].getBitrate ().toString () === '500') {
                return vod_links[ i ];
            }
        }
        return vod_links[ 1 ];
    }
    
    setChannelPageTitle (channel) {
        return 'پخش زنده ' + channel.getName (true);
    }
    
    setChannelPageMetaTags (channel) {
        let output: any;
        output = [
            {
                name   : 'title',
                content: channel.getName (true)
            },
            {
                name   : 'keywords',
                content: 'پخش زنده شبکه ۳, پخش زنده شبکه ورزش, پخش زنده شبکه خبر, اخبار لحظه ایی' + ',' + 'پخش زنده ' + channel.getName (true)
            },
            {
                name   : 'description',
                content: channel.getDescription ()
            }
        ];
        return output;
    }
    
    setChannelPageLinksTags (channel) {
        let output: any;
        output = [ {
            rel : 'canonical',
            href: 'http://www.telewebion.com/live/' + channel.getDescriptor ()
        } ];
        return output;
    }
}
