import { Channel } from './channel.model';

export class ChannelRepository extends Channel {
    constructor () {
        super();
    }

    public static setChannel (obj: any): Channel {
        const channel = new Channel();
        obj && obj.descriptor ? channel.setDescriptor(obj.descriptor) : channel.doNothing();
        obj && obj.id ? channel.setId(obj.id) : channel.doNothing();
        obj && obj.image_name ? channel.setImageName(obj.image_name) : channel.doNothing();
        obj && obj.name ? channel.setName(obj.name) : channel.doNothing();
        obj && obj.links ? channel.setLinks(obj.links) : channel.doNothing();
        obj && obj.description ? channel.setDescription(obj.description) : channel.doNothing();
        return channel;
    }

    public static setChannels (objects: Channel[]): Channel[] {
        const items: Channel[] = [];
        for (let i = 0; i < objects.length; i++) {
            items.push(this.setChannel(objects[ i ]));
        }
        return items;
    }

    public static getChannelDataFromChannelListByDescriptor(descriptor: string, channel_list: Channel[]): Channel {
        let channel = new Channel();
        for (let i = 0; i < channel_list.length; i++) {
            if(channel_list[i].getDescriptor() === descriptor){
                channel = ChannelRepository.setChannel(channel_list[i]);
            }
        }
        return channel;
    }
}