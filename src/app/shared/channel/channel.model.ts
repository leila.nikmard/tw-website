import { Base } from '../general/base.model';
import { ChannelLink } from './channel-link.model';
import { ChannelLinkRepository } from './channel-link.repository';
import { RefineChannelNamePipe } from '../../pipes/refineChannelName/refineChannelName.pipe';

export class Channel extends Base {
    private descriptor: string;
    private description: string;
    private id: number;
    private image_name: string;
    private name: string;
    private links: ChannelLink[];

    constructor() {
        super();
    }

    setDescriptor(_descriptor): void {
        this.descriptor = _descriptor;
    }

    getDescriptor(): string {
        return this.descriptor;
    }
    setDescription(_description): void {
        this.description = _description;
    }

    getDescription(): string {
        return this.description;
    }

    setId(_id): void {
        this.id = _id;
    }

    getId(): number {
        return this.id;
    }

    setImageName(_image_name): void {
        this.image_name = _image_name;
    }

    getImageName(): string {
        return this.image_name;
    }

    setName(_name): void {
        this.name = _name;
    }

    getName(should_refine=false): string {
        if (should_refine) {
            let refineChannelName = new RefineChannelNamePipe();
            return refineChannelName.transform(this)
        } else {
            return this.name;
        }
    }

    toString(): string {
        return name;
    }

    getLinks(): ChannelLink[] {
        return this.links;
    }

    setLinks(_links: ChannelLink[]): void {
        this.links = ChannelLinkRepository.setChannelsLink(_links);
    }

}
