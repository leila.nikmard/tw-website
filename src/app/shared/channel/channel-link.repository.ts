import { ChannelLink } from './channel-link.model';

export class ChannelLinkRepository extends ChannelLink {
    constructor () {
        super();
    }

    public static setChannelLink (obj: any): ChannelLink {
        const channelLink = new ChannelLink();
        obj && obj.bitrate ? channelLink.setBitrate(obj.bitrate) : channelLink.doNothing();
        obj && obj.link ? channelLink.setLink(obj.link) : channelLink.doNothing();
        obj && obj.isDefault ? channelLink.setIsDefault(obj.isDefault) : channelLink.doNothing();
        obj && obj.title ? channelLink.setTitle(obj.title) : channelLink.doNothing();
        return channelLink;
    }

    public static setChannelsLink (objects: any[]): ChannelLink[] {
        const items: ChannelLink[] = [];
        for (let i = 0; i < objects.length; i++) {
            items.push(this.setChannelLink(objects[ i ]));
        }
        return items;
    }
}