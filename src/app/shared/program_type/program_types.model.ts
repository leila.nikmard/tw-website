import { Base } from '../general/base.model';

export class ProgramType extends Base {
    private id: number;
    private title_fa: string;
    private title_en: string;
    private image_name: string;
    private descriptor: string;
    private priority: number;
    private order: number;

    constructor () {
        super();
    }

    setId (_id: number): void {
        this.id = _id;
    }

    getId (): number {
        return this.id;
    }

    setTitle (_title_fa: string): void {
        this.title_fa = _title_fa;
    }

    getTitle (): string {
        return this.title_fa;
    }

    setTitleEnglish (_title_en: string): void {
        this.title_en = _title_en;
    }

    getTitleEnglish (): string {
        return this.title_en;
    }

    setImageName (_image_name: string): void {
        this.image_name = _image_name;
    }

    getImageName (): string {
        return this.image_name;
    }

    setDescriptor (_descriptor: string): void {
        this.descriptor = _descriptor;
    }

    getDescriptor (): string {
        return this.descriptor;
    }

    setPriority (_priority: number): void {
        this.priority = _priority;
    }

    getPriority (): number {
        return this.priority;
    }

    setOrder (_order: number): void {
        this.order = _order;
    }

    getOrder (): number {
        return this.order;
    }

}
