import { ProgramType } from './program_types.model';

export class ProgramTypesRepository extends ProgramType {
    constructor () {
        super();
    }

    public static setProgramType (obj: any): ProgramType {
        const program_types = new ProgramType();
        obj && obj.id ? program_types.setId(obj.id) : program_types.doNothing();
        obj && obj.title_fa ? program_types.setTitle(obj.title_fa) : program_types.doNothing();
        obj && obj.title_en ? program_types.setTitleEnglish(obj.title_en) : program_types.doNothing();
        obj && obj.image_name ? program_types.setImageName(obj.image_name) : program_types.doNothing();
        obj && obj.descriptor ? program_types.setDescriptor(obj.descriptor) : program_types.doNothing();
        obj && obj.priority ? program_types.setPriority(obj.priority) : program_types.doNothing();
        obj && obj.order ? program_types.setOrder(obj.order) : program_types.doNothing();
        return program_types;
    }

    public static setProgramTypes (objects: ProgramType[]): ProgramType[] {
        const items: ProgramType[] = [];
        for (let i = 0; i < objects.length; i++) {
            items.push(ProgramTypesRepository.setProgramType(objects[ i ]));
        }
        return items;
    }
}