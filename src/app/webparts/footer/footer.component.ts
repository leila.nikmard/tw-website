import { Component, OnInit } from '@angular/core';

@Component ({
    selector   : 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls  : [ './footer.component.scss' ]
})
export class FooterComponent implements OnInit {
    
    public fast_access   = [
        {
            name : 'پخش زنده',
            route: '/'
        },
        {
            name : 'سریال',
            route: '/'
        },
        {
            name : 'فیلم',
            route: '/'
        },
        {
            name : 'برنامه کودک',
            route: '/'
        },
        {
            name : 'ورود / ثبت‌نام',
            route: '/'
        }
    ];
    public other_section = [
        {
            name : 'جدیدترینها',
            route: '/'
        },
        {
            name : 'پیشنهاد های‌ ما',
            route: '/'
        },
        {
            name : 'پیشنهاد کاربران',
            route: '/'
        },
        {
            name : 'بهترین های صدا و سیما',
            route: '/'
        }
    ];
    public about_tw      = [
        {
            name : 'قوانین و مقررات',
            route: '/terms'
        },
        {
            name : 'DMCA',
            route: '/dmca'
        },
        {
            name : 'فرصتهای شغلی',
            route: '/jobs'
        },
        {
            name : 'همکاران تجاری ما',
            route: '/partners'
        },
        {
            name : 'تماس با ما',
            route: '/contact'
        }
    ];
    public special_tw    = [
        {
            name : 'نود',
            route: '/'
        },
        {
            name : 'لیگ برتر',
            route: '/'
        },
        {
            name : 'خندوانه',
            route: '/'
        },
        {
            name : 'خنداننده شو',
            route: '/'
        },
        {
            name : 'برنده باش',
            route: '/'
        }
    ];
    public social        = [
        {
            name        : 'telegram',
            route       : 'https://telegram.me/telewebion',
            active_class: 'telegram-color'
        },
        {
            name        : 'linkedin',
            route       : '//www.linkedin.com/company/telewebion',
            active_class: 'linkedin-color'
        },
        {
            name        : 'instagram',
            route       : 'https://www.instagram.com/telewebioncom',
            active_class: 'instagram-color'
        },
        {
            name        : 'twitter',
            route       : 'https://twitter.com/telewebion',
            active_class: 'twitter-color'
        },
        {
            name        : 'facebook',
            route       : 'https://www.facebook.com/telewebion',
            active_class: 'facebook-color'
        }
    ];
    public download_list = [
        {
            image_src: '../../../assets/images/sibapp-badge.png',
            link     : 'https://new.sibapp.com/applications/telewebion'
        },
        {
            image_src: '../../../assets/images/bazaar-badge.png',
            link     : 'https://cafebazaar.ir/app/net.telewebion/?l=fa'
        },
        {
            image_src: '../../../assets/images/google-play-badge.png',
            link     : 'https://play.google.com/store/apps/details?id=net.telewebion&hl=en'
        }
    ];
    
    constructor () {
    }
    
    ngOnInit () {
    }
    
    //    openDropdown (index: number) {
    //        if (this.items[ index ].is_show) {
    //            this.items[ index ].is_show = !this.items[ index ].is_show;
    //        } else {
    //            for (let i = 0; i < this.items.length; i++) {
    //                this.items[ i ].is_show = false;
    //            }
    //            this.items[ index ].is_show = !this.items[ index ].is_show;
    //        }
    //    }
}
