import { Component, Input, OnInit } from '@angular/core';
import { Configs } from '../../shared/general/configs';

@Component ({
    selector   : 'app-program-description',
    templateUrl: './program-description.component.html',
    styleUrls  : [ './program-description.component.scss' ]
})
export class ProgramDescriptionComponent implements OnInit {
    @Input () Item;
    @Input () Type;
    
    public type;
    public title;
    public program_image;
    public genre;
    public category;
    public producer_country;
    public summary;
    public channel;
    public date;
    public time;
    public term;
    
    show_content = true;
    
    constructor (public configs: Configs) {
    }
    
    ngOnInit () {
        this.type     = (this.Item.getProgramTypes () && this.Item.getProgramTypes ().length > 0) ? this.Item.getProgramTypes ()[ 0 ].getDescriptor () : '';
        this.title    = this.Item.getTitle ();
        this.category = (this.Item.getProgramTypes () && this.Item.getProgramTypes ().length > 0) ? this.Item.getProgramTypes ()[ 0 ].getTitle () : '';
        if (this.Type === 'episode') {
            this.program_image    = this.Item.getPicturePath ();
            this.channel          = this.Item.getChannel ().getName ();
            this.date             = this.Item.getShowTime ();
            this.time             = this.Item.getShowTime ().split (' ')[ 1 ];
            this.term             = this.Item.getDurationMins ();
            this.genre            = '';
            this.producer_country = '';
            this.summary          = '';
        } else if (this.Type === 'program') {
            this.program_image    = this.Item.getBackgroundImageName ();
            this.genre            = this.Item.getProgramGenres ()[ 0 ].getName ();
            this.producer_country = this.Item.getProducerCountry ().getName ();
            this.summary          = this.Item.getSummaryFa ();
        }
    }
    
}
