import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgramDescriptionComponent } from './program-description.component';
import { ReplaceDateWithMonthNameModule } from '../../pipes/replaceDateWithMonthNmae/replaceDateWithMonthName.module';
import { NumberToPersianModule } from '../../pipes/numberToPersian/numberToPersian.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule({
    imports     : [
        CommonModule,
        ReplaceDateWithMonthNameModule,
        NumberToPersianModule,
        LazyLoadImageModule
    ],
    declarations: [ ProgramDescriptionComponent ],
    exports     : [ ProgramDescriptionComponent ]
})
export class ProgramDescriptionModule {
}
