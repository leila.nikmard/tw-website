import { Component, Input, OnInit } from '@angular/core';
import { FavoriteList } from '../../shared/favorite_list/favorite-list.model';

@Component({
    selector   : 'app-breadcrumb',
    templateUrl: './breadcrumb.component.html',
    styleUrls  : [ './breadcrumb.component.scss' ]
})
export class BreadcrumbComponent implements OnInit {
    @Input() Items: FavoriteList[];
    
    constructor () { }
    
    ngOnInit () {
    }
    
}
