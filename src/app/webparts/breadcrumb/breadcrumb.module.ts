import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadcrumbComponent } from './breadcrumb.component';
import { RouterModule } from '@angular/router';
import { NumberToPersianModule } from '../../pipes/numberToPersian/numberToPersian.module';

@NgModule({
    imports     : [
        CommonModule,
        RouterModule,
        NumberToPersianModule
    ],
    declarations: [ BreadcrumbComponent ],
    exports     : [ BreadcrumbComponent ]
})
export class BreadcrumbModule {
}
