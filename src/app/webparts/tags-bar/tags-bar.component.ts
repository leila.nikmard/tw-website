import { Component, Input, OnInit } from '@angular/core';
import { FavoriteList } from '../../shared/favorite_list/favorite-list.model';

@Component({
    selector   : 'app-tags-bar',
    templateUrl: './tags-bar.component.html',
    styleUrls  : [ './tags-bar.component.scss' ]
})
export class TagsBarComponent implements OnInit {
    @Input() Items: FavoriteList;
    
    constructor () { }
    
    ngOnInit () {
    }
    
}
