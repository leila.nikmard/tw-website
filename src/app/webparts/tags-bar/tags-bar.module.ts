import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NumberToPersianModule } from '../../pipes/numberToPersian/numberToPersian.module';
import { TagsBarComponent } from './tags-bar.component';

@NgModule({
    imports     : [
        CommonModule,
        RouterModule,
        NumberToPersianModule
    ],
    declarations: [ TagsBarComponent ],
    exports     : [ TagsBarComponent ]
})
export class TagsBarModule {
}
