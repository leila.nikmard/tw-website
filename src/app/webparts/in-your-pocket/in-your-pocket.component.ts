import { Component, OnInit } from '@angular/core';

@Component ({
    selector   : 'app-in-your-pocket',
    templateUrl: './in-your-pocket.component.html',
    styleUrls  : [ './in-your-pocket.component.scss' ]
})
export class InYourPocketComponent implements OnInit {
    public download_list = [
        {
            image_src: '../../../assets/images/sibapp-badge.png',
            link     : 'https://new.sibapp.com/applications/telewebion'
        },
        {
            image_src: '../../../assets/images/bazaar-badge.png',
            link     : 'https://cafebazaar.ir/app/net.telewebion/?l=fa'
        },
        {
            image_src: '../../../assets/images/google-play-badge.png',
            link     : 'https://play.google.com/store/apps/details?id=net.telewebion&hl=en'
        }
    ];
    
    constructor () {
    }
    
    ngOnInit () {
    }
    
}
