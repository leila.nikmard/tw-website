import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InYourPocketComponent } from './in-your-pocket.component';

describe('InYourPocketComponent', () => {
  let component: InYourPocketComponent;
  let fixture: ComponentFixture<InYourPocketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InYourPocketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InYourPocketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
