import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InYourPocketComponent } from './in-your-pocket.component';

@NgModule ({
    imports     : [
        CommonModule
    ],
    declarations: [ InYourPocketComponent ],
    exports     : [ InYourPocketComponent ]
})
export class InYourPocketModule {
}
