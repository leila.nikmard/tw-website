import { NgModule } from '@angular/core';
import { CarouselComponent } from './carousel.component';
import { CommonModule } from '@angular/common';
import { NumberToPersianModule } from '../../pipes/numberToPersian/numberToPersian.module';
import { RouterModule } from '@angular/router';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule({
    declarations: [ CarouselComponent ],
    exports     : [ CarouselComponent ],
    imports     : [
        CommonModule,
        RouterModule,
        NumberToPersianModule,
        LazyLoadImageModule
    ]
})

export class CarouselModule {

}