import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GeneralService } from '../../shared/general/general.service';

@Component({
    selector   : 'app-carousel',
    templateUrl: './carousel.component.html',
    styleUrls  : [ './carousel.component.scss' ]
})
export class CarouselComponent implements OnInit {
    @Input() Items;
    @Input() Type;
    @Input() Title;
    @Input() TitleRoute;
    @Input() IsLazyLoadImage: boolean;
    @Output() SelectHourlyArchive = new EventEmitter();
    
    type_movie      = false;
    type_channels   = false;
    type_episodes   = false;
    type_suggestion = false;
    
    constructor (public generalService: GeneralService) { }
    
    ngOnInit () {
        if (this.Type === this.generalService.configs.carouselTypes.movies ||
            this.Type === this.generalService.configs.carouselTypes.series) {
            this.type_movie = true;
        } else if (this.Type === this.generalService.configs.carouselTypes.channels) {
            this.type_channels = true;
        } else if (this.Type === this.generalService.configs.carouselTypes.episodes) {
            this.type_episodes = true;
        } else if (this.Type === this.generalService.configs.carouselTypes.suggestion) {
            this.type_suggestion = true;
        }
    }
    
    selectHourlyArchive (select_item) {
        this.SelectHourlyArchive.emit(select_item);
    }
    
}
