import { Component, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { RadiantPlayerComponent } from '../radiant-player/radiant-player.component';

@Component({
    selector   : 'app-social-info-bar',
    templateUrl: './social-info-bar.component.html',
    styleUrls  : [ './social-info-bar.component.scss' ]
})
export class SocialInfoBarComponent implements OnInit {
    @Input() Type;
    @Input() EpisodeTitle;
    @Input() ProgramTitle;
    @Input() ProgramId;
    @Input() PageId;
    @Input() ItemId;
    @Input() ShareFacebookLink;
    @Input() ShareTelegramLink;
    @Input() ShareIframeLink;
    @Input() DownloadLinks;
    @Input() ViewCount;
    @Input() Moments;
    @Output() SeekToSec = new EventEmitter();
    
    download_dropdown   = false;
    share_dropdown      = false;
    moments_dropdown    = false;
    download_last_state = false;
    share_last_state    = false;
    moment_last_state   = false;
    
    constructor () { }
    
    @HostListener('document:click', [ '$event' ]) onDocumentClick (event) {
        this.download_dropdown = false;
        this.share_dropdown    = false;
        this.moments_dropdown  = false;
        this.toggleDropDown(event, 'دانلود', 'fa-cloud-download', 'download');
        this.toggleDropDown(event, 'اشتراک', 'fa-share-alt', 'share');
        this.toggleDropDown(event, 'لحضه ها', 'fa-clock-o', 'moments');
    }
    
    ngOnInit () {
    }
    
    toggleDropDown (event, innerHtml, className, type) {
        if (type === 'share') {
            if (event.target.innerHTML === innerHtml || event.srcElement.classList[ 1 ] === className) {
                if (!this.share_last_state) {
                    this.share_dropdown   = true;
                    this.share_last_state = true;
                } else {
                    this.share_last_state = false;
                    this.share_dropdown   = false;
                }
            } else {
                this.share_last_state = false;
            }
        } else if (type === 'download') {
            if (event.target.innerHTML === innerHtml || event.srcElement.classList[ 1 ] === className) {
                if (!this.download_last_state) {
                    this.download_dropdown   = true;
                    this.download_last_state = true;
                } else {
                    this.download_last_state = false;
                    this.download_dropdown   = false;
                }
            } else {
                this.download_last_state = false;
            }
        } else if (type === 'moments') {
            if (event.target.innerHTML === innerHtml || event.srcElement.classList[ 1 ] === className) {
                if (!this.moment_last_state) {
                    this.moments_dropdown  = true;
                    this.moment_last_state = true;
                } else {
                    this.moment_last_state = false;
                    this.moments_dropdown  = false;
                }
            } else {
                this.moment_last_state = false;
            }
        }
    }
    
    
    seekTo (second) {
        this.SeekToSec.emit(second);
        window.scrollTo(0, 0);
    }
}
