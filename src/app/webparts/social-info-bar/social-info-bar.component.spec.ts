import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialInfoBarComponent } from './social-info-bar.component';

describe('SocialInfoBarComponent', () => {
  let component: SocialInfoBarComponent;
  let fixture: ComponentFixture<SocialInfoBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocialInfoBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialInfoBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
