import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SocialInfoBarComponent } from './social-info-bar.component';
import { NumberToPersianModule } from '../../pipes/numberToPersian/numberToPersian.module';
import { RouterModule } from '@angular/router';

@NgModule({
    imports     : [
        CommonModule,
        NumberToPersianModule,
        RouterModule
    ],
    declarations: [ SocialInfoBarComponent ],
    exports     : [ SocialInfoBarComponent ]
})
export class SocialInfoBarModule {
}
