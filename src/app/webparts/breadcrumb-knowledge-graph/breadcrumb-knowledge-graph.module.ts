import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadcrumbKnowledgeGraphComponent } from './breadcrumb-knowledge-graph.component';
import { RouterModule } from '@angular/router';
import { NumberToPersianModule } from '../../pipes/numberToPersian/numberToPersian.module';

@NgModule({
    imports     : [
        CommonModule,
        RouterModule,
        NumberToPersianModule
    ],
    declarations: [ BreadcrumbKnowledgeGraphComponent ],
    exports     : [ BreadcrumbKnowledgeGraphComponent ]
})
export class BreadcrumbKnowledgeGraphModule {
}
