import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BreadcrumbKnowledgeGraphComponent } from './breadcrumb-knowledge-graph.component';

describe('BreadcrumbKnowledgeGraphComponent', () => {
  let component: BreadcrumbKnowledgeGraphComponent;
  let fixture: ComponentFixture<BreadcrumbKnowledgeGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BreadcrumbKnowledgeGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreadcrumbKnowledgeGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
