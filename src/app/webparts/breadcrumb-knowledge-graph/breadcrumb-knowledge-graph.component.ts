import { FavoriteList } from '../../shared/favorite_list/favorite-list.model';
import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector   : 'app-breadcrumb-knowledge-graph',
    templateUrl: './breadcrumb-knowledge-graph.component.html',
    styleUrls  : [ './breadcrumb-knowledge-graph.component.scss' ]
})
export class BreadcrumbKnowledgeGraphComponent implements OnInit {
    @Input() FavList: FavoriteList[][] = [];
    
    fav_list_after_parse = new Array(new Array(new FavoriteList()));
    
    constructor () {
    }
    
    ngOnInit () {
        this.checkTags(this.FavList);
    }
    
    checkTags (tags: FavoriteList[][]): void {
        this.fav_list_after_parse = this.returnTagsMaxSize(tags);
        for (let i = 0; i < tags.length; i++) {
            let is_conflict = false;
            for (let j = 0; j < tags[ i ].length; j++) {
                for (let m = 0; m < this.fav_list_after_parse.length; m++) {
                    for (let n = 0; n < this.fav_list_after_parse[ m ].length; n++) {
                        if (tags[ i ][ j ].getId() === this.fav_list_after_parse[ m ][ n ].getId()) {
                            is_conflict = true;
                        }
                    }
                }
            }
            if (!is_conflict) {
                this.fav_list_after_parse.push(tags[ i ]);
            }
        }
    }
    
    returnTagsMaxSize (tags: FavoriteList[][]): FavoriteList[][] {
        const first_tags = new Array(new Array(new FavoriteList()));
        let max_length   = 0;
        let index        = 0;
        for (let i = 0; i < tags.length; i++) {
            if (tags[ i ].length > max_length) {
                max_length = tags[ i ].length;
                index      = i;
            }
        }
        first_tags.push(tags[ index ]);
        first_tags.splice(0, 1);
        return first_tags;
    }
}
