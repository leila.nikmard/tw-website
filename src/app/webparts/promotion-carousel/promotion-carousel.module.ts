import { NgModule } from '@angular/core';
import { PromotionCarouselComponent } from './promotion-carousel.component';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { NumberToPersianModule } from '../../pipes/numberToPersian/numberToPersian.module';
import { RouterModule } from '@angular/router';
import { CountdownModule } from 'ngx-countdown';
import { CountdownTimerModule } from '../../pipes/countdownTimer/countdownTimer.module';

@NgModule ({
    declarations: [ PromotionCarouselComponent ],
    exports     : [ PromotionCarouselComponent ],
    imports     : [
        CommonModule,
        NumberToPersianModule,
        RouterModule,
        NgbCarouselModule.forRoot (),
        CountdownModule,
        CountdownTimerModule
    ]
})

export class PromotionCarouselModule {

}
