import { Component, Input, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { Promotion } from '../../shared/promotion/promotion.model';
import { CountdownTimerPipe } from '../../pipes/countdownTimer/countdownTimer.pipe';

@Component ({
    selector   : 'app-promotion-carousel',
    templateUrl: './promotion-carousel.component.html',
    styleUrls  : [ './promotion-carousel.component.scss' ]
})
export class PromotionCarouselComponent implements OnInit {
    @Input () Promotions: Array<Promotion>;
    public day_per_sec    = CountdownTimerPipe.DAY_PER_SEC;
    
    constructor (private carouselConfig: NgbCarouselConfig) {
        this.carouselConfig.interval                 = 100000;
        this.carouselConfig.wrap                     = true;
        this.carouselConfig.keyboard                 = true;
        this.carouselConfig.pauseOnHover             = false;
        this.carouselConfig.showNavigationArrows     = true;
        this.carouselConfig.showNavigationIndicators = true;
    }
    
    ngOnInit () {
    }
    
}
