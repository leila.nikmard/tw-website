import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector   : 'app-poster-image',
    templateUrl: './poster-image.component.html',
    styleUrls  : [ './poster-image.component.scss' ]
})
export class PosterImageComponent implements OnInit {
    @Input() Image;
    @Input() Name;
    
    constructor () { }
    
    ngOnInit () {
    }
    
}
