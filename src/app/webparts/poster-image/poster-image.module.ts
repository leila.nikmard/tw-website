import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NumberToPersianModule } from '../../pipes/numberToPersian/numberToPersian.module';
import { PosterImageComponent } from './poster-image.component';

@NgModule({
    imports     : [
        CommonModule,
        NumberToPersianModule
    ],
    declarations: [ PosterImageComponent ],
    exports     : [ PosterImageComponent ]
})
export class PosterImageModule {
}
