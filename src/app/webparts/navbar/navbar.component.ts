import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../../shared/general/general.service';
import { UserService } from '../../shared/user/user.service';

@Component ({
    selector   : 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls  : [ './navbar.component.scss' ]
})

export class NavbarComponent implements OnInit {
    items = [
        {
            title  : 'پخش زنده',
            content: []
        },
        {
            title  : 'فیلم',
            content: [ 'انیمیشن', 'کمدی', 'اکشن', 'فانتزی', 'علمی-تخیلی', 'ترسناک', 'خانوادگی', 'دفاع مقدس', 'تاریخی', 'پلیسی', 'معمایی', 'عاشقانه' ]
        },
        {
            title  : 'سریال',
            content: [ 'انیمیشن', 'کمدی', 'اکشن', 'فانتزی', 'علمی-تخیلی', 'ترسناک', 'خانوادگی', 'دفاع مقدس', 'تاریخی', 'پلیسی', 'معمایی', 'عاشقانه' ]
        },
        {
            title  : 'کارتون',
            content: []
        },
        {
            title  : 'آرشیو',
            content: []
        },
        {
            title  : 'دریافت اپ',
            content: []
        }
    ];
    
    constructor (private generalService: GeneralService, public userService: UserService) {
    }
    
    ngOnInit () {
    }
    
    sign_out () {
        this.userService.user_is_sign_in = false;
        this.generalService.removeCookie (this.generalService.configs.variableNames.user_token);
        this.generalService.removeLocalStorage (this.generalService.configs.getBaseUrlsKey (this.generalService.configs.baseURLs.GET_USER_INFO));
    }
    
}
