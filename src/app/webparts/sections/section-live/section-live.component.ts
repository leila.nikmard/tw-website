import { Component, Input, OnInit } from '@angular/core';
import { Channel } from '../../../shared/channel/channel.model';

@Component ({
    selector   : 'app-section-live',
    templateUrl: './section-live.component.html',
    styleUrls  : [ './section-live.component.scss', '../section-styles.scss' ]
})
export class SectionLiveComponent implements OnInit {
    @Input () Channels: Array<Channel>;
    
    constructor () {
    }
    
    ngOnInit () {
    }
    
}
