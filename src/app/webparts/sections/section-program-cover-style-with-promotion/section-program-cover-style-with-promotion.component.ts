import { Component, Input, OnInit } from '@angular/core';
import { Program } from '../../../shared/program/program.model';

@Component ({
    selector   : 'app_section-program-cover-style-with-promotion',
    templateUrl: './section-program-cover-style-with-promotion.component.html',
    styleUrls  : [ './section-program-cover-style-with-promotion.component.scss' ]
})
export class SectionProgramCoverStyleWithPromotionComponent implements OnInit {
    @Input () Items: Program;
    
    constructor () {
    }
    
    ngOnInit () {
    }
}
