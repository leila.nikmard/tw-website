import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionProgramCoverStyleWithPromotionComponent } from './section-program-cover-style-with-promotion.component';

describe('SectionProgramCoverStyleWithPromotionComponent', () => {
  let component: SectionProgramCoverStyleWithPromotionComponent;
  let fixture: ComponentFixture<SectionProgramCoverStyleWithPromotionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionProgramCoverStyleWithPromotionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionProgramCoverStyleWithPromotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
