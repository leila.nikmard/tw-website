import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionEpisodeCoverStyleWithPromotionComponent } from './section-episode-cover-style-with-promotion.component';

describe('SectionEpisodeCoverStyleWithPromotionComponent', () => {
  let component: SectionEpisodeCoverStyleWithPromotionComponent;
  let fixture: ComponentFixture<SectionEpisodeCoverStyleWithPromotionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionEpisodeCoverStyleWithPromotionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionEpisodeCoverStyleWithPromotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
