import { Component, Input, OnInit } from '@angular/core';
import { Episode } from '../../../shared/episode/episode.model';
import { EpisodeRepository } from '../../../shared/episode/episode.repository';
import { GeneralService } from '../../../shared/general/general.service';

@Component ({
    selector   : 'app-section-episode-cover-style-with-promotion',
    templateUrl: './section-episode-cover-style-with-promotion.component.html',
    styleUrls  : [ './section-episode-cover-style-with-promotion.component.scss', '../section-styles.scss' ]
})
export class SectionEpisodeCoverStyleWithPromotionComponent implements OnInit {
    @Input () Episodes: Array<Episode>;
    @Input () SectionTitle: string;
    @Input () SectionTitleRoute: string;
    public first_item: Episode;
    public other_item: Array<Episode> = [];
    
    constructor (public generalService: GeneralService) {
    }
    
    ngOnInit () {
        this.first_item = EpisodeRepository.setEpisode (this.Episodes[ 0 ]);
        for (let i = 1; i < this.Episodes.length; i++) {
            this.other_item.push (EpisodeRepository.setEpisode (this.Episodes[ i ]));
        }
    }
    
}
