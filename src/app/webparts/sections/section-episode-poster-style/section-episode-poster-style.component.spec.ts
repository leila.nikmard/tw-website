import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionEpisodePosterStyleComponent } from './section-episode-poster-style.component';

describe('SectionEpisodePosterStyleComponent', () => {
  let component: SectionEpisodePosterStyleComponent;
  let fixture: ComponentFixture<SectionEpisodePosterStyleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionEpisodePosterStyleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionEpisodePosterStyleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
