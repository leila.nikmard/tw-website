import { Component, Input, OnInit } from '@angular/core';
import { Episode } from '../../../shared/episode/episode.model';
import { GeneralService } from '../../../shared/general/general.service';

@Component ({
    selector   : 'app-section-episode-cover-style',
    templateUrl: './section-episode-cover-style.component.html',
    styleUrls  : [ './section-episode-cover-style.component.scss', '../section-styles.scss' ]
})
export class SectionEpisodeCoverStyleComponent implements OnInit {
    @Input () Episodes: Array<Episode>;
    @Input () SectionTitle: string;
    @Input () SectionTitleRoute: string;
    
    constructor (public generalService: GeneralService) {
    }
    
    ngOnInit () {
    }
    
}
