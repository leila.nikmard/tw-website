import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionEpisodeCoverStyleComponent } from './section-episode-cover-style.component';

describe('SectionEpisodeCoverStyleComponent', () => {
  let component: SectionEpisodeCoverStyleComponent;
  let fixture: ComponentFixture<SectionEpisodeCoverStyleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionEpisodeCoverStyleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionEpisodeCoverStyleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
