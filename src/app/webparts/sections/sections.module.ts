import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SectionEpisodeCoverStyleComponent } from './section-episode-cover-style/section-episode-cover-style.component';
import { SectionEpisodeCoverStyleWithPromotionComponent } from './section-episode-cover-style-with-promotion/section-episode-cover-style-with-promotion.component';
import { SectionEpisodePosterStyleComponent } from './section-episode-poster-style/section-episode-poster-style.component';
import { SectionProgramPosterStyleComponent } from './section-program-poster-style/section-program-poster-style.component';
import { SectionProgramCoverStyleWithPromotionComponent } from './section-program-cover-style-with-promotion/section-program-cover-style-with-promotion.component';
import { SectionLiveComponent } from './section-live/section-live.component';
import { RouterModule } from '@angular/router';
import { NumberToPersianModule } from '../../pipes/numberToPersian/numberToPersian.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule ({
    imports     : [
        CommonModule,
        RouterModule,
        NumberToPersianModule,
        LazyLoadImageModule
    ],
    declarations: [
        SectionEpisodeCoverStyleComponent,
        SectionEpisodeCoverStyleWithPromotionComponent,
        SectionEpisodePosterStyleComponent,
        SectionProgramPosterStyleComponent,
        SectionProgramCoverStyleWithPromotionComponent,
        SectionLiveComponent
    ],
    exports     : [
        SectionEpisodeCoverStyleComponent,
        SectionEpisodeCoverStyleWithPromotionComponent,
        SectionEpisodePosterStyleComponent,
        SectionProgramPosterStyleComponent,
        SectionProgramCoverStyleWithPromotionComponent,
        SectionLiveComponent
    ]
})
export class SectionsModule {
}
