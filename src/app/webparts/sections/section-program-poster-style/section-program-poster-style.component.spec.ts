import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionProgramPosterStyleComponent } from './section-program-poster-style.component';

describe('SectionProgramPosterStyleComponent', () => {
  let component: SectionProgramPosterStyleComponent;
  let fixture: ComponentFixture<SectionProgramPosterStyleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionProgramPosterStyleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionProgramPosterStyleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
