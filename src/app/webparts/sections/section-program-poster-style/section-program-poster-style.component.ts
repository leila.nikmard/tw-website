import { Component, Input, OnInit } from '@angular/core';
import { Program } from '../../../shared/program/program.model';

@Component ({
    selector   : 'app-section-program-poster-style',
    templateUrl: './section-program-poster-style.component.html',
    styleUrls  : [ './section-program-poster-style.component.scss' ]
})
export class SectionProgramPosterStyleComponent implements OnInit {
    @Input () Items: Program;
    
    constructor () {
    }
    
    ngOnInit () {
    }
    
}
