import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QualityBarComponent } from './quality-bar.component';
import { NumberToPersianModule } from '../../pipes/numberToPersian/numberToPersian.module';

@NgModule({
    imports     : [
        CommonModule,
        NumberToPersianModule
    ],
    declarations: [ QualityBarComponent ],
    exports     : [ QualityBarComponent ]
})
export class QualityBarModule {
}
