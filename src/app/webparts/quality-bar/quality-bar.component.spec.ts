import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QualityBarComponent } from './quality-bar.component';

describe('QualityBarComponent', () => {
  let component: QualityBarComponent;
  let fixture: ComponentFixture<QualityBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QualityBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QualityBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
