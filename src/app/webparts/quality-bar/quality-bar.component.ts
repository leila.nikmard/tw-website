import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { ChannelLink } from '../../shared/channel/channel-link.model';

@Component({
    selector   : 'app-quality-bar',
    templateUrl: './quality-bar.component.html',
    styleUrls  : [ './quality-bar.component.scss' ]
})
export class QualityBarComponent implements OnInit, OnChanges {
    @Input() Links;
    @Input() ActiveLink;
    @Input() IsLive;
    @Input() HourlyArchiveTitle;
    @Input() HourlyArchiveChannel;
    @Output() SelectLink = new EventEmitter();
    @Output() BackToLive = new EventEmitter();
    
    constructor () { }
    
    ngOnInit () {
        this.Links = this.sortChannelsByBitrate(this.Links);
    }
    
    ngOnChanges () {
        this.Links = this.sortChannelsByBitrate(this.Links);
    }
    
    changeQuality (select_link) {
        this.SelectLink.emit(select_link.getLink());
        this.ActiveLink = select_link;
    }
    
    sortChannelsByBitrate (links: ChannelLink[]) {
        for (let i = 1; i < links.length; i++) {
            if (+links[ i ].getBitrate() < +links[ 0 ].getBitrate()) {
                links.unshift(links.splice(i, 1)[ 0 ]);
            }
            else if (+links[ i ].getBitrate() > +links[ i - 1 ].getBitrate()) {
                continue;
            }
            else {
                for (let j = 1; j < i; j++) {
                    if (+links[ i ].getBitrate() > +links[ j - 1 ].getBitrate() && +links[ i ].getBitrate() < +links[ j ].getBitrate()) {
                        links.splice(j, 0, links.splice(i, 1)[ 0 ]);
                    }
                }
            }
        }
        return links;
    }
    
    backToLive () {
        this.BackToLive.emit(this.ActiveLink);
        this.IsLive = true;
    }
}
