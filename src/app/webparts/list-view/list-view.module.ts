import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListViewComponent } from './list-view.component';
import { RouterModule } from '@angular/router';
import { NumberToPersianModule } from '../../pipes/numberToPersian/numberToPersian.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule({
    imports     : [
        CommonModule,
        RouterModule,
        NumberToPersianModule,
        LazyLoadImageModule
    ],
    declarations: [ ListViewComponent ],
    exports     : [ ListViewComponent ]
})
export class ListViewModule {
}
