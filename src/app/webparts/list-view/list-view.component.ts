import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GeneralService } from '../../shared/general/general.service';

@Component({
    selector   : 'app-list-view',
    templateUrl: './list-view.component.html',
    styleUrls  : [ './list-view.component.scss' ]
})
export class ListViewComponent implements OnInit {
    @Input() Items;
    @Input() Title;
    @Input() Type;
    @Input() EpisodeType;
    @Input() LoadMoreTitle = 'قسمت‌های';
    @Output() LoadMore = new EventEmitter();
    
    constructor (public generalService: GeneralService) {
    }
    
    ngOnInit () {
    }
}
