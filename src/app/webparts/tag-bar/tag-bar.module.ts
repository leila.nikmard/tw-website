import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TagBarComponent } from './tag-bar.component';
import { RouterModule } from '@angular/router';

@NgModule({
    imports     : [
        CommonModule,
        RouterModule
    ],
    declarations: [ TagBarComponent ],
    exports     : [ TagBarComponent ]
})
export class TagBarModule {
}
