import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector   : 'app-tag-bar',
    templateUrl: './tag-bar.component.html',
    styleUrls  : [ './tag-bar.component.scss' ]
})
export class TagBarComponent implements OnInit {
    @Input() Tags;
    
    constructor () { }
    
    ngOnInit () {
        this.Tags = this.Tags.split(',').reverse();
    }
    
}
