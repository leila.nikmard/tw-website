import { Component, Input, OnInit } from '@angular/core';
import { GeneralService } from '../../shared/general/general.service';
import { ActivatedRoute } from '@angular/router';
import { ChannelService } from '../../shared/channel/channel.service';
import { HourlyArchiveService } from '../../shared/hourly-archive/hourly-archive.service';

declare function RadiantMP (place_to_load: string): void;

@Component({
    selector   : 'app-radiant-player',
    templateUrl: './radiant-player.component.html',
    styleUrls  : [ './radiant-player.component.scss' ]
})

export class RadiantPlayerComponent implements OnInit {
    rmp: any;
    player_container: any;
    @Input() EpisodeLink: string;
    @Input() IsLive: boolean;
    @Input() Poster: string;
    @Input() AdUrl: string;
    init_settings: any = {
        licenseKey                  : 'Kl8lN3ZjdjR2K3NlazJ5ZWk/cm9tNWRhc2lzMzBkYjBBJV8q',
        bitrates                    : '',
        isLive                      : false,
        delayToFade                 : 3000,
        width                       : 900,
        height                      : 400,
        autoHeightMode              : true,
        poster                      : '',
        ads                         : true,
        debug                       : true,
        streamDebug                 : true,
        adLinearHideControls        : false,
        autoplay                    : true,
        hlsJSMaxBufferLength        : 60,
        hlsJSStopDownloadWhilePaused: false,
        hlsJSEnableCEACaptions      : true,
        hlsJSLiveSyncDuration       : 30,
        googleCast                  : true,
        adTagUrl                    : '',
        adLocale                    : 'fa',
        adCountDown                 : true
    };

    constructor (public generalService: GeneralService) {
    }
    
    ngOnInit () {
        this.initPlayer();
    }
    
    setPlayLinkAndInit (link) {
        if(this.generalService.is_platform_browser){
            if (this.player_container) {
                this.player_container.innerHTML = '';
            }
            this.rmp.destroy();
            this.initPlayer();
            this.init_settings.isLive   = this.IsLive;
            this.init_settings.poster   = this.Poster;
            this.init_settings.bitrates = {
                hls: link
            };
            this.rmp.init(this.init_settings);
        }
    }
    
    destroyPlayer () {
        if(this.generalService.is_platform_browser){
            if (this.player_container) {
                this.player_container.innerHTML = '';
            }
            this.rmp.destroy();
        }
    }
    
    initPlayer () {
        if(this.generalService.is_platform_browser){
            this.rmp              = new RadiantMP('player');
            this.player_container = document.getElementById('player');
        }
    }
    
    setPlayerMoment (second) {
        if(this.generalService.is_platform_browser){
            this.rmp.seekTo(second * 1000);
        }
    }
}
