import { NgModule } from '@angular/core';
import { RadiantPlayerComponent } from './radiant-player.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [ RadiantPlayerComponent ],
    exports     : [ RadiantPlayerComponent ],
    imports     : [
        CommonModule,
        RouterModule
    ]
})

export class RadiantPlayerModule {

}
