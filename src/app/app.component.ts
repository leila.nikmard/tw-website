import { Component, OnInit } from '@angular/core';
import { GeneralService } from './shared/general/general.service';
import { NavigationEnd, Router } from '@angular/router';
import { UserService } from './shared/user/user.service';
import { UserRepository } from './shared/user/user.repository';

@Component ({
    selector   : 'app-root',
    templateUrl: './app.component.html',
    styleUrls  : [ './app.component.scss' ]
})
export class AppComponent implements OnInit {
    constructor (private generalService: GeneralService, private router: Router, private userService: UserService) {
    }
    
    ngOnInit () {
        const date = new Date ();
        if (this.generalService.getLocalStorage (this.generalService.configs.variableNames.expire_time) ===
            this.generalService.configs.variableNames.null_local_storage) {
            this.generalService.setLocalStorage (this.generalService.configs.variableNames.expire_time,
                (date.setTime (date.getTime () + 10 * 60 * 1000)).toString ());
        } else if (this.generalService.getLocalStorage (this.generalService.configs.variableNames.expire_time) < date.getTime ().toString ()) {
            this.generalService.clearLocalStorage ();
        }
        this.getUserInfo (false);
    }
    
    getUserInfo (force_request: boolean) {
        this.generalService.requestRouterUrl (force_request).subscribe (
            () => {
                this.userService.getUserInfo (this.generalService.router_path).subscribe (
                    (result) => {
                        this.generalService.current_user = UserRepository.setUser (result.data[ 0 ].user_info);
                    },
                    (error) => {
                    }
                );
            },
            (error) => {
            }
        );
    }
}
