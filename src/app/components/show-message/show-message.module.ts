import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowMessageComponent } from './show-message.component';

@NgModule ({
    imports     : [
        CommonModule
    ],
    declarations: [ ShowMessageComponent ],
    exports     : [ ShowMessageComponent ]
})
export class ShowMessageModule {
}
