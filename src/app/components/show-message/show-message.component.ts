import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../../shared/general/general.service';

@Component ({
    selector   : 'app-show-message',
    templateUrl: './show-message.component.html',
    styleUrls  : [ './show-message.component.scss' ]
})
export class ShowMessageComponent implements OnInit {
    
    constructor (public generalService: GeneralService) {
    }
    
    ngOnInit () {
    }
}
