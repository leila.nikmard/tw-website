import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NavbarModule } from './webparts/navbar/navbar.module';
import { FooterModule } from './webparts/footer/footer.module';
import { GeneralService } from './shared/general/general.service';
import { AnalyticsService } from './shared/analytic/analytics.service';
import { JalaliDateService } from './shared/jalali-date/jalali.service';
import { LinkService } from './shared/general/link.service';
import { UserService } from './shared/user/user.service';
import { Configs, StaticPagesData } from './shared/general/configs';
import { LocalStorageService } from './shared/general/local-storage.service';
import { RadiantPlayerModule } from './webparts/radiant-player/radiant-player.module';
import { NotFoundModule } from './pages/not-found/not-found.module';
import { ShowMessageModule } from './components/show-message/show-message.module';
import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { CookiesService } from './shared/general/cookies.service';

@NgModule ({
    schemas     : [ CUSTOM_ELEMENTS_SCHEMA ],
    declarations: [
        AppComponent
    ],
    imports     : [
        NavbarModule,
        ShowMessageModule,
        RadiantPlayerModule,
        FooterModule,
        CommonModule,
        NotFoundModule,
        HttpClientModule,
        BrowserModule.withServerTransition ({ appId: 'new-tw' }),
        RouterModule.forRoot ([
                {
                    path        : '',
                    loadChildren: './modules#Modules'
                }
            ],
            {
                useHash                  : false,
                scrollPositionRestoration: 'top'
            })
    ],
    providers   : [ GeneralService, LinkService, AnalyticsService, JalaliDateService, Configs, StaticPagesData, LocalStorageService, UserService, CookiesService, CookieService
    ],
    bootstrap   : [ AppComponent ]
})
export class AppModule {
}
