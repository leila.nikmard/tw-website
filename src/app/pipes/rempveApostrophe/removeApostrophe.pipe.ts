import { Pipe, PipeTransform } from '@angular/core';

@Pipe ({
    name: 'removeApostrophe'
})
export class RemoveApostrophePipe implements PipeTransform {
    
    static removeApostrophe (string) {
        return RemoveApostrophePipe.replaceAll (string, '\'', '');
    }
    
    static replaceAll (str, to_find, to_replace) {
        return str.split (to_find).join (to_replace);
    }
    
    transform (value: any, args?: any): any {
        if (!value) {
            return value;
        }
        return RemoveApostrophePipe.removeApostrophe (value);
    }
}
