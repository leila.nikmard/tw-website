import { NgModule } from '@angular/core';
import { NumberToPersianPipe } from './numberToPersian.pipe';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [ NumberToPersianPipe ],
    exports     : [ NumberToPersianPipe ],
    imports     : [
        CommonModule
    ]
})

export class NumberToPersianModule {

}