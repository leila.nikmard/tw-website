import { Pipe, PipeTransform } from '@angular/core';

@Pipe ({ name: 'numberToPersian' })
export class NumberToPersianPipe implements PipeTransform {
    static toPersianNum (number) {
        number = NumberToPersianPipe.replaceAll (number, '1', '۱');
        number = NumberToPersianPipe.replaceAll (number, '2', '۲');
        number = NumberToPersianPipe.replaceAll (number, '3', '۳');
        number = NumberToPersianPipe.replaceAll (number, '4', '۴');
        number = NumberToPersianPipe.replaceAll (number, '5', '۵');
        number = NumberToPersianPipe.replaceAll (number, '6', '۶');
        number = NumberToPersianPipe.replaceAll (number, '7', '۷');
        number = NumberToPersianPipe.replaceAll (number, '8', '۸');
        number = NumberToPersianPipe.replaceAll (number, '9', '۹');
        number = NumberToPersianPipe.replaceAll (number, '0', '۰');
        return number;
    }
    
    static replaceAll (str, to_find, to_replace) {
        const str_temp = str.split (to_find);
        return str_temp.join (to_replace);
    }
    
    transform (value: any, args?: any): any {
        if (!value) {
            return value;
        }
        return NumberToPersianPipe.toPersianNum (value);
    }
}
