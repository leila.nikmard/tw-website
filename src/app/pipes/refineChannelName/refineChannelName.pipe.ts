import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'refineChannelName'
})
export class RefineChannelNamePipe implements PipeTransform {
    
    ok_channels = [ 1, 2, 3, 4, 5 ];
    
    transform (value: any, args?: any): any {
        return this.refine_channel_name(value);
    }
    
    refine_channel_name (channel) {
        if (this.ok_channels.includes(channel.id)) {
            return channel.name;
        }
        else {
            return 'شبکه ' + channel.name;
        }
        
    }
}
