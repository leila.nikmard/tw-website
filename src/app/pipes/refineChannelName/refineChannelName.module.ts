import { NgModule } from '@angular/core';
import { RefineChannelNamePipe } from './refineChannelName.pipe';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [ RefineChannelNamePipe ],
    exports     : [ RefineChannelNamePipe ],
    imports: [
        CommonModule
    ]
})
export class RefineChannelNameModule {

}