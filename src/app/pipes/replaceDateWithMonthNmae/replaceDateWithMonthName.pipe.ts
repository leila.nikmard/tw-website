import { Pipe, PipeTransform } from '@angular/core';

@Pipe ({
    name: 'replaceDateWithMonthName'
})
export class ReplaceDateWithMonthNamePipe implements PipeTransform {
    
    static month_to_name = {
        '01': 'فروردین',
        '02': 'اردیبهشت',
        '03': 'خرداد',
        '04': 'تیر',
        '05': 'مرداد',
        '06': 'شهریور',
        '07': 'مهر',
        '08': 'آبان',
        '09': 'آذر',
        '10': 'دی',
        '11': 'بهمن',
        '12': 'اسفند'
    };
    
    static replace_month_name (date_play_time) {
        let date       = date_play_time.split (' ')[ 0 ].split ('-');
        let year       = date[ 0 ];
        let month      = +date[ 1 ];
        let day        = date[ 2 ];
        let month_name = ReplaceDateWithMonthNamePipe.month_to_name[ month.toLocaleString (undefined, { minimumIntegerDigits: 2 }) ];
        return day + ' ' + month_name + ' ' + year;
    }
    
    transform (value: any, args?: any): any {
        return ReplaceDateWithMonthNamePipe.replace_month_name (value);
    }
}
