import { NgModule } from '@angular/core';
import { ReplaceDateWithMonthNamePipe } from './replaceDateWithMonthName.pipe';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [ ReplaceDateWithMonthNamePipe ],
    exports     : [ ReplaceDateWithMonthNamePipe ],
    imports     : [
        CommonModule
    ]
})

export class ReplaceDateWithMonthNameModule {

}