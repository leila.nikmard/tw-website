import { Pipe, PipeTransform } from '@angular/core';

@Pipe ({
    name: 'generateVideoDuration'
})
export class GenerateVideoDuration implements PipeTransform {
    
    static generate_video_duration (value) {
        return value;
    }
    
    static generate_video_duration_for_episodes (value, episode_id) {
        if (value.search (':') == '1') {
            const array_date     = value.split (':');
            return (3600 * array_date[ 0 ]) + (60 * array_date[ 1 ]);
        } else {
            if (value == 0) {
                const seconds = episode_id.toString ().substring (parseInt (episode_id.toString ().length) - 2, 3);
                if (seconds > 59) {
                    return seconds;
                } else {
                    return Math.round (seconds / 2);
                }
            } else {
                return 60 * value;
            }
            
        }
    }
    
    transform (value: any, args?: any): any {
        return GenerateVideoDuration.generate_video_duration (value);
    }
}
