import { Pipe, PipeTransform } from '@angular/core';

@Pipe ({ name: 'countdownTime' })
export class CountdownTimerPipe implements PipeTransform {
    static CONST_PARAMETER = 1000;
    static DAY_PER_HOUR    = 24;
    static HOUR_PER_MIN    = 60;
    static MIN_PER_SEC     = 60;
    static DAY_PER_SEC     = CountdownTimerPipe.DAY_PER_HOUR * CountdownTimerPipe.HOUR_PER_MIN * CountdownTimerPipe.MIN_PER_SEC;
    static HOUR_PER_SEC    = CountdownTimerPipe.HOUR_PER_MIN * CountdownTimerPipe.MIN_PER_SEC;
    
    static convertTime (time: string): number {
        return CountdownTimerPipe.calculateDiffTime (new Date (), new Date (time));
    }
    
    static calculateDiffTime (start: Date, end: Date): number {
        let diff      = end.getTime () - start.getTime ();
        let diff_time =
                (Math.floor ((Math.floor (diff / ((this.CONST_PARAMETER * this.HOUR_PER_MIN * this.MIN_PER_SEC)))) / this.DAY_PER_HOUR)).toLocaleString (undefined, { minimumIntegerDigits: 2 }) + ':' +
                (Math.floor (diff / (this.CONST_PARAMETER * this.HOUR_PER_MIN * this.MIN_PER_SEC)) % this.DAY_PER_HOUR).toLocaleString (undefined, { minimumIntegerDigits: 2 }) + ':' +
                (Math.floor (diff / (this.CONST_PARAMETER * this.HOUR_PER_MIN)) % this.MIN_PER_SEC).toLocaleString (undefined, { minimumIntegerDigits: 2 }) + ':' +
                (Math.floor (diff / this.CONST_PARAMETER) % this.HOUR_PER_MIN).toLocaleString (undefined, { minimumIntegerDigits: 2 });
        return CountdownTimerPipe.calculateRemainderTime (diff_time);
    }
    
    static calculateRemainderTime (time): number {
        let day    = +time.split (':')[ 0 ];
        let hour   = +time.split (':')[ 1 ];
        let minute = +time.split (':')[ 2 ];
        let sec    = +time.split (':')[ 3 ];
        return (day * this.DAY_PER_SEC) + (hour * this.HOUR_PER_SEC) + (minute * this.MIN_PER_SEC) + sec;
    }
    
    transform (show_time: string, args?: any): any {
        if (show_time === '') {
            return 0;
        }
        return CountdownTimerPipe.convertTime (show_time);
    }
}
