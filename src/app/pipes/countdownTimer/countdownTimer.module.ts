import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountdownTimerPipe } from './countdownTimer.pipe';

@NgModule ({
    declarations: [ CountdownTimerPipe ],
    exports     : [ CountdownTimerPipe ],
    imports     : [
        CommonModule
    ]
})

export class CountdownTimerModule {

}
